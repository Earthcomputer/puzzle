package net.earthcomputer.puzzle.algebra;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.ast.IAstNode;

public class FormattedText {

	private String[] strings;
	private IAstNode[] astNodes;

	private FormattedText(String[] strings, IAstNode[] astNodes) {
		this.strings = strings;
		this.astNodes = astNodes;
	}

	public String[] getStrings() {
		return strings;
	}

	public IAstNode[] getAstNodes() {
		return astNodes;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private List<String> strings = new ArrayList<>();
		private List<IAstNode> astNodes = new ArrayList<>();
		private StringBuilder currentString = new StringBuilder();

		private Builder() {
		}

		public Builder append(boolean b) {
			currentString.append(b);
			return this;
		}

		public Builder append(char c) {
			currentString.append(c);
			return this;
		}

		public Builder append(char[] chars, int start, int len) {
			currentString.append(chars, start, len);
			return this;
		}

		public Builder append(char[] charArray) {
			currentString.append(charArray);
			return this;
		}

		public Builder append(CharSequence chars, int start, int len) {
			currentString.append(chars, start, len);
			return this;
		}

		public Builder append(CharSequence chars) {
			currentString.append(chars);
			return this;
		}

		public Builder append(double d) {
			currentString.append(d);
			return this;
		}

		public Builder append(float f) {
			currentString.append(f);
			return this;
		}

		public Builder append(int i) {
			currentString.append(i);
			return this;
		}

		public Builder append(long l) {
			currentString.append(l);
			return this;
		}

		public Builder append(Object o) {
			currentString.append(o);
			return this;
		}

		public Builder append(String s) {
			currentString.append(s);
			return this;
		}

		public Builder append(StringBuffer sb) {
			currentString.append(sb);
			return this;
		}

		public Builder appendCodePoint(int c) {
			currentString.appendCodePoint(c);
			return this;
		}

		public Builder append(IAstNode astNode) {
			strings.add(currentString.toString());
			currentString.setLength(0);
			astNodes.add(astNode);
			return this;
		}

		public Builder append(FormattedText text) {
			for (int i = 0; i < text.getAstNodes().length; i++) {
				append(text.getStrings()[i]).append(text.getAstNodes()[i]);
			}
			return append(text.getStrings()[text.getStrings().length - 1]);
		}

		public FormattedText build() {
			strings.add(currentString.toString());
			return new FormattedText(strings.toArray(new String[strings.size()]),
					astNodes.toArray(new IAstNode[astNodes.size()]));
		}
	}

}
