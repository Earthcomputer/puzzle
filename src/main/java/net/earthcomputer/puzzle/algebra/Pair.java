package net.earthcomputer.puzzle.algebra;

import java.util.Objects;

public class Pair<L, R> {

	private final L left;
	private final R right;

	private Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}

	public static <L, R> Pair<L, R> of(L left, R right) {
		return new Pair<>(left, right);
	}

	public L getLeft() {
		return left;
	}

	public R getRight() {
		return right;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(left) + Objects.hashCode(right);
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Pair && equals((Pair<?, ?>) other);
	}

	public boolean equals(Pair<?, ?> other) {
		return Objects.equals(left, other.left) && Objects.equals(right, other.right);
	}
	
	@Override
	public String toString() {
		return "(" + left + ", " + right + ")";
	}

}
