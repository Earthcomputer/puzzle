package net.earthcomputer.puzzle.algebra.soltree;

import net.earthcomputer.puzzle.algebra.ast.Equation;

public class SolutionTree {

	private SolutionTreeNode root;
	private SolutionTreeNode workingNode;

	public SolutionTree(Equation equation) {
		root = new SolutionTreeNode(equation, null, null);
		workingNode = root;
	}

	public SolutionTreeNode getRootNode() {
		return root;
	}

	public SolutionTreeNode getWorkingNode() {
		return workingNode;
	}

	public void setWorkingNode(SolutionTreeNode node) {
		this.workingNode = node;
	}

}
