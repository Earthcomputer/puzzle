package net.earthcomputer.puzzle.algebra.soltree;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.rule.IAlgebraRule;
import net.earthcomputer.puzzle.algebra.rule.RuleContext;

public class SolutionTreeNode {

	private SolutionTreeNode parent;
	private List<SolutionTreeNode> children = new ArrayList<>();
	private Equation equation;
	private IAlgebraRule rule;
	private RuleContext context;

	public SolutionTreeNode(Equation equation, IAlgebraRule rule, RuleContext context) {
		this.equation = equation;
		this.rule = rule;
		this.context = context;
	}

	public List<SolutionTreeNode> getChildren() {
		return children;
	}

	public void addChild(SolutionTreeNode child) {
		children.add(child);
		child.parent = this;
	}

	public Equation getEquation() {
		return equation;
	}

	public SolutionTreeNode getParent() {
		return parent;
	}

	public FormattedText getTooltip() {
		FormattedText.Builder text = FormattedText.builder().append(equation);
		if (rule != null) {
			text.append("\nRule applied: ").append(rule.getDescription(context));
		}
		return text.build();
	}

}
