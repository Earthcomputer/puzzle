package net.earthcomputer.puzzle.algebra;

import static java.util.Calendar.*;

import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Random;

public class DateMessageGenerator {

	private DateMessageGenerator() {
	}

	public static String generateWittyMessage() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(YEAR);
		int month = cal.get(MONTH);
		int day = cal.get(DAY_OF_MONTH);

		Pair<Integer, Integer> easter = calculateDateOfEaster(year);
		Calendar d1 = Calendar.getInstance();
		d1.clear();
		d1.set(year, month, day);
		Calendar d2 = Calendar.getInstance();
		d2.clear();
		d2.set(year, easter.getRight(), easter.getLeft());
		int daysUntilEaster = (int) ChronoUnit.DAYS.between(d1.toInstant(), d2.toInstant());

		if (month == JANUARY && day == 1) {
			return "Happy New Year!";
		}
		if (month == FEBRUARY && day == 14) {
			return "Happy Valentine's Day <3";
		}
		if (month == FEBRUARY && day == 29) {
			return "I hope this in not your birthday";
		}
		if (month == MARCH && day == 14) {
			return "Happy Pi Day! 3.141592653589793236264338327950";
		}
		if (month == APRIL && day == 1 && cal.get(Calendar.HOUR_OF_DAY) < 12) {
			return "April fools!";
		}
		if (month == MAY && day == 4) {
			return "May the force be with you!";
		}
		if (month == OCTOBER && day == 31) {
			return "Happy Halloween!";
		}
		if (month == DECEMBER && day == 24) {
			return "Merry Christmas Eve!";
		}
		if (month == DECEMBER && day == 25) {
			return "Merry Christmas!";
		}
		if (month == DECEMBER && day == 31) {
			return "Happy New Year's Eve!";
		}
		if (daysUntilEaster == 0) {
			return "Happy Easter!";
		}
		if (daysUntilEaster == 2) {
			return "Happy Good Friday!";
		}
		if (daysUntilEaster == 28) {
			return "Happy Shrove Tuesday!";
		}

		String[] wittyComments = {
				"What do you get if you divide the circumference of a jack o'lantern by its diameter? A pumpkin pi!",
				"At a job interview, tell them you're willing to give 110%. Unless the job is a statistician.",
				"Did you hear the joke about the statistician? Probably.",
				"Why did the chicken cross the Mobius strip? To get to the same side.",
				"Why don't calculus majors throw house parties? Because you should never drink and derive.",
				"Infinitely many mathematicians walk into a bar. The first says, \"I'll have a beer\". The second says, \"I'll have half a beer\". The third says, \"I'll have a quarter of a beer\". Before anyone else can speak, the barman fills up exactly two glasses of beer and serves them. \"Come on now,\" he says to the group, \"You guys need to learn your limits.\"" };
		return wittyComments[new Random().nextInt(wittyComments.length)];
	}

	private static Pair<Integer, Integer> calculateDateOfEaster(int year) {
		int a = year % 19;
		int b = year / 100;
		int c = year % 100;
		int d = b / 4;
		int e = b % 4;
		int f = (b + 8) / 25;
		int g = (b - f + 1) / 3;
		int h = (19 * a + b - d - g + 15) % 30;
		int i = c / 4;
		int k = c % 4;
		int l = (32 + 2 * e + 2 * i - h - k) % 7;
		int m = (a + 11 * h + 22 * l) / 451;
		int n = (h + l - 7 * m + 114) / 31;
		int p = (h + l - 7 * m + 114) % 31;
		return Pair.of(p + 1, n - 1);
	}

}
