package net.earthcomputer.puzzle.algebra.ast;

import java.math.BigDecimal;

/**
 * A decimal literal
 */
public class NumberTerm extends SimpleTerm implements ILeafAstNode {

	private BigDecimal value;

	public NumberTerm(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String toLatex() {
		return value.toString();
	}

	@Override
	public String getFormat() {
		return value.toString();
	}

	@Override
	public NumberTerm copy() {
		return new NumberTerm(value);
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.NUMBER;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.NUMBER;
	}

	@Override
	public boolean isEquivalent(IAstNode other) {
		return other instanceof NumberTerm && value.equals(((NumberTerm) other).value);
	}

}
