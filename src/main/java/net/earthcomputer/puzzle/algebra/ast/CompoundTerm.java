package net.earthcomputer.puzzle.algebra.ast;

public abstract class CompoundTerm extends Term {

	@Override
	public abstract CompoundTerm copy();
	
}
