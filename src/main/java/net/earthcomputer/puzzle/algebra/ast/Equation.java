package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * An assertion that two expressions are equal, or otherwise comparing them.
 */
public class Equation implements IAstNode {

	private Expression left;
	private Expression right;
	private ComparisonOperator operator;

	public Equation(Expression left, Expression right, ComparisonOperator operator) {
		this.left = left;
		this.right = right;
		this.operator = operator;
	}

	public Expression getLeft() {
		return left;
	}

	public void setLeft(Expression left) {
		this.left = left;
	}

	public Expression getRight() {
		return right;
	}

	public void setRight(Expression right) {
		this.right = right;
	}

	public ComparisonOperator getOperator() {
		return operator;
	}

	public void setOperator(ComparisonOperator operator) {
		this.operator = operator;
	}

	@Override
	public String toLatex() {
		return left.toLatex() + operator.toLatex() + right.toLatex();
	}

	@Override
	public String getFormat() {
		return "$1$2$3";
	}

	@Override
	public Equation copy() {
		return new Equation(left.copy(), right.copy(), operator);
	}

	@Override
	public IAstNode getChild(int index) {
		switch (index) {
		case 0:
			return left;
		case 1:
			return operator;
		case 2:
			return right;
		default:
			throw Util.indexOutOfBounds(index, 3);
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		switch (index) {
		case 0:
			left = (Expression) child;
			break;
		case 1:
			operator = (ComparisonOperator) child;
			break;
		case 2:
			right = (Expression) child;
			break;
		default:
			throw Util.indexOutOfBounds(index, 3);
		}
	}

	@Override
	public int getChildCount() {
		return 3;
	}

}
