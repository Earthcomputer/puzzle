package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * Surrounds an expression in parentheses
 */
public class ExpressionTerm extends SimpleTerm {

	private Expression expression;

	public ExpressionTerm(Expression expression) {
		this.expression = expression;
	}

	public Expression getExpression() {
		return expression;
	}

	public void setExpression(Expression expression) {
		this.expression = expression;
	}

	@Override
	public String toLatex() {
		return "(" + expression.toLatex() + ")";
	}

	@Override
	public String getFormat() {
		return "($1)";
	}

	@Override
	public ExpressionTerm copy() {
		return new ExpressionTerm(expression.copy());
	}

	@Override
	public IAstNode getChild(int index) {
		if (index == 0) {
			return expression;
		} else {
			throw Util.indexOutOfBounds(index, 1);
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		if (index == 0) {
			expression = (Expression) child;
		} else {
			throw Util.indexOutOfBounds(index, 1);
		}
	}

	@Override
	public int getChildCount() {
		return 1;
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.PARENTHESIS;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.PARENTHESIS;
	}

}
