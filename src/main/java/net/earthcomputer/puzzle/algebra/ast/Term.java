package net.earthcomputer.puzzle.algebra.ast;

public abstract class Term implements IAstNode {

	@Override
	public abstract Term copy();
	
}
