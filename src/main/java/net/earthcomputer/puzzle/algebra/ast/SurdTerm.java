package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * Normally represents the square root of an algebraic expression, but sometimes
 * (an arbitrary algebraic expression)th root
 */
public class SurdTerm extends SimpleTerm implements ISurd<Expression, Expression> {

	private Expression n;
	private Expression x;

	public SurdTerm(Expression n, Expression x) {
		this.n = n;
		this.x = x;
	}

	@Override
	public Expression getN() {
		return n;
	}

	public void setN(Expression n) {
		this.n = n;
	}

	@Override
	public Expression getX() {
		return x;
	}

	public void setX(Expression x) {
		this.x = x;
	}

	@Override
	public String toLatex() {
		if (n == null) {
			return "\\sqrt{" + x.toLatex() + "}";
		} else {
			return "\\sqrt[" + n.toLatex() + "]{" + x.toLatex() + "}";
		}
	}

	@Override
	public String getFormat() {
		return null;
	}

	@Override
	public SurdTerm copy() {
		return new SurdTerm(n == null ? null : n.copy(), x.copy());
	}

	@Override
	public IAstNode getChild(int index) {
		switch (index) {
		case 0:
			return n;
		case 1:
			return x;
		default:
			throw Util.indexOutOfBounds(index, 2);
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		switch (index) {
		case 0:
			n = (Expression) child;
			break;
		case 1:
			x = (Expression) child;
			break;
		default:
			throw Util.indexOutOfBounds(index, 2);
		}
	}

	@Override
	public int getChildCount() {
		return 2;
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.SURD;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.SURD;
	}

}
