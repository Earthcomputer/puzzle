package net.earthcomputer.puzzle.algebra.ast;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.SpecialChars;

/**
 * An additive operator, used in expressions
 */
public enum Sign implements ILeafAstNode {

	PLUS(" + ", "+"), MINUS(" - ", "-"), PLUS_MINUS(" " + SpecialChars.PLUS_MINUS + " ", "\\pm"), MINUS_PLUS(null,
			"\\mp");

	private final String text;
	private final String latex;

	private Sign(String text, String latex) {
		this.text = text;
		this.latex = latex;
	}

	public String getText() {
		return text;
	}

	public Sign negate() {
		switch (this) {
		case PLUS:
			return MINUS;
		case MINUS:
			return PLUS;
		case PLUS_MINUS:
			return MINUS_PLUS;
		case MINUS_PLUS:
			return PLUS_MINUS;
		default:
			throw new AssertionError();
		}
	}

	public Sign multiply(Sign other) {
		switch (this) {
		case PLUS:
			return other;
		case MINUS:
			return other.negate();
		case PLUS_MINUS:
			switch (other) {
			case PLUS:
				return PLUS_MINUS;
			case MINUS:
				return MINUS_PLUS;
			case PLUS_MINUS:
				return PLUS;
			case MINUS_PLUS:
				return MINUS;
			default:
				throw new AssertionError();
			}
		case MINUS_PLUS:
			switch (other) {
			case PLUS:
				return MINUS_PLUS;
			case MINUS:
				return PLUS_MINUS;
			case PLUS_MINUS:
				return MINUS;
			case MINUS_PLUS:
				return PLUS;
			default:
				throw new AssertionError();
			}
		default:
			throw new AssertionError();
		}
	}

	public boolean isAmbiguous() {
		return this != PLUS && this != MINUS;
	}

	public BigDecimal multiply(BigDecimal val) {
		if (isAmbiguous()) {
			throw new IllegalStateException("Cannot multiply by ambiguous sign");
		} else if (this == MINUS) {
			return val.negate();
		} else {
			return val;
		}
	}

	@Override
	public String toLatex() {
		return latex;
	}

	@Override
	public String getFormat() {
		return text;
	}

	@Override
	public Sign copy() {
		return this;
	}

	@Override
	public boolean isEquivalent(IAstNode other) {
		return this == other;
	}

}
