package net.earthcomputer.puzzle.algebra.ast;

/**
 * An algebraic variable
 */
public class VariableTerm extends SimpleTerm implements ILeafAstNode {

	private String name;

	public VariableTerm(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toLatex() {
		return name;
	}

	@Override
	public String getFormat() {
		return name;
	}

	@Override
	public VariableTerm copy() {
		return new VariableTerm(name);
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.LETTER;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.LETTER;
	}

	@Override
	public boolean isEquivalent(IAstNode other) {
		return other instanceof VariableTerm && name.equals(((VariableTerm) other).name);
	}

}
