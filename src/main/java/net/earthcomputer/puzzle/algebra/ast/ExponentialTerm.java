package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * An algebraic term raised to the power of an algebraic expression
 */
public class ExponentialTerm extends SimpleTerm implements IExponent<SimpleTerm, Expression> {

	private SimpleTerm base;
	private Expression exponent;

	public ExponentialTerm(SimpleTerm base, Expression exponent) {
		this.base = base;
		this.exponent = exponent;
	}

	@Override
	public SimpleTerm getBase() {
		return base;
	}

	public void setBase(SimpleTerm base) {
		this.base = base;
	}

	@Override
	public Expression getExponent() {
		return exponent;
	}

	public void setExponent(Expression exponent) {
		this.exponent = exponent;
	}

	@Override
	public String toLatex() {
		return base.toLatex() + "^{" + exponent.toLatex() + "}";
	}

	@Override
	public String getFormat() {
		return null;
	}

	@Override
	public ExponentialTerm copy() {
		return new ExponentialTerm(base.copy(), exponent.copy());
	}

	@Override
	public IAstNode getChild(int index) {
		switch (index) {
		case 0:
			return base;
		case 1:
			return exponent;
		default:
			throw Util.indexOutOfBounds(index, 2);
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		switch (index) {
		case 0:
			base = (SimpleTerm) child;
			break;
		case 1:
			exponent = (Expression) child;
			break;
		default:
			throw Util.indexOutOfBounds(index, 2);
		}
	}

	@Override
	public int getChildCount() {
		return 2;
	}

	@Override
	public TermEndType getStartType() {
		return base.getStartType();
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.EXPONENT;
	}

}
