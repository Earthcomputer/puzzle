package net.earthcomputer.puzzle.algebra.ast;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * A term signifying a product of simpler algebraic terms
 */
public class ProductTerm extends CompoundTerm {

	private List<SimpleTerm> terms;

	public ProductTerm(SimpleTerm... terms) {
		this(Util.arrayListOf(terms));
	}

	public ProductTerm(List<SimpleTerm> terms) {
		this.terms = terms;
	}

	public List<SimpleTerm> getTerms() {
		return terms;
	}

	public void setTerms(List<SimpleTerm> terms) {
		this.terms = terms;
	}

	@Override
	public String toLatex() {
		if (terms.isEmpty()) {
			return "";
		}
		StringBuilder latex = new StringBuilder();
		latex.append(terms.get(0).toLatex());
		for (int i = 1; i < terms.size(); i++) {
			if (!Util.shouldJuxtapose(terms.get(i - 1), terms.get(i))) {
				latex.append("\\times");
			}
			latex.append(terms.get(i).toLatex());
		}
		return latex.toString();
	}

	@Override
	public String getFormat() {
		if (terms.isEmpty()) {
			return "";
		}
		StringBuilder format = new StringBuilder();
		format.append("$1");
		for (int i = 1; i < terms.size(); i++) {
			if (!Util.shouldJuxtapose(terms.get(i - 1), terms.get(i))) {
				format.append(" * ");
			}
			format.append("$").append(i + 1);
		}
		return format.toString();
	}

	@Override
	public ProductTerm copy() {
		List<SimpleTerm> newTerms = new ArrayList<>(terms.size());
		terms.forEach(it -> newTerms.add(it.copy()));
		return new ProductTerm(newTerms);
	}

	@Override
	public IAstNode getChild(int index) {
		return terms.get(index);
	}

	@Override
	public void setChild(int index, IAstNode child) {
		terms.set(index, (SimpleTerm) child);
	}

	@Override
	public int getChildCount() {
		return terms.size();
	}

}
