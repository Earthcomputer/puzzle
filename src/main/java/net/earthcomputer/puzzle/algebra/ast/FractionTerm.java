package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * Signifies an algebraic expression divided by another algebraic expression
 */
public class FractionTerm extends SimpleTerm implements IFraction<Expression> {

	private Expression ntor;
	private Expression dtor;

	public FractionTerm(Expression ntor, Expression dtor) {
		this.ntor = ntor;
		this.dtor = dtor;
	}

	@Override
	public Expression getNumerator() {
		return ntor;
	}

	public void setNumerator(Expression ntor) {
		this.ntor = ntor;
	}

	@Override
	public Expression getDenominator() {
		return dtor;
	}

	public void setDenominator(Expression dtor) {
		this.dtor = dtor;
	}

	@Override
	public String toLatex() {
		return "\\frac{" + ntor.toLatex() + "}{" + dtor.toLatex() + "}";
	}

	@Override
	public String getFormat() {
		return null;
	}

	@Override
	public FractionTerm copy() {
		return new FractionTerm(ntor.copy(), dtor.copy());
	}

	@Override
	public IAstNode getChild(int index) {
		switch (index) {
		case 0:
			return ntor;
		case 1:
			return dtor;
		default:
			throw Util.indexOutOfBounds(index, 2);
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		switch (index) {
		case 0:
			ntor = (Expression) child;
			break;
		case 1:
			dtor = (Expression) child;
			break;
		default:
			throw Util.indexOutOfBounds(index, 2);
		}
	}

	@Override
	public int getChildCount() {
		return 2;
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.FRACTION;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.FRACTION;
	}

}
