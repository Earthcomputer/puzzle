package net.earthcomputer.puzzle.algebra.ast;

import java.util.AbstractList;
import java.util.List;

/**
 * An equation AST node
 */
public interface IAstNode {

	/**
	 * Copies this node
	 */
	IAstNode copy();

	/**
	 * Returns the number of child nodes
	 */
	int getChildCount();

	/**
	 * Returns the child node at index
	 */
	IAstNode getChild(int index);

	/**
	 * Sets the child node at index
	 */
	void setChild(int index, IAstNode val);

	/**
	 * Gets a list of the child nodes
	 */
	default List<IAstNode> getChildren() {
		return new AbstractList<IAstNode>() {
			@Override
			public IAstNode get(int index) {
				return getChild(index);
			}

			@Override
			public IAstNode set(int index, IAstNode val) {
				IAstNode old = getChild(index);
				setChild(index, val);
				return old;
			}

			@Override
			public int size() {
				return getChildCount();
			}
		};
	}

	/**
	 * Converts this AST node to latex
	 */
	String toLatex();

	/**
	 * Formats this AST node for conversion to an editable equation
	 */
	String getFormat();

	/**
	 * Whether this AST node is deemed to be equal to the other AST node, as in
	 * both algebraically equal and expressed in exactly the same way. Avoid
	 * using equals() so that maps and sets don't consider them equal.
	 */
	default boolean isEquivalent(IAstNode other) {
		if (getClass() != other.getClass()) {
			return false;
		}
		if (getChildCount() != other.getChildCount()) {
			return false;
		}
		for (int i = 0; i < getChildCount(); i++) {
			if (!getChild(i).isEquivalent(other.getChild(i))) {
				return false;
			}
		}
		return true;
	}

}
