package net.earthcomputer.puzzle.algebra.ast;

/**
 * Something divided by something else
 */
public interface IFraction<T> {

	T getNumerator();

	T getDenominator();

}
