package net.earthcomputer.puzzle.algebra.ast;

public enum TermEndType {

	NUMBER, LETTER, PARENTHESIS, FRACTION, EXPONENT, SURD
	
}
