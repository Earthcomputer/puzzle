package net.earthcomputer.puzzle.algebra.ast;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * An algebraic expression, or sum of terms
 */
public class Expression implements IAstNode {

	private List<ProductTerm> terms;
	private List<Sign> signs;

	public Expression(List<ProductTerm> terms, List<Sign> signs) {
		this.terms = terms;
		this.signs = signs;
	}

	public Expression(Object... items) {
		if (items.length % 2 != 0) {
			throw new IllegalArgumentException("Incorrect number of items");
		}

		List<ProductTerm> terms = new ArrayList<>();
		List<Sign> signs = new ArrayList<>();
		try {
			for (int i = 0; i < items.length; i += 2) {
				signs.add((Sign) items[i]);
				terms.add((ProductTerm) items[i + 1]);
			}
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Incorrect type of item(s)");
		}

		this.terms = terms;
		this.signs = signs;
	}

	public List<ProductTerm> getTerms() {
		return terms;
	}

	public void setTerms(List<ProductTerm> terms) {
		this.terms = terms;
	}

	public List<Sign> getSigns() {
		return signs;
	}

	public void setSigns(List<Sign> signs) {
		this.signs = signs;
	}

	@Override
	public String toLatex() {
		if (terms.isEmpty()) {
			return "";
		}

		StringBuilder latex = new StringBuilder();
		if (signs.get(0) != Sign.PLUS) {
			latex.append(signs.get(0).toLatex());
		}

		latex.append(terms.get(0).toLatex());
		for (int i = 1; i < terms.size(); i++) {
			latex.append(signs.get(i).toLatex()).append(terms.get(i).toLatex());
		}

		return latex.toString();
	}

	@Override
	public String getFormat() {
		if (terms.isEmpty()) {
			return "";
		}

		StringBuilder format = new StringBuilder();
		if (signs.get(0) != Sign.PLUS) {
			format.append(signs.get(0).getText().substring(1, 2));
		}

		format.append("$2");
		for (int i = 1; i < terms.size(); i++) {
			format.append(signs.get(i).getText()).append("$").append((i + 1) * 2);
		}

		return format.toString();
	}

	@Override
	public Expression copy() {
		List<ProductTerm> newTerms = new ArrayList<>(terms.size());
		terms.forEach(it -> newTerms.add(it.copy()));
		List<Sign> newSigns = new ArrayList<>(signs);
		return new Expression(newTerms, newSigns);
	}

	@Override
	public IAstNode getChild(int index) {
		if (index < 0 || index >= terms.size() * 2) {
			throw Util.indexOutOfBounds(index, terms.size() * 2);
		}
		if (index % 2 == 0) {
			return signs.get(index / 2);
		} else {
			return terms.get(index / 2);
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		if (index < 0 || index >= terms.size() * 2) {
			throw Util.indexOutOfBounds(index, terms.size() * 2);
		}
		if (index % 2 == 0) {
			signs.set(index / 2, (Sign) child);
		} else {
			terms.set(index / 2, (ProductTerm) child);
		}
	}

	@Override
	public int getChildCount() {
		return terms.size() * 2;
	}

}
