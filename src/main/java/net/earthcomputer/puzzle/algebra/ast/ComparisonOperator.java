package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.SpecialChars;

/**
 * Compares the left and right of an equation
 */
public enum ComparisonOperator implements ILeafAstNode {

	EQUALS(" = ", "="), GREATER_THAN(" > ", ">"), LESS_THAN(" < ", "<"), GREATER_THAN_EQUAL(
			" " + SpecialChars.GREATER_THAN_EQUAL + " ",
			"\\ge"), LESS_THAN_EQUAL(" " + SpecialChars.LESS_THAN_EQUAL + " ", "\\le");

	private final String text;
	private final String latex;

	private ComparisonOperator(String text, String latex) {
		this.text = text;
		this.latex = latex;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toLatex() {
		return latex;
	}

	@Override
	public String getFormat() {
		return text;
	}

	@Override
	public ComparisonOperator copy() {
		return this;
	}

	@Override
	public boolean isEquivalent(IAstNode other) {
		return this == other;
	}

}
