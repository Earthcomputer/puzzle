package net.earthcomputer.puzzle.algebra.ast;

/**
 * Something raised to the power of something else
 */
public interface IExponent<B, E> {

	B getBase();

	E getExponent();

}
