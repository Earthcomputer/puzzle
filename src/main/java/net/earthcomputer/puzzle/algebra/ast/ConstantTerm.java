package net.earthcomputer.puzzle.algebra.ast;

/**
 * Contains a named mathematical constant, e.g. "pi"
 */
public class ConstantTerm extends SimpleTerm implements ILeafAstNode {

	private String name;

	public ConstantTerm(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toLatex() {
		return name;
	}

	@Override
	public String getFormat() {
		return name;
	}

	@Override
	public ConstantTerm copy() {
		return new ConstantTerm(name);
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.LETTER;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.LETTER;
	}

	@Override
	public boolean isEquivalent(IAstNode other) {
		return other instanceof ConstantTerm && name.equals(((ConstantTerm) other).name);
	}

}
