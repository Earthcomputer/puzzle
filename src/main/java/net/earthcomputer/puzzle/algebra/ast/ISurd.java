package net.earthcomputer.puzzle.algebra.ast;

/**
 * Represents the square root of x if n is null, and the nth root of x otherwise
 */
public interface ISurd<N, X> {

	N getN();

	X getX();

}
