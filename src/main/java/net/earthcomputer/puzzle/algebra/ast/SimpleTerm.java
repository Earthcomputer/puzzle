package net.earthcomputer.puzzle.algebra.ast;

public abstract class SimpleTerm extends Term {

	@Override
	public abstract SimpleTerm copy();

	public abstract TermEndType getStartType();

	public abstract TermEndType getEndType();

}
