package net.earthcomputer.puzzle.algebra.ast;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import net.earthcomputer.puzzle.algebra.Util;

/**
 * A reference to a many-to-one mapping between numbers
 */
public class FunctionTerm extends SimpleTerm {

	private String name;
	private boolean inverse;
	private int nDerivatives;
	private int powers;
	private List<Expression> params;
	private List<Expression> args;

	public FunctionTerm(String name, boolean inverse, int nDerivatives, int powers, List<Expression> params,
			List<Expression> args) {
		this.name = name;
		this.inverse = inverse;
		this.nDerivatives = nDerivatives;
		this.powers = powers;
		this.params = params;
		this.args = args;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isInverse() {
		return inverse;
	}

	public void setInverse(boolean inverse) {
		this.inverse = inverse;
	}

	public int getnDerivatives() {
		return nDerivatives;
	}

	public void setnDerivatives(int nDerivatives) {
		this.nDerivatives = nDerivatives;
	}

	public int getPowers() {
		return powers;
	}

	public void setPowers(int powers) {
		this.powers = powers;
	}

	public List<Expression> getParams() {
		return params;
	}

	public void setParams(List<Expression> params) {
		this.params = params;
	}

	public List<Expression> getArgs() {
		return args;
	}

	public void setArgs(List<Expression> args) {
		this.args = args;
	}

	@Override
	public String toLatex() {
		StringBuilder latex = new StringBuilder(name);
		if (!params.isEmpty()) {
			latex.append("_{").append(params.stream().map(IAstNode::toLatex).collect(Collectors.joining(",")))
					.append("}");
		}
		if (inverse) {
			latex.append("^{-1}");
		} else if (nDerivatives != 0) {
			latex.append(Stream.generate(() -> "'").limit(nDerivatives).collect(Collectors.joining()));
		} else if (powers != 1) {
			latex.append("^{" + powers + "}");
		}
		latex.append("(").append(args.stream().map(IAstNode::toLatex).collect(Collectors.joining(","))).append(")");
		return latex.toString();
	}

	@Override
	public String getFormat() {
		if (!params.isEmpty() || inverse || powers != 1) {
			return null;
		}

		StringBuilder format = new StringBuilder(name);
		if (nDerivatives != 0) {
			format.append(Stream.generate(() -> "'").limit(nDerivatives).collect(Collectors.joining()));
		}
		format.append("(");
		if (!args.isEmpty()) {
			format.append("$1");
			for (int i = 1; i < args.size(); i++) {
				format.append(", $").append(i + 1);
			}
		}
		format.append(")");
		return format.toString();
	}

	@Override
	public FunctionTerm copy() {
		List<Expression> newParams = new ArrayList<>(params.size());
		params.forEach(it -> newParams.add(it.copy()));
		List<Expression> newArgs = new ArrayList<>(args.size());
		args.forEach(it -> newArgs.add(it.copy()));
		return new FunctionTerm(name, inverse, nDerivatives, powers, newParams, newArgs);
	}

	@Override
	public IAstNode getChild(int index) {
		if (index >= 0 && index < params.size()) {
			return params.get(index);
		} else if (index < params.size() + args.size()) {
			return args.get(index - params.size());
		} else {
			throw Util.indexOutOfBounds(index, params.size() + args.size());
		}
	}

	@Override
	public void setChild(int index, IAstNode child) {
		if (index >= 0 && index < params.size()) {
			params.set(index, (Expression) child);
		} else if (index < params.size() + args.size()) {
			args.set(index - params.size(), (Expression) child);
		} else {
			throw Util.indexOutOfBounds(index, params.size() + args.size());
		}
	}

	@Override
	public int getChildCount() {
		return params.size() + args.size();
	}

	@Override
	public TermEndType getStartType() {
		return TermEndType.LETTER;
	}

	@Override
	public TermEndType getEndType() {
		return TermEndType.PARENTHESIS;
	}

}
