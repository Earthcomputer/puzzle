package net.earthcomputer.puzzle.algebra.ast;

import net.earthcomputer.puzzle.algebra.Util;

public interface ILeafAstNode extends IAstNode {

	@Override
	default IAstNode getChild(int index) {
		throw Util.indexOutOfBounds(index, 0);
	}

	@Override
	default void setChild(int index, IAstNode child) {
		throw Util.indexOutOfBounds(index, 0);
	}

	@Override
	default int getChildCount() {
		return 0;
	}

}
