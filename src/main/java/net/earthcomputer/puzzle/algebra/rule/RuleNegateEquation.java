package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;

public class RuleNegateEquation implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		return context.getPathSize() == 1;
	}

	@Override
	public void apply(RuleContext context) {
		negateExpression(context.getEquation().getLeft());
		negateExpression(context.getEquation().getRight());
	}

	private static void negateExpression(Expression expr) {
		for (int i = 0; i < expr.getTerms().size(); i++) {
			expr.getSigns().set(i, expr.getSigns().get(i).negate());
		}
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Negate equation").build();
	}

	@Override
	public String getIcon() {
		return "buttons/negate_equation";
	}

}
