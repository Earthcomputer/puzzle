package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.ExponentialTerm;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleNumericalIndices implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof ExponentialTerm)) {
			return false;
		}
		ExponentialTerm exponent = (ExponentialTerm) context.getDeepestNode();
		if (!(exponent.getBase() instanceof NumberTerm)) {
			return false;
		}
		Expression sup = exponent.getExponent();
		if (sup.getTerms().size() != 1) {
			return false;
		}
		if (sup.getSigns().get(0).isAmbiguous()) {
			return false;
		}
		ProductTerm product = sup.getTerms().get(0);
		if (product.getTerms().size() != 1) {
			return false;
		}
		if (!(product.getTerms().get(0) instanceof NumberTerm)) {
			return false;
		}
		BigDecimal b = ((NumberTerm) exponent.getBase()).getValue();
		BigDecimal e = ((NumberTerm) product.getTerms().get(0)).getValue();
		if (sup.getSigns().get(0) == Sign.MINUS) {
			e = e.negate();
		}
		try {
			BigDecimals.pow(b, e);
		} catch (ArithmeticException e1) {
			return false;
		}
		return true;
	}

	@Override
	public void apply(RuleContext context) {
		ExponentialTerm exponent = (ExponentialTerm) context.getDeepestNode();
		BigDecimal b = ((NumberTerm) exponent.getBase()).getValue();
		BigDecimal e = ((NumberTerm) exponent.getExponent().getTerms().get(0).getTerms().get(0)).getValue();
		if (exponent.getExponent().getSigns().get(0) == Sign.MINUS) {
			e = e.negate();
		}

		BigDecimal result = BigDecimals.pow(b, e);
		SimpleTerm term;
		if (result.signum() == -1) {
			term = new ExpressionTerm(new Expression(Sign.MINUS, new ProductTerm(new NumberTerm(result.negate()))));
		} else {
			term = new NumberTerm(result);
		}
		context.getNodeFromEnd(1).setChild(context.getPathIndexFromEnd(1), term);
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Numerical indices").build();
	}

	@Override
	public String getIcon() {
		return "buttons/numerical_indices";
	}

}
