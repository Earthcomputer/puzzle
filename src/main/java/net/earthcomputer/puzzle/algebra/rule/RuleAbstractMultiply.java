package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public abstract class RuleAbstractMultiply implements IAlgebraRule {

	private static final List<RuleAbstractMultiply> MULTIPLY_RULES = new ArrayList<>();

	public RuleAbstractMultiply() {
		MULTIPLY_RULES.add(this);
	}

	public static void doMultiplication(RuleContext context) {
		boolean changed = true;
		outerLoop: while (changed && !context.isInvalid()) {
			changed = false;
			for (RuleAbstractMultiply rule : MULTIPLY_RULES) {
				if (rule.canApplyAutomatically() && rule.canApply(context)) {
					rule.apply(context);
					changed = true;
					continue outerLoop;
				}
			}
		}
	}

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof ProductTerm)) {
			return false;
		}
		ProductTerm product = (ProductTerm) context.getDeepestNode();

		for (SimpleTerm a : product.getTerms()) {
			for (SimpleTerm b : product.getTerms()) {
				if (a != b) {
					if (canApplyOnPair(a, b)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void apply(RuleContext context) {
		ProductTerm product = (ProductTerm) context.getDeepestNode();

		for (SimpleTerm a : product.getTerms()) {
			for (SimpleTerm b : product.getTerms()) {
				if (a != b) {
					if (canApplyOnPair(a, b)) {
						SimpleTerm result = applyOnPair(a, b);
						product.getTerms().remove(a);
						product.getTerms().remove(b);
						if (result != null) {
							product.getTerms().add(0, result);
						} else if (product.getTerms().isEmpty()) {
							product.getTerms().add(new NumberTerm(BigDecimals.ONE));
						}
						postApplyOnPair(context.withOneExtra(0));
						return;
					}
				}
			}
		}
	}

	protected boolean canApplyAutomatically() {
		return true;
	}

	public abstract boolean canApplyOnPair(SimpleTerm pre, SimpleTerm post);

	public abstract SimpleTerm applyOnPair(SimpleTerm pre, SimpleTerm post);

	public void postApplyOnPair(RuleContext context) {
	}

}
