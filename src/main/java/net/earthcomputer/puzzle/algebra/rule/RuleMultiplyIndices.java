package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.ExponentialTerm;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleMultiplyIndices extends RuleAbstractMultiply {

	public static Pair<SimpleTerm, Expression> getAsIndex(SimpleTerm term) {
		if (term instanceof ExponentialTerm) {
			ExponentialTerm exponential = (ExponentialTerm) term;
			return Pair.of(exponential.getBase(), exponential.getExponent());
		} else {
			return Pair.of(term, new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(BigDecimals.ONE))));
		}
	}

	public static BigDecimal expressionAsNumber(Expression expr) {
		if (expr.getTerms().size() != 1) {
			return null;
		}
		Sign sign = expr.getSigns().get(0);
		if (sign.isAmbiguous()) {
			return null;
		}
		ProductTerm product = expr.getTerms().get(0);
		if (product.getTerms().size() != 1) {
			return null;
		}
		SimpleTerm term = product.getTerms().get(0);
		if (!(term instanceof NumberTerm)) {
			return null;
		}
		return sign.multiply(((NumberTerm) term).getValue());
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Multiply indices").build();
	}

	@Override
	public String getIcon() {
		return "buttons/multiply_indices";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm pre, SimpleTerm post) {
		return getAsIndex(pre).getLeft().isEquivalent(getAsIndex(post).getLeft());
	}

	@Override
	public SimpleTerm applyOnPair(SimpleTerm pre, SimpleTerm post) {
		Pair<SimpleTerm, Expression> prePair = getAsIndex(pre);
		Pair<SimpleTerm, Expression> postPair = getAsIndex(post);
		SimpleTerm base = prePair.getLeft();

		BigDecimal preNum = expressionAsNumber(prePair.getRight());
		BigDecimal postNum = expressionAsNumber(postPair.getRight());
		if (preNum == null || postNum == null) {
			Expression exponent = new Expression();
			exponent.getSigns().addAll(prePair.getRight().getSigns());
			exponent.getTerms().addAll(prePair.getRight().getTerms());
			exponent.getSigns().addAll(postPair.getRight().getSigns());
			exponent.getTerms().addAll(postPair.getRight().getTerms());
			return new ExponentialTerm(base, exponent);
		} else {
			BigDecimal newIndex = preNum.add(postNum);
			if (newIndex.signum() == 0) {
				return null;
			} else if (newIndex.equals(BigDecimals.ONE)) {
				return base;
			}
			Expression exponent;
			if (newIndex.signum() == 1) {
				exponent = new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(newIndex)));
			} else {
				exponent = new Expression(Sign.MINUS, new ProductTerm(new NumberTerm(newIndex.negate())));
			}
			return new ExponentialTerm(base, exponent);
		}
	}

}
