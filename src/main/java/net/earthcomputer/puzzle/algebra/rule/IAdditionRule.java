package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public interface IAdditionRule {

	boolean canApply(SimpleTerm leftTerm, SimpleTerm rightTerm);

	Pair<Sign, SimpleTerm> add(SimpleTerm leftTerm, SimpleTerm rightTerm);

	Pair<Sign, SimpleTerm> subtract(SimpleTerm leftTerm, SimpleTerm rightTerm);

}
