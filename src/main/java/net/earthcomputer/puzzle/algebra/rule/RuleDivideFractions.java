package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleDivideFractions extends RuleAbstractFractionCancel {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Divide fractions").build();
	}

	@Override
	public String getIcon() {
		return "buttons/divide_fraction";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		return ntorTerm instanceof FractionTerm || dtorTerm instanceof FractionTerm;
	}

	@Override
	public Pair<ProductTerm, ProductTerm> applyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		Expression ntorNtor, ntorDtor;
		if (ntorTerm instanceof FractionTerm) {
			FractionTerm fraction = (FractionTerm) ntorTerm;
			ntorNtor = fraction.getNumerator();
			ntorDtor = fraction.getDenominator();
		} else {
			ntorNtor = new Expression(Sign.PLUS, new ProductTerm(ntorTerm));
			ntorDtor = null;
		}
		Expression dtorNtor, dtorDtor;
		if (dtorTerm instanceof FractionTerm) {
			FractionTerm fraction = (FractionTerm) dtorTerm;
			dtorNtor = fraction.getNumerator();
			dtorDtor = fraction.getDenominator();
		} else {
			dtorNtor = new Expression(Sign.PLUS, new ProductTerm(dtorTerm));
			dtorDtor = null;
		}

		ProductTerm ntor = new ProductTerm();
		if (ntorNtor.getTerms().size() == 1 && ntorNtor.getSigns().get(0) == Sign.PLUS) {
			ntor.getTerms().addAll(ntorNtor.getTerms().get(0).getTerms());
		} else {
			ntor.getTerms().add(new ExpressionTerm(ntorNtor));
		}
		if (dtorDtor != null) {
			if (dtorDtor.getTerms().size() == 1 && dtorDtor.getSigns().get(0) == Sign.PLUS) {
				ntor.getTerms().addAll(dtorDtor.getTerms().get(0).getTerms());
			} else {
				ntor.getTerms().add(new ExpressionTerm(dtorDtor));
			}
		}

		ProductTerm dtor = new ProductTerm();
		if (ntorDtor != null) {
			if (ntorDtor.getTerms().size() == 1 && ntorDtor.getSigns().get(0) == Sign.PLUS) {
				dtor.getTerms().addAll(ntorDtor.getTerms().get(0).getTerms());
			} else {
				dtor.getTerms().add(new ExpressionTerm(ntorDtor));
			}
		}
		if (dtorNtor.getTerms().size() == 1 && dtorNtor.getSigns().get(0) == Sign.PLUS) {
			dtor.getTerms().addAll(dtorNtor.getTerms().get(0).getTerms());
		} else {
			dtor.getTerms().add(new ExpressionTerm(dtorNtor));
		}

		return Pair.of(ntor, dtor);
	}

	@Override
	public void postApplyOnPair(RuleContext context) {
		RuleAbstractMultiply.doMultiplication(context.withOneExtra(0).withOneExtra(1));
		RuleAbstractMultiply.doMultiplication(context.withOneExtra(1).withOneExtra(1));
	}

}
