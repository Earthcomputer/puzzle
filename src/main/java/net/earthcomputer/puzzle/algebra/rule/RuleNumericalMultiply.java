package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleNumericalMultiply extends RuleAbstractMultiply {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Numerical multiply").build();
	}

	@Override
	public String getIcon() {
		return "buttons/multiply";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm pre, SimpleTerm post) {
		return pre instanceof NumberTerm && post instanceof NumberTerm;
	}

	@Override
	public SimpleTerm applyOnPair(SimpleTerm pre, SimpleTerm post) {
		BigDecimal n = ((NumberTerm) pre).getValue().multiply(((NumberTerm) post).getValue(), BigDecimals.MATH_CTX);
		if (n.equals(BigDecimals.ONE)) {
			return null;
		} else {
			return new NumberTerm(n);
		}
	}

}
