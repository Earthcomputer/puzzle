package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.ast.Expression;

public class RuleExchangeSides implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		return context.getPathSize() == 1;
	}

	@Override
	public void apply(RuleContext context) {
		Equation equation = context.getEquation();
		Expression temp = equation.getLeft();
		equation.setLeft(equation.getRight());
		equation.setRight(temp);
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Exchange sides").build();
	}

	@Override
	public String getIcon() {
		return "buttons/exchange_sides";
	}

}
