package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public class RuleApproxFraction implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		return context.getDeepestNode() instanceof NumberTerm
				&& !BigDecimals.isInteger(((NumberTerm) context.getDeepestNode()).getValue());
	}

	@Override
	public void apply(RuleContext context) {
		Pair<BigDecimal, BigDecimal> frac = BigDecimals
				.approximateToFraction(((NumberTerm) context.getDeepestNode()).getValue());
		context.getNodeFromEnd(1).setChild(context.getPathIndexFromEnd(1),
				new FractionTerm(new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(frac.getLeft()))),
						new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(frac.getRight())))));
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Approximate to fraction").build();
	}

	@Override
	public String getIcon() {
		return "buttons/approx_fraction";
	}

}
