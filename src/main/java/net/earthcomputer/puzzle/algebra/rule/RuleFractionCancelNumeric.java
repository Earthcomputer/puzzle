package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleFractionCancelNumeric extends RuleAbstractFractionCancel {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Numeric fraction cancel").build();
	}

	@Override
	public String getIcon() {
		return "buttons/fraction_cancel_numeric";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		if (!(ntorTerm instanceof NumberTerm)) {
			return false;
		}
		if (!(dtorTerm instanceof NumberTerm)) {
			return false;
		}
		BigDecimal dtor = ((NumberTerm) dtorTerm).getValue();
		if (dtor.signum() == 0) {
			return false;
		}
		BigDecimal ntor = ((NumberTerm) ntorTerm).getValue();
		if (BigDecimals.gcd(ntor, dtor).equals(BigDecimals.ONE)) {
			return false;
		}
		return true;
	}

	@Override
	public Pair<ProductTerm, ProductTerm> applyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		NumberTerm ntorNum = (NumberTerm) ntorTerm;
		NumberTerm dtorNum = (NumberTerm) dtorTerm;
		BigDecimal gcd = BigDecimals.gcd(ntorNum.getValue(), dtorNum.getValue());
		BigDecimal ntor = ntorNum.getValue().divide(gcd, BigDecimals.MATH_CTX);
		BigDecimal dtor = dtorNum.getValue().divide(gcd, BigDecimals.MATH_CTX);
		if (ntor.equals(BigDecimals.ONE)) {
			ntorNum = null;
		} else {
			ntorNum.setValue(ntor);
		}
		if (dtor.equals(BigDecimals.ONE)) {
			dtorNum = null;
		} else {
			dtorNum.setValue(dtor);
		}
		return Pair.of(ntorNum == null ? new ProductTerm() : new ProductTerm(ntorNum),
				dtorNum == null ? new ProductTerm() : new ProductTerm(dtorNum));
	}

}
