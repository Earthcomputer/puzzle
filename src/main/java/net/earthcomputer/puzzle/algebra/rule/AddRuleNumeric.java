package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class AddRuleNumeric implements IAdditionRule {

	@Override
	public boolean canApply(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		return leftTerm instanceof NumberTerm && rightTerm instanceof NumberTerm;
	}

	@Override
	public Pair<Sign, SimpleTerm> add(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		return Pair.of(Sign.PLUS,
				new NumberTerm(((NumberTerm) leftTerm).getValue().add(((NumberTerm) rightTerm).getValue())));
	}

	@Override
	public Pair<Sign, SimpleTerm> subtract(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		BigDecimal result = ((NumberTerm) leftTerm).getValue().subtract(((NumberTerm) rightTerm).getValue());
		if (result.signum() == -1) {
			return Pair.of(Sign.MINUS, new NumberTerm(result.negate()));
		} else {
			return Pair.of(Sign.PLUS, new NumberTerm(result));
		}
	}

}
