package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleExpandBrackets extends RuleAbstractMultiply {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Expand brackets").build();
	}

	@Override
	public String getIcon() {
		return "buttons/expand_brackets";
	}

	@Override
	public boolean canApplyAutomatically() {
		return false;
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm pre, SimpleTerm post) {
		return pre instanceof ExpressionTerm || post instanceof ExpressionTerm;
	}

	@Override
	public SimpleTerm applyOnPair(SimpleTerm pre, SimpleTerm post) {
		List<Pair<Sign, ProductTerm>> preTerms = getAllTerms(pre);
		List<Pair<Sign, ProductTerm>> postTerms = getAllTerms(post);

		Expression newExpr = new Expression();
		for (Pair<Sign, ProductTerm> preTerm : preTerms) {
			for (Pair<Sign, ProductTerm> postTerm : postTerms) {
				ProductTerm newProduct = new ProductTerm();
				newProduct.getTerms().addAll(preTerm.getRight().copy().getTerms());
				newProduct.getTerms().addAll(postTerm.getRight().copy().getTerms());
				newExpr.getSigns().add(preTerm.getLeft().multiply(postTerm.getLeft()));
				newExpr.getTerms().add(newProduct);
			}
		}

		return new ExpressionTerm(newExpr);
	}

	private static List<Pair<Sign, ProductTerm>> getAllTerms(SimpleTerm term) {
		List<Pair<Sign, ProductTerm>> terms = new ArrayList<>();
		if (term instanceof ExpressionTerm) {
			Expression expr = ((ExpressionTerm) term).getExpression();
			for (int i = 0; i < expr.getTerms().size(); i++) {
				terms.add(Pair.of(expr.getSigns().get(i), expr.getTerms().get(i)));
			}
		} else {
			terms.add(Pair.of(Sign.PLUS, new ProductTerm(term)));
		}
		return terms;
	}

	@Override
	public void postApplyOnPair(RuleContext context) {
		Expression expr = ((ExpressionTerm) context.getDeepestNode()).getExpression();

		RuleContext exprContext = context.withOneExtra(0);
		List<RuleContext> contextsToMultiply = new ArrayList<>();
		for (int i = expr.getTerms().size() - 1; i >= 0; i--) {
			contextsToMultiply.add(exprContext.withOneExtra(i * 2 + 1));
		}

		for (RuleContext toMultiply : contextsToMultiply) {
			if (toMultiply.isInvalid()) {
				continue;
			}
			RuleAbstractMultiply.doMultiplication(toMultiply);
		}

		IAlgebraRule.insertExpression(expr, (Expression) context.getNodeFromEnd(2), context.getPathIndexFromEnd(2) / 2,
				context.getPathIndexFromEnd(1));
	}

}
