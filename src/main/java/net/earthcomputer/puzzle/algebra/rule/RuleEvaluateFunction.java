package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.BuiltInFunctions;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FunctionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleEvaluateFunction implements IAlgebraRule {

	private static BigDecimal evaluateToNumber(Expression expression) {
		if (expression.getTerms().size() != 1) {
			return null;
		}
		if (expression.getSigns().get(0).isAmbiguous()) {
			return null;
		}
		ProductTerm product = expression.getTerms().get(0);
		if (product.getTerms().size() != 1) {
			return null;
		}
		if (!(product.getTerms().get(0) instanceof NumberTerm)) {
			return null;
		}
		return expression.getSigns().get(0).multiply(((NumberTerm) product.getTerms().get(0)).getValue());
	}

	private static BigDecimal evaluate(FunctionTerm function, BigDecimal input) {
		String functionName = function.getName();
		if (function.isInverse()) {
			if (functionName.startsWith("arc")) {
				functionName = functionName.substring(3);
			} else if (functionName.startsWith("ar")) {
				functionName = functionName.substring(2);
			} else if (functionName.endsWith("h")) {
				functionName = "ar" + functionName;
			} else {
				functionName = "arc" + functionName;
			}
		}
		BigDecimal value = input;
		value = BuiltInFunctions.evaluate(functionName, value);
		if (function.getPowers() != 1) {
			value = BigDecimals.pow(value, BigDecimal.valueOf(function.getPowers()));
		}
		return value;
	}

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof FunctionTerm)) {
			return false;
		}
		FunctionTerm function = (FunctionTerm) context.getDeepestNode();
		if (!BuiltInFunctions.isTrigFunction(function.getName())) {
			if (function.isInverse()) {
				return false;
			}
			if (function.getPowers() != 1) {
				return false;
			}
		}
		if (function.getnDerivatives() != 0) {
			return false;
		}
		if (function.getParams().size() != 0) {
			return false;
		}
		if (function.getArgs().size() != 1) {
			return false;
		}
		BigDecimal value = evaluateToNumber(function.getArgs().get(0));
		if (value == null) {
			return false;
		}
		try {
			evaluate(function, value);
		} catch (ArithmeticException e) {
			return false;
		}
		return true;
	}

	@Override
	public void apply(RuleContext context) {
		FunctionTerm function = (FunctionTerm) context.getDeepestNode();
		BigDecimal value = evaluateToNumber(function.getArgs().get(0));
		value = evaluate(function, value);

		SimpleTerm term;
		if (value.signum() == -1) {
			term = new ExpressionTerm(new Expression(Sign.MINUS, new ProductTerm(new NumberTerm(value.negate()))));
		} else {
			term = new NumberTerm(value);
		}
		context.getNodeFromEnd(1).setChild(context.getPathIndexFromEnd(1), term);
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Evaluate ").append(context.getDeepestNode()).build();
	}

	@Override
	public String getIcon() {
		return "buttons/evaluate_function";
	}

}
