package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;
import net.earthcomputer.puzzle.algebra.ast.VariableTerm;

public class RuleNumericalDivide extends RuleAbstractFractionCancel {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Numerical divide").build();
	}

	@Override
	public String getIcon() {
		return "buttons/divide";
	}

	@Override
	protected boolean canApplyAutomatically() {
		return false;
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		return ntorTerm instanceof NumberTerm && dtorTerm instanceof NumberTerm;
	}

	@Override
	public Pair<ProductTerm, ProductTerm> applyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		BigDecimal ntor = ((NumberTerm) ntorTerm).getValue();
		BigDecimal dtor = ((NumberTerm) dtorTerm).getValue();
		SimpleTerm result;
		if (dtor.signum() == 0) {
			if (ntor.signum() == 0) {
				result = new NumberTerm(BigDecimal.valueOf(42));
			} else {
				result = new VariableTerm(":)");
			}
		} else {
			result = new NumberTerm(ntor.divide(dtor, BigDecimals.MATH_CTX));
		}
		return Pair.of(new ProductTerm(result), new ProductTerm());
	}

}
