package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleMultiplyFractions extends RuleAbstractMultiply {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Multiply fractions").build();
	}

	@Override
	public String getIcon() {
		return "buttons/multiply_fraction";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm pre, SimpleTerm post) {
		return pre instanceof FractionTerm || post instanceof FractionTerm;
	}

	@Override
	public SimpleTerm applyOnPair(SimpleTerm pre, SimpleTerm post) {
		Expression preNtor, preDtor;
		if (pre instanceof FractionTerm) {
			FractionTerm fraction = (FractionTerm) pre;
			preNtor = fraction.getNumerator();
			preDtor = fraction.getDenominator();
		} else {
			preNtor = new Expression(Sign.PLUS, new ProductTerm(pre));
			preDtor = null;
		}
		Expression postNtor, postDtor;
		if (post instanceof FractionTerm) {
			FractionTerm fraction = (FractionTerm) post;
			postNtor = fraction.getNumerator();
			postDtor = fraction.getDenominator();
		} else {
			postNtor = new Expression(Sign.PLUS, new ProductTerm(post));
			postDtor = null;
		}

		ProductTerm ntor = new ProductTerm();
		if (preNtor.getTerms().size() == 1 && preNtor.getSigns().get(0) == Sign.PLUS) {
			ntor.getTerms().addAll(preNtor.getTerms().get(0).getTerms());
		} else {
			ntor.getTerms().add(new ExpressionTerm(preNtor));
		}
		if (postNtor.getTerms().size() == 1 && postNtor.getSigns().get(0) == Sign.PLUS) {
			ntor.getTerms().addAll(postNtor.getTerms().get(0).getTerms());
		} else {
			ntor.getTerms().add(new ExpressionTerm(postNtor));
		}

		ProductTerm dtor = new ProductTerm();
		if (preDtor != null) {
			if (preDtor.getTerms().size() == 1 && preDtor.getSigns().get(0) == Sign.PLUS) {
				dtor.getTerms().addAll(preDtor.getTerms().get(0).getTerms());
			} else {
				dtor.getTerms().add(new ExpressionTerm(preDtor));
			}
		}
		if (postDtor != null) {
			if (postDtor.getTerms().size() == 1 && postDtor.getSigns().get(0) == Sign.PLUS) {
				dtor.getTerms().addAll(postDtor.getTerms().get(0).getTerms());
			} else {
				dtor.getTerms().add(new ExpressionTerm(postDtor));
			}
		}

		return new FractionTerm(new Expression(Sign.PLUS, ntor), new Expression(Sign.PLUS, dtor));
	}

	@Override
	public void postApplyOnPair(RuleContext context) {
		RuleAbstractMultiply.doMultiplication(context.withOneExtra(0).withOneExtra(1));
		RuleAbstractMultiply.doMultiplication(context.withOneExtra(1).withOneExtra(1));
	}

}
