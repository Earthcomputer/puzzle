package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.ExponentialTerm;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleDivideIndices extends RuleAbstractFractionCancel {

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Divide indices").build();
	}

	@Override
	public String getIcon() {
		return "buttons/divide_indices";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		return RuleMultiplyIndices.getAsIndex(ntorTerm).getLeft()
				.isEquivalent(RuleMultiplyIndices.getAsIndex(dtorTerm).getLeft());
	}

	@Override
	public Pair<ProductTerm, ProductTerm> applyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm) {
		Pair<SimpleTerm, Expression> ntorPair = RuleMultiplyIndices.getAsIndex(ntorTerm);
		Pair<SimpleTerm, Expression> dtorPair = RuleMultiplyIndices.getAsIndex(dtorTerm);
		SimpleTerm base = ntorPair.getLeft();

		BigDecimal preNum = RuleMultiplyIndices.expressionAsNumber(ntorPair.getRight());
		BigDecimal postNum = RuleMultiplyIndices.expressionAsNumber(dtorPair.getRight());
		if (preNum == null || postNum == null) {
			Expression exponent = new Expression();
			exponent.getSigns().addAll(ntorPair.getRight().getSigns());
			exponent.getTerms().addAll(ntorPair.getRight().getTerms());
			if (dtorPair.getRight().getTerms().size() == 1) {
				exponent.getSigns().add(dtorPair.getRight().getSigns().get(0).negate());
				exponent.getTerms().add(dtorPair.getRight().getTerms().get(0));
			} else {
				exponent.getSigns().add(Sign.MINUS);
				exponent.getTerms().add(new ProductTerm(new ExpressionTerm(dtorPair.getRight())));
			}
			return Pair.of(new ProductTerm(new ExponentialTerm(base, exponent)), new ProductTerm());
		} else {
			BigDecimal newIndex = preNum.subtract(postNum);
			if (newIndex.signum() == 0) {
				return Pair.of(new ProductTerm(), new ProductTerm());
			} else if (newIndex.equals(BigDecimals.ONE)) {
				return Pair.of(new ProductTerm(base), new ProductTerm());
			} else if (newIndex.equals(BigDecimals.MINUS_ONE)) {
				return Pair.of(new ProductTerm(), new ProductTerm(base));
			}
			Expression exponent;
			if (newIndex.signum() == 1) {
				exponent = new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(newIndex)));
				return Pair.of(new ProductTerm(new ExponentialTerm(base, exponent)), new ProductTerm());
			} else {
				exponent = new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(newIndex.negate())));
				return Pair.of(new ProductTerm(), new ProductTerm(new ExponentialTerm(base, exponent)));
			}
		}
	}

}
