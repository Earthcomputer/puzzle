package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleDivideBothSides implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (context.getPathSize() != 4) {
			return false;
		}
		Expression expr = (Expression) context.getNodeFromStart(1);
		if (expr.getTerms().size() != 1) {
			return false;
		}
		return true;
	}

	@Override
	public void apply(RuleContext context) {
		SimpleTerm dividing = (SimpleTerm) context.getDeepestNode();

		ProductTerm thisSideProduct = (ProductTerm) context.getNodeFromStart(2);
		thisSideProduct.getTerms().remove(dividing);
		if (thisSideProduct.getTerms().isEmpty()) {
			thisSideProduct.getTerms().add(new NumberTerm(BigDecimals.ONE));
		}

		Expression expr = (Expression) context.getEquation().getChild(2 - context.getPathIndexFromStart(0));
		expr = new Expression(Sign.PLUS,
				new ProductTerm(new FractionTerm(expr, new Expression(Sign.PLUS, new ProductTerm(dividing)))));
		context.getEquation().setChild(2 - context.getPathIndexFromStart(0), expr);
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Divide both sides by ").append(context.getDeepestNode()).build();
	}

	@Override
	public String getIcon() {
		return "buttons/divide_both_sides";
	}

}
