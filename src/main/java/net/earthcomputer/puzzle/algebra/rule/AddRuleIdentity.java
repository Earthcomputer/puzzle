package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class AddRuleIdentity implements IAdditionRule {

	static boolean isZero(SimpleTerm term) {
		return term instanceof NumberTerm && ((NumberTerm) term).getValue().signum() == 0;
	}

	@Override
	public boolean canApply(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		return isZero(leftTerm) || isZero(rightTerm);
	}

	@Override
	public Pair<Sign, SimpleTerm> add(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		if (isZero(leftTerm)) {
			return Pair.of(Sign.PLUS, rightTerm);
		} else {
			return Pair.of(Sign.PLUS, leftTerm);
		}
	}

	@Override
	public Pair<Sign, SimpleTerm> subtract(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		if (isZero(leftTerm)) {
			return Pair.of(Sign.MINUS, rightTerm);
		} else {
			return Pair.of(Sign.PLUS, leftTerm);
		}
	}

}
