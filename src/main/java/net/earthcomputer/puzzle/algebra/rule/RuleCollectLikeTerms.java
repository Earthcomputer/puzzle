package net.earthcomputer.puzzle.algebra.rule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleCollectLikeTerms implements IAlgebraRule {

	private static final List<IAdditionRule> ADD_RULES = new ArrayList<>();

	static {
		ADD_RULES.add(new AddRuleIdentity());
		ADD_RULES.add(new AddRuleNumeric());
		ADD_RULES.add(new AddRuleFractions());
	}

	private static ToAdd findToAdd(RuleContext context) {
		Expression expr = (Expression) context.getDeepestNode();
		for (IAdditionRule rule : ADD_RULES) {
			for (int productIndexA = 0; productIndexA < expr.getTerms().size(); productIndexA++) {
				if (expr.getSigns().get(productIndexA).isAmbiguous()) {
					continue;
				}
				ProductTerm productA = expr.getTerms().get(productIndexA);
				for (int productIndexB = 0; productIndexB < expr.getTerms().size(); productIndexB++) {
					if (expr.getSigns().get(productIndexB).isAmbiguous()) {
						continue;
					}
					if (productIndexA != productIndexB) {
						ProductTerm productB = expr.getTerms().get(productIndexB);
						for (int simpleIndexA = productA.getTerms().size() - 1; simpleIndexA >= -1; simpleIndexA--) {
							SimpleTerm simpleA = simpleIndexA == -1 ? new NumberTerm(BigDecimals.ONE)
									: productA.getTerms().get(simpleIndexA);
							for (int simpleIndexB = productB.getTerms().size()
									- 1; simpleIndexB >= -1; simpleIndexB--) {
								SimpleTerm simpleB = simpleIndexB == -1 ? new NumberTerm(BigDecimals.ONE)
										: productB.getTerms().get(simpleIndexB);
								ProductTerm othersA = productA.copy();
								if (simpleIndexA != -1) {
									othersA.getTerms().remove(simpleIndexA);
								}
								ProductTerm othersB = productB.copy();
								if (simpleIndexB != -1) {
									othersB.getTerms().remove(simpleIndexB);
								}
								if (othersA.isEquivalent(othersB) && rule.canApply(simpleA, simpleB)) {
									return new ToAdd(rule, productIndexA, productIndexB, simpleIndexA, simpleIndexB);
								}
							}
						}
					}
				}
			}
		}

		if (expr.getTerms().size() != 1) {
			for (int zeroIndex = 0; zeroIndex < expr.getTerms().size(); zeroIndex++) {
				ProductTerm zeroProduct = expr.getTerms().get(zeroIndex);
				if (zeroProduct.getTerms().size() == 1 && AddRuleIdentity.isZero(zeroProduct.getTerms().get(0))) {
					return new ToAdd(zeroIndex);
				}
			}
		}

		return null;
	}

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof Expression)) {
			return false;
		}

		return findToAdd(context) != null;
	}

	@Override
	public void apply(RuleContext context) {
		Expression expr = (Expression) context.getDeepestNode();
		ToAdd toAdd = findToAdd(context);

		if (toAdd.isRemovingZero()) {
			expr.getSigns().remove(toAdd.getZeroIndex());
			expr.getTerms().remove(toAdd.getZeroIndex());
		} else {
			Sign signA, signB;
			ProductTerm productA, productB;
			if (toAdd.getProductIndexA() > toAdd.getProductIndexB()) {
				signA = expr.getSigns().remove(toAdd.getProductIndexA());
				signB = expr.getSigns().remove(toAdd.getProductIndexB());
				productA = expr.getTerms().remove(toAdd.getProductIndexA());
				productB = expr.getTerms().remove(toAdd.getProductIndexB());
			} else {
				signB = expr.getSigns().remove(toAdd.getProductIndexB());
				signA = expr.getSigns().remove(toAdd.getProductIndexA());
				productB = expr.getTerms().remove(toAdd.getProductIndexB());
				productA = expr.getTerms().remove(toAdd.getProductIndexA());
			}
			boolean negate = false;
			if (signA == Sign.MINUS) {
				negate = true;
				signA = Sign.PLUS;
				signB = signB.negate();
			}
			SimpleTerm simpleA = toAdd.getSimpleIndexA() == -1 ? new NumberTerm(BigDecimals.ONE)
					: productA.getTerms().get(toAdd.getSimpleIndexA());
			SimpleTerm simpleB = toAdd.getSimpleIndexB() == -1 ? new NumberTerm(BigDecimals.ONE)
					: productB.getTerms().get(toAdd.getSimpleIndexB());
			ProductTerm resultProduct = productA.copy();
			if (toAdd.getSimpleIndexA() != -1) {
				resultProduct.getTerms().remove(toAdd.getSimpleIndexA());
			}
			SimpleTerm resultSimple;
			Sign resultSign;
			if (signB == Sign.PLUS) {
				Pair<Sign, SimpleTerm> pair = toAdd.getRule().add(simpleA, simpleB);
				resultSimple = pair.getRight();
				resultSign = pair.getLeft();
			} else {
				Pair<Sign, SimpleTerm> pair = toAdd.getRule().subtract(simpleA, simpleB);
				resultSimple = pair.getRight();
				resultSign = pair.getLeft();
			}
			if (negate) {
				resultSign = resultSign.negate();
			}

			boolean zero = false, one = false;
			if (resultSimple instanceof NumberTerm) {
				BigDecimal num = ((NumberTerm) resultSimple).getValue();
				if (num.signum() == 0) {
					zero = true;
				} else if (num.equals(BigDecimals.ONE)) {
					one = true;
				}
			}

			if (!zero) {
				if (!one) {
					resultProduct.getTerms().add(0, resultSimple);
				} else if (resultProduct.getTerms().isEmpty()) {
					resultProduct.getTerms().add(new NumberTerm(BigDecimals.ONE));
				}
				int index = Math.min(toAdd.getProductIndexA(), toAdd.getProductIndexB());
				expr.getSigns().add(index, resultSign);
				expr.getTerms().add(index, resultProduct);
			} else if (expr.getTerms().isEmpty()) {
				expr.getSigns().add(Sign.PLUS);
				expr.getTerms().add(new ProductTerm(new NumberTerm(BigDecimals.ZERO)));
			}
		}
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Collect like terms").build();
	}

	@Override
	public String getIcon() {
		return "buttons/collect_like_terms";
	}

	private static class ToAdd {
		private IAdditionRule rule;
		private int productIndexA;
		private int productIndexB;
		private int simpleIndexA;
		private int simpleIndexB;
		private boolean isRemovingZero = false;

		public ToAdd(int zeroIndex) {
			this.productIndexA = zeroIndex;
			this.isRemovingZero = true;
		}

		public ToAdd(IAdditionRule rule, int productIndexA, int productIndexB, int simpleIndexA, int simpleIndexB) {
			this.rule = rule;
			this.productIndexA = productIndexA;
			this.productIndexB = productIndexB;
			this.simpleIndexA = simpleIndexA;
			this.simpleIndexB = simpleIndexB;
		}

		public IAdditionRule getRule() {
			return rule;
		}

		public int getProductIndexA() {
			return productIndexA;
		}

		public int getProductIndexB() {
			return productIndexB;
		}

		public int getSimpleIndexA() {
			return simpleIndexA;
		}

		public int getSimpleIndexB() {
			return simpleIndexB;
		}

		public int getZeroIndex() {
			return productIndexA;
		}

		public boolean isRemovingZero() {
			return isRemovingZero;
		}
	}

}
