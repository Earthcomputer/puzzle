package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public class RuleSubtractFromBothSides implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (context.getPathSize() != 3) {
			return false;
		}
		if (!(context.getDeepestNode() instanceof ProductTerm)) {
			return false;
		}
		Expression expression = (Expression) context.getNodeFromStart(1);
		return expression.getSigns().get(context.getPathIndexFromStart(1) / 2) == Sign.PLUS;
	}

	@Override
	public void apply(RuleContext context) {
		Expression oldExpr = (Expression) context.getNodeFromStart(1);
		Expression newExpr = (Expression) context.getEquation().getChild(2 - context.getPathIndexFromStart(0));
		oldExpr.getSigns().remove(context.getPathIndexFromStart(1) / 2);
		oldExpr.getTerms().remove(context.getPathIndexFromStart(1) / 2);
		if (oldExpr.getTerms().isEmpty()) {
			oldExpr.getSigns().add(Sign.PLUS);
			oldExpr.getTerms().add(new ProductTerm(new NumberTerm(BigDecimals.ZERO)));
		}
		newExpr.getSigns().add(Sign.MINUS);
		newExpr.getTerms().add((ProductTerm) context.getDeepestNode());
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Subtract ").append(context.getDeepestNode()).append(" from both sides")
				.build();
	}

	@Override
	public String getIcon() {
		return "buttons/subtract_from_both_sides";
	}

}
