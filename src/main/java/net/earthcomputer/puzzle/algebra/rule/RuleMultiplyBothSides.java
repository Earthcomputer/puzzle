package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public class RuleMultiplyBothSides implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (context.getPathSize() != 5) {
			return false;
		}
		Expression expr = (Expression) context.getNodeFromStart(1);
		if (expr.getTerms().size() != 1) {
			return false;
		}
		if (!(context.getNodeFromEnd(1) instanceof FractionTerm)) {
			return false;
		}
		return context.getPathIndexFromEnd(1) == 1;
	}

	@Override
	public void apply(RuleContext context) {
		ProductTerm otherSideProduct = new ProductTerm();

		Expression expr = (Expression) context.getDeepestNode();
		if (expr.getTerms().size() == 1 && expr.getSigns().get(0) == Sign.PLUS) {
			otherSideProduct.getTerms().addAll(expr.getTerms().get(0).getTerms());
		} else {
			otherSideProduct.getTerms().add(new ExpressionTerm(expr));
		}

		expr = (Expression) context.getEquation().getChild(2 - context.getPathIndexFromStart(0));
		if (expr.getTerms().size() == 1 && expr.getSigns().get(0) == Sign.PLUS) {
			otherSideProduct.getTerms().addAll(expr.getTerms().get(0).getTerms());
		} else {
			otherSideProduct.getTerms().add(new ExpressionTerm(expr));
		}

		context.getEquation().setChild(2 - context.getPathIndexFromStart(0),
				new Expression(Sign.PLUS, otherSideProduct));

		Expression ntor = ((FractionTerm) context.getNodeFromEnd(1)).getNumerator();
		IAlgebraRule.insertExpression(ntor, (Expression) context.getNodeFromEnd(3), context.getPathIndexFromEnd(3) / 2,
				context.getPathIndexFromEnd(2));
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Multiply both sides by ").append(context.getDeepestNode()).build();
	}

	@Override
	public String getIcon() {
		return "buttons/multiply_both_sides";
	}

}
