package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public abstract class RuleAbstractFractionCancel implements IAlgebraRule {

	private static final List<RuleAbstractFractionCancel> CANCEL_RULES = new ArrayList<>();

	public RuleAbstractFractionCancel() {
		CANCEL_RULES.add(this);
	}

	public static void doFractionCancel(RuleContext context) {
		boolean changed = true;
		outerLoop: while (changed && !context.isInvalid()) {
			changed = false;
			for (RuleAbstractFractionCancel rule : CANCEL_RULES) {
				if (rule.canApplyAutomatically() && rule.canApply(context)) {
					rule.apply(context);
					changed = true;
					continue outerLoop;
				}
			}
		}
	}

	private static Pair<Sign, List<SimpleTerm>> getMultipliedTerms(Expression expr) {
		if (expr.getTerms().size() == 1) {
			return Pair.of(expr.getSigns().get(0), expr.getTerms().get(0).getTerms());
		} else {
			return Pair.of(Sign.PLUS, Util.arrayListOf(new ExpressionTerm(expr)));
		}
	}

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof FractionTerm)) {
			return false;
		}
		FractionTerm fraction = (FractionTerm) context.getDeepestNode();

		Expression ntor = fraction.getNumerator();
		Expression dtor = fraction.getDenominator();

		List<SimpleTerm> ntorProduct = getMultipliedTerms(ntor).getRight();
		List<SimpleTerm> dtorProduct = getMultipliedTerms(dtor).getRight();

		for (SimpleTerm ntorTerm : ntorProduct) {
			for (SimpleTerm dtorTerm : dtorProduct) {
				if (canApplyOnPair(ntorTerm, dtorTerm)) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void apply(RuleContext context) {
		FractionTerm fraction = (FractionTerm) context.getDeepestNode();

		Expression ntor = fraction.getNumerator();
		Expression dtor = fraction.getDenominator();

		Pair<Sign, List<SimpleTerm>> pair = getMultipliedTerms(ntor);
		Sign ntorSign = pair.getLeft();
		List<SimpleTerm> ntorProduct = pair.getRight();
		pair = getMultipliedTerms(dtor);
		Sign dtorSign = pair.getLeft();
		List<SimpleTerm> dtorProduct = pair.getRight();
		Sign resultSign = ntorSign.multiply(dtorSign);

		for (int ntorIndex = 0; ntorIndex < ntorProduct.size(); ntorIndex++) {
			SimpleTerm ntorTerm = ntorProduct.get(ntorIndex);
			for (int dtorIndex = 0; dtorIndex < dtorProduct.size(); dtorIndex++) {
				SimpleTerm dtorTerm = dtorProduct.get(dtorIndex);
				if (canApplyOnPair(ntorTerm, dtorTerm)) {
					Pair<ProductTerm, ProductTerm> result = applyOnPair(ntorTerm, dtorTerm);
					ntorProduct.remove(ntorIndex);
					ntorProduct.addAll(ntorIndex, result.getLeft().getTerms());
					if (ntorProduct.isEmpty()) {
						ntorProduct.add(new NumberTerm(BigDecimals.ONE));
					}
					dtorProduct.remove(dtorIndex);
					dtorProduct.addAll(dtorIndex, result.getRight().getTerms());
					fraction.setNumerator(new Expression(resultSign, new ProductTerm(ntorProduct)));
					fraction.setDenominator(new Expression(Sign.PLUS, new ProductTerm(dtorProduct)));
					if (dtorProduct.isEmpty()) {
						Expression expr;
						if (ntorProduct.size() == 1 && ntorProduct.get(0) instanceof ExpressionTerm
								&& resultSign == Sign.PLUS) {
							expr = ((ExpressionTerm) ntorProduct.get(0)).getExpression();
						} else {
							expr = new Expression(resultSign, new ProductTerm(ntorProduct));
						}
						IAlgebraRule.insertExpression(expr, (Expression) context.getNodeFromEnd(2),
								context.getPathIndexFromEnd(2) / 2, context.getPathIndexFromEnd(1));
					} else {
						postApplyOnPair(context);
					}
					return;
				}
			}
		}
	}

	protected boolean canApplyAutomatically() {
		return true;
	}

	public abstract boolean canApplyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm);

	public abstract Pair<ProductTerm, ProductTerm> applyOnPair(SimpleTerm ntorTerm, SimpleTerm dtorTerm);

	public void postApplyOnPair(RuleContext context) {
	}

}
