package net.earthcomputer.puzzle.algebra.rule;

import javax.swing.JOptionPane;

import net.earthcomputer.puzzle.algebra.DateMessageGenerator;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.VariableTerm;

public class RuleSmileyFace implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof VariableTerm)) {
			return false;
		}
		VariableTerm var = (VariableTerm) context.getDeepestNode();
		return var.getName().equals(":)");
	}

	@Override
	public void apply(RuleContext context) {
		JOptionPane.showMessageDialog(null, DateMessageGenerator.generateWittyMessage(), "Puzzle",
				JOptionPane.PLAIN_MESSAGE);
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append(":)").build();
	}

	@Override
	public String getIcon() {
		return "buttons/smiley_face";
	}

}
