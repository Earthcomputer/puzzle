package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.ast.IAstNode;

public class RuleContext {

	private Equation equation;
	private List<Integer> pathIndexes;
	private List<IAstNode> nodes;

	private RuleContext(Equation equation, List<Integer> path, List<IAstNode> nodesInPath) {
		this.equation = equation;
		this.pathIndexes = path;
		this.nodes = nodesInPath;
	}

	public static RuleContext create(Equation equation, List<Integer> path) {
		List<IAstNode> nodesInPath = new ArrayList<>(path.size() + 1);
		nodesInPath.add(equation);
		IAstNode node = equation;
		for (int index : path) {
			node = node.getChild(index);
			nodesInPath.add(node);
		}
		return new RuleContext(equation, path, nodesInPath);
	}

	public Equation getEquation() {
		return equation;
	}

	public IAstNode getDeepestNode() {
		return getNodeFromEnd(0);
	}

	public int getPathIndexFromStart(int index) {
		return pathIndexes.get(index);
	}

	public IAstNode getNodeFromStart(int index) {
		return nodes.get(index);
	}

	public int getPathIndexFromEnd(int index) {
		return getPathIndexFromStart(pathIndexes.size() - index);
	}

	public IAstNode getNodeFromEnd(int index) {
		return getNodeFromStart(pathIndexes.size() - index);
	}

	public int getPathSize() {
		return nodes.size();
	}

	public boolean isInvalid() {
		IAstNode node = equation;
		for (int i = 0; i < pathIndexes.size(); i++) {
			int pathIndex = pathIndexes.get(i);
			if (pathIndex < 0 || pathIndex >= node.getChildCount()) {
				return true;
			}
			IAstNode child = node.getChild(pathIndex);
			if (child != nodes.get(i + 1)) {
				return true;
			}
			node = child;
		}
		return false;
	}

	public RuleContext withOneExtra(int index) {
		List<Integer> pathIndexes = new ArrayList<>(this.pathIndexes);
		pathIndexes.add(index);
		List<IAstNode> nodes = new ArrayList<>(this.nodes);
		nodes.add(getDeepestNode().getChild(index));
		return new RuleContext(equation, pathIndexes, nodes);
	}

	public RuleContext withOneLess() {
		List<Integer> pathIndexes = new ArrayList<>(this.pathIndexes);
		pathIndexes.remove(pathIndexes.size() - 1);
		List<IAstNode> nodes = new ArrayList<>(this.nodes);
		nodes.remove(nodes.size() - 1);
		return new RuleContext(equation, pathIndexes, nodes);
	}

	public RuleContext withDifferentRoot(Equation newRoot) {
		List<Integer> pathIndexes = new ArrayList<>(this.pathIndexes);
		return RuleContext.create(newRoot, pathIndexes);
	}

}
