package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class AddRuleFractions implements IAdditionRule {

	@Override
	public boolean canApply(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		return leftTerm instanceof FractionTerm || rightTerm instanceof FractionTerm;
	}

	@Override
	public Pair<Sign, SimpleTerm> add(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		return reduce(leftTerm, rightTerm, Sign.PLUS);
	}

	@Override
	public Pair<Sign, SimpleTerm> subtract(SimpleTerm leftTerm, SimpleTerm rightTerm) {
		return reduce(leftTerm, rightTerm, Sign.MINUS);
	}

	private static Pair<Sign, SimpleTerm> reduce(SimpleTerm leftTerm, SimpleTerm rightTerm, Sign sign) {
		Expression leftNtor, leftDtor;
		if (leftTerm instanceof FractionTerm) {
			FractionTerm fraction = (FractionTerm) leftTerm;
			leftNtor = fraction.getNumerator();
			leftDtor = fraction.getDenominator();
		} else {
			leftNtor = new Expression(Sign.PLUS, new ProductTerm(leftTerm));
			leftDtor = null;
		}
		Expression rightNtor, rightDtor;
		if (rightTerm instanceof FractionTerm) {
			FractionTerm fraction = (FractionTerm) rightTerm;
			rightNtor = fraction.getNumerator();
			rightDtor = fraction.getDenominator();
		} else {
			rightNtor = new Expression(Sign.PLUS, new ProductTerm(rightTerm));
			rightDtor = null;
		}

		Expression newNtor = new Expression();

		ProductTerm newProduct = new ProductTerm();
		if (rightDtor != null) {
			if (rightDtor.getTerms().size() == 1 && rightDtor.getSigns().get(0) == Sign.PLUS) {
				newProduct.getTerms().addAll(rightDtor.getTerms().get(0).copy().getTerms());
			} else {
				newProduct.getTerms().add(new ExpressionTerm(rightDtor.copy()));
			}
		}
		if (leftNtor.getTerms().size() == 1 && leftNtor.getSigns().get(0) == Sign.PLUS) {
			newProduct.getTerms().addAll(leftNtor.getTerms().get(0).getTerms());
		} else {
			newProduct.getTerms().add(new ExpressionTerm(leftNtor));
		}
		newNtor.getSigns().add(Sign.PLUS);
		newNtor.getTerms().add(newProduct);

		newProduct = new ProductTerm();
		if (leftDtor != null) {
			if (leftDtor.getTerms().size() == 1 && leftDtor.getSigns().get(0) == Sign.PLUS) {
				newProduct.getTerms().addAll(leftDtor.getTerms().get(0).copy().getTerms());
			} else {
				newProduct.getTerms().add(new ExpressionTerm(leftDtor.copy()));
			}
		}
		if (rightNtor.getTerms().size() == 1 && rightNtor.getSigns().get(0) == Sign.PLUS) {
			newProduct.getTerms().addAll(rightNtor.getTerms().get(0).getTerms());
		} else {
			newProduct.getTerms().add(new ExpressionTerm(rightNtor));
		}
		newNtor.getSigns().add(sign);
		newNtor.getTerms().add(newProduct);

		newProduct = new ProductTerm();
		if (leftDtor != null) {
			if (leftDtor.getTerms().size() == 1 && leftDtor.getSigns().get(0) == Sign.PLUS) {
				newProduct.getTerms().addAll(leftDtor.getTerms().get(0).copy().getTerms());
			} else {
				newProduct.getTerms().add(new ExpressionTerm(leftDtor.copy()));
			}
		}
		if (rightDtor != null) {
			if (rightDtor.getTerms().size() == 1 && rightDtor.getSigns().get(0) == Sign.PLUS) {
				newProduct.getTerms().addAll(rightDtor.getTerms().get(0).copy().getTerms());
			} else {
				newProduct.getTerms().add(new ExpressionTerm(rightDtor.copy()));
			}
		}
		Expression newDtor = new Expression(Sign.PLUS, newProduct);

		return Pair.of(Sign.PLUS, new FractionTerm(newNtor, newDtor));
	}

}
