package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public interface IAlgebraRule {

	boolean canApply(RuleContext context);

	void apply(RuleContext context);

	FormattedText getDescription(RuleContext context);

	String getIcon();

	public static void insertExpression(Expression fromExpr, Expression intoExpr, int indexInExpr, int indexInProduct) {
		ProductTerm product = intoExpr.getTerms().get(indexInExpr);
		if (product.getTerms().size() == 1) {
			Sign sign = intoExpr.getSigns().get(indexInExpr);
			intoExpr.getSigns().remove(indexInExpr);
			intoExpr.getTerms().remove(indexInExpr);
			if (sign == Sign.PLUS) {
				intoExpr.getSigns().addAll(indexInExpr, fromExpr.getSigns());
				intoExpr.getTerms().addAll(indexInExpr, fromExpr.getTerms());
			} else if (fromExpr.getTerms().size() == 1) {
				intoExpr.getSigns().add(sign.multiply(fromExpr.getSigns().get(0)));
				intoExpr.getTerms().add(fromExpr.getTerms().get(0));
			} else {
				intoExpr.getSigns().add(sign);
				intoExpr.getTerms().add(new ProductTerm(new ExpressionTerm(fromExpr)));
			}
		} else {
			if (fromExpr.getTerms().size() == 1 && fromExpr.getSigns().get(0) == Sign.PLUS) {
				product.getTerms().remove(indexInProduct);
				product.getTerms().addAll(indexInProduct, fromExpr.getTerms().get(0).getTerms());
			} else {
				product.getTerms().set(indexInProduct, new ExpressionTerm(fromExpr));
			}
		}
	}

}
