package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleDivideEveryTerm implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (context.getPathSize() != 4) {
			return false;
		}
		if (!(context.getNodeFromEnd(1) instanceof ProductTerm)) {
			return false;
		}
		return true;
	}

	@Override
	public void apply(RuleContext context) {
		SimpleTerm termToDivide = (SimpleTerm) context.getDeepestNode().copy();

		List<RuleContext> contextsToDivide = new ArrayList<>();
		for (int i = 0; i <= 2; i += 2) {
			Expression expr = (Expression) context.getEquation().getChild(i);
			for (int j = expr.getTerms().size() - 1; j >= 0; j--) {
				contextsToDivide.add(RuleContext.create(context.getEquation(), Util.arrayListOf(i, j * 2 + 1)));
			}
		}

		for (RuleContext toDivide : contextsToDivide) {
			if (toDivide.isInvalid()) {
				continue;
			}
			ProductTerm product = (ProductTerm) toDivide.getDeepestNode();
			Expression ntor, dtor;
			if (product.getTerms().size() == 1 && product.getTerms().get(0) instanceof FractionTerm) {
				FractionTerm fraction = (FractionTerm) product.getTerms().get(0);
				ntor = fraction.getNumerator();
				dtor = fraction.getDenominator();
			} else {
				ntor = new Expression(Sign.PLUS, product);
				dtor = new Expression(Sign.PLUS, new ProductTerm(new NumberTerm(BigDecimals.ONE)));
			}

			if (dtor.getTerms().size() == 1 && dtor.getSigns().get(0) == Sign.PLUS) {
				dtor.getTerms().get(0).getTerms().add(termToDivide.copy());
			} else {
				dtor = new Expression(Sign.PLUS, new ProductTerm(termToDivide, new ExpressionTerm(dtor)));
			}

			toDivide.getNodeFromEnd(1).setChild(toDivide.getPathIndexFromEnd(1),
					new ProductTerm(new FractionTerm(ntor, dtor)));
			toDivide = toDivide.withOneLess().withOneExtra(toDivide.getPathIndexFromEnd(1)).withOneExtra(0);

			RuleAbstractMultiply.doMultiplication(toDivide.withOneExtra(1).withOneExtra(1));
			if (!toDivide.isInvalid()) {
				RuleAbstractFractionCancel.doFractionCancel(toDivide);
				toDivide = toDivide.withOneLess();
				if (!toDivide.isInvalid()) {
					RuleAbstractMultiply.doMultiplication(toDivide);
				}
			}
		}
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Divide every term by ").append(context.getDeepestNode()).build();
	}

	@Override
	public String getIcon() {
		return "buttons/divide_terms";
	}

}
