package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public class RuleExtractSignFromFraction implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof FractionTerm)) {
			return false;
		}
		if (!(context.getNodeFromEnd(1) instanceof ProductTerm)) {
			return false;
		}
		ProductTerm product = (ProductTerm) context.getNodeFromEnd(1);
		if (product.getTerms().size() != 1) {
			return false;
		}
		if (!(context.getNodeFromEnd(2) instanceof Expression)) {
			return false;
		}
		return true;
	}

	@Override
	public void apply(RuleContext context) {
		Expression parentExpr = (Expression) context.getNodeFromEnd(2);
		int indexInParentExpr = context.getPathIndexFromEnd(2) / 2;
		Expression childExpr = ((FractionTerm) context.getDeepestNode()).getNumerator();

		for (int i = 0; i < childExpr.getSigns().size(); i++) {
			childExpr.getSigns().set(i, childExpr.getSigns().get(i).negate());
		}

		parentExpr.getSigns().set(indexInParentExpr, parentExpr.getSigns().get(indexInParentExpr).negate());
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Extract ").append(Sign.MINUS).append(" sign from fraction").build();
	}

	@Override
	public String getIcon() {
		return "buttons/extract_sign_from_fraction";
	}

}
