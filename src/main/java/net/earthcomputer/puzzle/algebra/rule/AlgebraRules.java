package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import net.earthcomputer.puzzle.algebra.Pair;

public class AlgebraRules {

	private static final List<IAlgebraRule> RULES = new ArrayList<>();

	static {
		RULES.add(new RuleAddToBothSides());
		RULES.add(new RuleSubtractFromBothSides());
		RULES.add(new RuleCollectLikeTerms());
		RULES.add(new RuleMultiplyEveryTerm());
		RULES.add(new RuleDivideEveryTerm());
		RULES.add(new RuleMultiplyBothSides());
		RULES.add(new RuleDivideBothSides());
		RULES.add(new RuleDistributeSign());
		RULES.add(new RuleExtractSignFromFraction());
		RULES.add(new RuleNegateEquation());
		RULES.add(new RuleExchangeSides());
		RULES.add(new RuleEvaluateFunction());
		RULES.add(new RuleNumericalIndices());

		RULES.add(new RuleMultiplicativeIdentity());
		RULES.add(new RuleNumericalMultiply());
		RULES.add(new RuleMultiplyFractions());
		RULES.add(new RuleMultiplyIndices());
		RULES.add(new RuleExpandBrackets());

		RULES.add(new RuleApproxFraction());
		RULES.add(new RuleFractionCancelNumeric());
		RULES.add(new RuleNumericalDivide());
		RULES.add(new RuleDivideFractions());
		RULES.add(new RuleDivideIndices());

		RULES.add(new RuleSmileyFace());
	}

	private AlgebraRules() {
	}

	public static List<IAlgebraRule> getApplicableRules(RuleContext context) {
		return RULES.stream().filter(rule -> rule.canApply(context)).collect(Collectors.toList());
	}

	public static List<Pair<IAlgebraRule, RuleContext>> getApplicableRules(List<RuleContext> contexts) {
		if (contexts == null) {
			return Collections.emptyList();
		}
		return contexts.stream()
				.flatMap(context -> getApplicableRules(context).stream().map(rule -> Pair.of(rule, context)))
				.collect(Collectors.toList());
	}

}
