package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public class RuleDistributeSign implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (!(context.getDeepestNode() instanceof ExpressionTerm)) {
			return false;
		}
		if (!(context.getNodeFromEnd(1) instanceof ProductTerm)) {
			return false;
		}
		ProductTerm product = (ProductTerm) context.getNodeFromEnd(1);
		if (product.getTerms().size() != 1) {
			return false;
		}
		return context.getNodeFromEnd(2) instanceof Expression;
	}

	@Override
	public void apply(RuleContext context) {
		Expression parentExpr = (Expression) context.getNodeFromEnd(2);
		int indexInParentExpr = context.getPathIndexFromEnd(2) / 2;
		Expression childExpr = ((ExpressionTerm) context.getDeepestNode()).getExpression();

		Sign sign = parentExpr.getSigns().get(indexInParentExpr);
		for (int i = 0; i < childExpr.getSigns().size(); i++) {
			childExpr.getSigns().set(i, sign.multiply(childExpr.getSigns().get(i)));
		}

		parentExpr.getSigns().remove(indexInParentExpr);
		parentExpr.getTerms().remove(indexInParentExpr);
		parentExpr.getSigns().addAll(childExpr.getSigns());
		parentExpr.getTerms().addAll(childExpr.getTerms());
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Distribute ")
				.append(((Expression) context.getNodeFromEnd(2)).getSigns().get(context.getPathIndexFromEnd(2) / 2))
				.append(" sign").build();
	}

	@Override
	public String getIcon() {
		return "buttons/distribute_sign";
	}

}
