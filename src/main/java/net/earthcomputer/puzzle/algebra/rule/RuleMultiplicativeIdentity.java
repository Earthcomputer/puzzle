package net.earthcomputer.puzzle.algebra.rule;

import net.earthcomputer.puzzle.algebra.BigDecimals;
import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;

public class RuleMultiplicativeIdentity extends RuleAbstractMultiply {

	private static boolean isZero(SimpleTerm term) {
		return term instanceof NumberTerm && ((NumberTerm) term).getValue().signum() == 0;
	}

	private static boolean isOne(SimpleTerm term) {
		return term instanceof NumberTerm && ((NumberTerm) term).getValue().equals(BigDecimals.ONE);
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Use multiplicative identity").build();
	}

	@Override
	public String getIcon() {
		return "buttons/multiplicative_identity";
	}

	@Override
	public boolean canApplyOnPair(SimpleTerm pre, SimpleTerm post) {
		return isZero(pre) || isOne(pre) || isZero(post) || isOne(post);
	}

	@Override
	public SimpleTerm applyOnPair(SimpleTerm pre, SimpleTerm post) {
		if (isZero(pre) || isZero(post)) {
			return new NumberTerm(BigDecimals.ZERO);
		} else if (isOne(pre)) {
			if (isOne(post)) {
				return null;
			}
			return post;
		} else {
			return pre;
		}
	}

}
