package net.earthcomputer.puzzle.algebra.rule;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;

public class RuleMultiplyEveryTerm implements IAlgebraRule {

	@Override
	public boolean canApply(RuleContext context) {
		if (context.getPathSize() != 5) {
			return false;
		}
		if (!(context.getNodeFromEnd(1) instanceof FractionTerm)) {
			return false;
		}
		return context.getPathIndexFromEnd(1) == 1;
	}

	@Override
	public void apply(RuleContext context) {
		FractionTerm fraction = (FractionTerm) context.getNodeFromEnd(1);
		Expression dtor = fraction.getDenominator();

		List<RuleContext> contextsToMultiply = new ArrayList<>();
		for (int i = 0; i <= 2; i += 2) {
			Expression expr = (Expression) context.getEquation().getChild(i);
			for (int j = expr.getTerms().size() - 1; j >= 0; j--) {
				contextsToMultiply.add(RuleContext.create(context.getEquation(), Util.arrayListOf(i, j * 2 + 1)));
			}
		}

		for (RuleContext toMultiply : contextsToMultiply) {
			if (toMultiply.isInvalid()) {
				continue;
			}
			ProductTerm product = (ProductTerm) toMultiply.getDeepestNode();

			if (dtor.getTerms().size() == 1 && dtor.getSigns().get(0) == Sign.PLUS) {
				product.getTerms().addAll(0, dtor.getTerms().get(0).copy().getTerms());
			} else {
				product.getTerms().add(0, new ExpressionTerm(dtor.copy()));
			}

			RuleAbstractMultiply.doMultiplication(toMultiply);

			if (!toMultiply.isInvalid() && product.getTerms().size() == 1
					&& product.getTerms().get(0) instanceof FractionTerm) {
				RuleAbstractFractionCancel.doFractionCancel(toMultiply.withOneExtra(0));
				if (!toMultiply.isInvalid()) {
					RuleAbstractMultiply.doMultiplication(toMultiply);
				}
			}
		}
	}

	@Override
	public FormattedText getDescription(RuleContext context) {
		return FormattedText.builder().append("Multiply every term by ").append(context.getDeepestNode()).build();
	}

	@Override
	public String getIcon() {
		return "buttons/multiply_terms";
	}

}
