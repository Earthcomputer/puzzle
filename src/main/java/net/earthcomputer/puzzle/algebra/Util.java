package net.earthcomputer.puzzle.algebra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import net.earthcomputer.puzzle.algebra.ast.ExponentialTerm;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;
import net.earthcomputer.puzzle.algebra.ast.TermEndType;

/**
 * This class contains general utilities
 */
public class Util {

	private Util() {
	}

	/**
	 * Creates an ArrayList containing the given elements
	 */
	@SafeVarargs
	public static <T> ArrayList<T> arrayListOf(T... things) {
		ArrayList<T> list = new ArrayList<>(things.length);
		Collections.addAll(list, things);
		return list;
	}

	/**
	 * Creates an ArrayList containing count occurrences of thing
	 */
	public static <T> ArrayList<T> arrayListWithNCopies(int count, T thing) {
		ArrayList<T> list = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			list.add(thing);
		}
		return list;
	}

	/**
	 * Creates a HashSet containing the given elements
	 */
	@SafeVarargs
	public static <T> HashSet<T> hashSetOf(T... things) {
		HashSet<T> set = new HashSet<>(things.length);
		Collections.addAll(set, things);
		return set;
	}

	/**
	 * Removes the elements from start (inclusive) to end (exclusive)
	 */
	public static void removeRange(List<?> list, int start, int end) {
		int len = end - start;
		for (int i = 0; i < len; i++) {
			list.remove(start);
		}
	}

	/**
	 * Removes the elements from start (inclusive) to end (exclusive)
	 */
	public static void removeRange(Iterable<?> iterable, int start, int end) {
		Iterator<?> itr = iterable.iterator();
		for (int i = 0; i < start; i++) {
			itr.next();
		}
		for (int i = start; i < end; i++) {
			itr.next();
			itr.remove();
		}
	}

	/**
	 * Same as List.get(int), except works for any iterable and runs in linear
	 * time (not constant time), as the iterable may not be random access
	 */
	public static <T> T linearGet(Iterable<T> iterable, int index) {
		Iterator<T> itr = iterable.iterator();
		for (int i = 0; i < index; i++) {
			itr.next();
		}
		return itr.next();
	}

	/**
	 * Creates an index out of bounds exception
	 */
	public static IndexOutOfBoundsException indexOutOfBounds(int index, int size) {
		return new IndexOutOfBoundsException("index " + index + ", size " + size);
	}

	/**
	 * Whether the program should juxtapose two terms for multiplication
	 */
	public static boolean shouldJuxtapose(SimpleTerm before, SimpleTerm after) {
		TermEndType beforeType = before.getEndType();
		TermEndType afterType = after.getStartType();

		switch (afterType) {
		case PARENTHESIS:
		case LETTER:
		case SURD:
			return true;
		case NUMBER:
			return false;
		case FRACTION:
			while (beforeType == TermEndType.EXPONENT) {
				before = ((ExponentialTerm) before).getBase();
				beforeType = before.getEndType();
			}
			return beforeType == TermEndType.PARENTHESIS;
		case EXPONENT:
			// EXPONENT should only be an end type
			throw new AssertionError("Wrong usage of TermEndType.EXPONENT");
		default:
			throw new AssertionError();
		}
	}

	/**
	 * Whether the user is allowed to juxtapose two terms to signify
	 * multiplication. This differs from the above method because sometimes it
	 * is allowed to juxtapose even if it is not considered good practice.
	 */
	public static boolean isJuxtapositionAllowed(SimpleTerm before, SimpleTerm after) {
		TermEndType beforeType = before.getEndType();
		TermEndType afterType = after.getStartType();

		if (beforeType == TermEndType.NUMBER) {
			if (afterType == TermEndType.NUMBER) {
				// Obviously "23" is not "2*3"
				return false;
			}
			if (afterType == TermEndType.FRACTION) {
				// Otherwise we get confused with mixed fractions
				return false;
			}
		}
		if (beforeType == TermEndType.FRACTION) {
			if (afterType == TermEndType.FRACTION) {
				// Otherwise we're joining the divider line
				return false;
			}
		}

		return true;
	}

}
