package net.earthcomputer.puzzle.algebra;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

/**
 * This class is designed to perform functions normally in the Math class on
 * BigDecimals.
 */
public class BigDecimals {

	// Rounding constants
	public static final int INTERNAL_DECIMAL_PLACES = 100;
	public static final int DISPLAYED_DECIMAL_PLACES = 50;
	public static final MathContext MATH_CTX = new MathContext(INTERNAL_DECIMAL_PLACES, RoundingMode.HALF_UP);
	public static final BigDecimal EPSILON = BigDecimal.valueOf(1, INTERNAL_DECIMAL_PLACES);

	// Integer constants
	public static final BigDecimal MINUS_ONE = BigDecimal.valueOf(-1);
	public static final BigDecimal ZERO = BigDecimal.ZERO;
	public static final BigDecimal ONE = BigDecimal.ONE;
	public static final BigDecimal TWO = BigDecimal.valueOf(2);
	public static final BigDecimal THREE = BigDecimal.valueOf(3);
	public static final BigDecimal FOUR = BigDecimal.valueOf(4);
	public static final BigDecimal TEN = BigDecimal.TEN;

	// Irrational constants
	// @formatter:off
	public static final BigDecimal PI = new BigDecimal(
			"3.1415926535"
			+ "8979323846"
			+ "2643383279"
			+ "5028841971"
			+ "6939937510"
			+ "5820974944"
			+ "5923078164"
			+ "0628620899"
			+ "8628034825"
			+ "3421170679");
	public static final BigDecimal E = new BigDecimal(
			"2.7182818284"
			+ "5904523536"
			+ "0287471352"
			+ "6624977572"
			+ "4709369995"
			+ "9574966967"
			+ "6277240766"
			+ "3035354759"
			+ "4571382178"
			+ "5251664274");
	// @formatter:on
	public static final BigDecimal HALF_PI = PI.divide(TWO, MATH_CTX);
	public static final BigDecimal TWO_PI = PI.multiply(TWO, MATH_CTX);
	public static final BigDecimal SQRT_TWO = sqrt(TWO);

	private BigDecimals() {
	}

	// Factorization

	/**
	 * Finds a pair of integers a and b such that |x - a/b| < 10^-90
	 */
	public static Pair<BigDecimal, BigDecimal> approximateToFraction(BigDecimal x) {
		if (isInteger(x)) {
			return Pair.of(x, ONE);
		}

		boolean negative = false;
		if (x.signum() == -1) {
			negative = true;
			x = x.negate();
		}

		// calculate the continued fraction of x
		Deque<BigDecimal> continuedFraction = new ArrayDeque<>();
		BigDecimal fractionalPart;
		int count = 0;
		while (!isInteger(x)) {
			BigDecimal integerPart = floor(x);
			continuedFraction.push(integerPart);
			fractionalPart = x.subtract(integerPart);
			x = reciprocal(fractionalPart);
			if (count++ > 1000) {
				// I guess we're accurate enough...
				break;
			}
		}
		BigDecimal integerPart = round(x);
		continuedFraction.push(integerPart);

		// convert continued fraction to a single fraction
		BigDecimal r3 = ONE;
		BigDecimal r2 = continuedFraction.pop();
		while (!continuedFraction.isEmpty()) {
			BigDecimal r1 = continuedFraction.pop();
			r1 = r1.multiply(r2, MATH_CTX).add(r3);
			r3 = r2;
			r2 = r1;
		}

		if (negative) {
			r2 = r2.negate();
		}

		return Pair.of(round(r2), round(r3));
	}

	/**
	 * Finds the greatest common divisor of a and b. If a and b are
	 * non-integers, the result may be larger than the inputs, because the
	 * result is then the greatest common divisor of two integers with the same
	 * ratio. If a = b = 0, then gcd(a, b) = 0, for convenience.
	 * 
	 * Domain: a, b >= 0
	 */
	public static BigDecimal gcd(BigDecimal a, BigDecimal b) {
		if (a.signum() == -1 || b.signum() == -1) {
			throw new ArithmeticException("Can only find the greatest common divisor of positive numbers");
		}

		BigDecimal toMultiplyAnswerBy = ONE;
		if (!isInteger(a) || !isInteger(b)) {
			Pair<BigDecimal, BigDecimal> ratio = approximateToFraction(a.divide(b, MATH_CTX));
			toMultiplyAnswerBy = a.divide(ratio.getLeft(), MATH_CTX);
			a = ratio.getLeft();
			b = ratio.getRight();
		}

		if (a.signum() == 0) {
			return b;
		}
		if (b.signum() == 0) {
			return a;
		}

		BigInteger intA = a.toBigInteger();
		BigInteger intB = b.toBigInteger();

		BigInteger twoPowK = BigInteger.ONE;
		while (!intA.equals(intB)) {
			if (intA.testBit(0)) {
				if (intB.testBit(0)) {
					if (intA.compareTo(intB) > 0) {
						intA = intA.subtract(intB).shiftRight(1);
					} else {
						BigInteger temp = intB.subtract(intA).shiftRight(1);
						intB = intA;
						intA = temp;
					}
				} else {
					intB = intB.shiftRight(1);
				}
			} else {
				if (intB.testBit(0)) {
					intA = intA.shiftRight(1);
				} else {
					intA = intA.shiftRight(1);
					intB = intB.shiftRight(1);
					twoPowK = twoPowK.shiftLeft(1);
				}
			}
		}

		BigInteger gcd = twoPowK.multiply(intB);
		return new BigDecimal(gcd).multiply(toMultiplyAnswerBy);
	}

	/**
	 * Finds the lowest common multiple of a and b. The formula used for this is
	 * lcm(a, b) = ab/gcd(a, b).
	 * 
	 * Domain: a, b >= 0, if a = b then a != 0
	 */
	public static BigDecimal lcm(BigDecimal a, BigDecimal b) {
		return a.multiply(b, MATH_CTX).divide(gcd(a, b), MATH_CTX);
	}

	// Logs and exponentials

	/**
	 * Finds the square root of n
	 * 
	 * Domain: positive numbers and zero
	 */
	public static BigDecimal sqrt(BigDecimal n) {
		switch (n.signum()) {
		case -1:
			throw new ArithmeticException("Cannot square root a negative number");
		case 0:
			return n;
		case 1:
			// Initial guess = 1
			// f(x) = x^2 - n
			// f'(x) = 2x
			return newtonRaphson(ONE, x -> x.multiply(x, MATH_CTX).subtract(n), x -> x.multiply(TWO, MATH_CTX));
		default:
			throw new AssertionError();
		}
	}

	/**
	 * Finds the nth root of underRoot
	 * 
	 * Domain: n is not zero; if underRoot is negative, the reciprocal of n must
	 * be an integer
	 */
	public static BigDecimal nthRoot(BigDecimal n, BigDecimal underRoot) {
		if (n.signum() == 0) {
			throw new ArithmeticException("Cannot find zeroeth root");
		}

		// nth root x = x^(1/n)
		BigDecimal reciprocal = reciprocal(n);

		if (underRoot.signum() == -1 && !isInteger(reciprocal)) {
			if (n.equals(TWO)) {
				throw new ArithmeticException("Cannot square root a negative number");
			}
			throw new ArithmeticException(
					"Cannot compute the nth root of a negative number where n = 1/k where k is an integer");
		}

		return pow(underRoot, reciprocal);
	}

	/**
	 * Finds e^x
	 * 
	 * Domain: all values of x
	 */
	public static BigDecimal exp(BigDecimal x) {
		// Split e^x into e^(i+d) where i is an integer.
		BigDecimal i = floor(x);
		BigDecimal d = x.subtract(i);

		// 1 + d + (d^2)/(2!) + (d^3)/(3!) + ...
		BigDecimal expD = taylorSeries(ONE, ONE, (index, n) -> n.multiply(d, MATH_CTX),
				(iteration, denominator) -> denominator.multiply(iteration, MATH_CTX));

		// If i is negative, the answer is the reciprocal, then make i positive
		boolean reciprocal = i.signum() == -1;
		i = i.abs();

		// We use our own stack here to avoid using a recursive function. For a
		// recursive version of the function, see the project write-up.

		// The function is mathematically defined as follows:
		// f(0) = 1
		// f(i) = f(i/2)^2 if i is even
		// f(i) = e*f(i-1) if i is odd
		// Domain: positive integers and zero
		Deque<Boolean> shouldSquare = new ArrayDeque<>();
		while (i.signum() == 1) {
			if (i.remainder(TWO).signum() == 0) {
				i = i.divide(TWO);
				shouldSquare.push(true);
			} else {
				i = i.subtract(ONE);
				shouldSquare.push(false);
			}
		}
		BigDecimal expI = ONE;
		while (!shouldSquare.isEmpty()) {
			if (shouldSquare.pop()) {
				expI = expI.multiply(expI, MATH_CTX);
			} else {
				expI = expI.multiply(E, MATH_CTX);
			}
		}

		// do the reciprocal
		if (reciprocal) {
			expI = reciprocal(expI);
		}

		// using the identity e^(i+d) = e^i * e^d
		return expI.multiply(expD, MATH_CTX);
	}

	private static final BigDecimal LN_SQRT_THRESHOLD = BigDecimal.valueOf(0.01);

	/**
	 * Finds the natural logarithm of x
	 * 
	 * Domain: positive numbers
	 */
	public static BigDecimal ln(BigDecimal x) {
		if (x.signum() != 1) {
			throw new ArithmeticException("Cannot log a number <= 0");
		}

		// ln(1) = 0
		if (x.equals(ONE)) {
			return ZERO;
		}

		// The taylor series only converges for x < 1, so for x > 1 we need to
		// use the identity ln(1/x) = -ln(x)

		BigDecimal toMultiplyBy = ONE;
		if (x.compareTo(ONE) > 0) {
			toMultiplyBy = MINUS_ONE;
			x = reciprocal(x);
		}

		// When x << 1, the taylor series is slow to converge, so use ln(x) = 2ln(sqrt(x))
		while (x.compareTo(LN_SQRT_THRESHOLD) < 0) {
			x = sqrt(x);
			toMultiplyBy = toMultiplyBy.add(toMultiplyBy);
		}

		// ln(x + 1) = x - (x^2)/(2) + (x^3)/(3) - (x^4)/(4) + ...
		x = x.subtract(ONE);
		BigDecimal negX = x.negate();
		BigDecimal lnX = taylorSeries(x, ONE, (i, n) -> n.multiply(negX, MATH_CTX), (i, d) -> d.add(ONE));

		// do the multiplication
		lnX = toMultiplyBy.multiply(lnX, MATH_CTX);

		return lnX;
	}

	/**
	 * Computes the log base b of x
	 * 
	 * Domain: x > 0; b > 0; b != 1
	 */
	public static BigDecimal log(BigDecimal x, BigDecimal b) {
		if (b.signum() != 1) {
			throw new ArithmeticException("Cannot do log base negative or zero");
		}

		if (b.equals(ONE)) {
			throw new ArithmeticException("Cannot do log base 1");
		}

		// Use the identity log_b(x) = ln(x)/ln(b)
		return ln(x).divide(ln(b), MATH_CTX);
	}

	/**
	 * Computes x^n
	 * 
	 * Domain: if x is negative, n must be an integer
	 */
	public static BigDecimal pow(BigDecimal x, BigDecimal n) {
		// Treat negative numbers separately, otherwise the identity below fails
		// with ln of a negative number.
		if (x.signum() == -1) {
			if (isInteger(n)) {
				// A negative number raised to an even power is the positive
				// version of the power. Otherwise it's the negative version.
				BigDecimal xPowN = pow(x.negate(), n);
				if (n.remainder(TWO).intValue() == 1) {
					xPowN = xPowN.negate();
				}
				return xPowN;
			} else {
				throw new ArithmeticException("Cannot raise a negative number to a non-integer power");
			}
		}

		// If x = 0 we cannot compute ln(x), so 0 is a special case
		if (x.signum() == 0) {
			if (n.signum() == 1) {
				return ZERO;
			} else {
				throw new ArithmeticException("Cannot raise zero to a non-positive power");
			}
		}

		// Use the identity x^n = e^(nln(x))
		return exp(ln(x).multiply(n, MATH_CTX));
	}

	// Trig functions

	/**
	 * Computes the sine of x in radians
	 * 
	 * Domain: all values of x
	 */
	public static BigDecimal sin(BigDecimal x) {
		// Get x between 0 <= x < pi/2 so that the taylor series converges
		// quicker

		boolean negate = false;

		// Use the identity sin(-x) = -sin(x)
		if (x.signum() == -1) {
			x = x.negate();
			negate = true;
		}

		// Use periodicy
		BigDecimal[] qr = x.divideAndRemainder(HALF_PI);
		BigDecimal r = qr[1];
		// now 0 <= r < pi/2

		// Adjust r based on which quadrant we're in
		switch (qr[0].remainder(FOUR).intValue()) {
		case 0: // 0 <= x < pi/2 (mod 2pi)
			break;
		case 1: // pi/2 <= x < pi (mod 2pi)
			r = HALF_PI.subtract(r);
			break;
		case 2: // pi <= x < 3pi/2 (mod 2pi)
			negate = !negate;
			break;
		case 3: // 3pi/2 <= x < 2pi (mod 2pi)
			r = HALF_PI.subtract(r);
			negate = !negate;
			break;
		default:
			throw new AssertionError();
		}

		// sin(r) = r - (r^3)/(3!) + (r^5)/(5!) - (r^7) / (7!) + ...
		BigDecimal minusRSquared = r.multiply(r, MATH_CTX).negate();
		BigDecimal sinR = taylorSeries(r, ONE, (i, n) -> n.multiply(minusRSquared, MATH_CTX), (i, d) -> {
			i = i.add(i);
			return d.multiply(i, MATH_CTX).multiply(i.add(ONE), MATH_CTX);
		});

		// Do the negation
		BigDecimal sinX;
		if (negate) {
			sinX = sinR.negate();
		} else {
			sinX = sinR;
		}

		return sinX;
	}

	/**
	 * Computs the cosine of x in radians
	 * 
	 * Domain: all values of x
	 */
	public static BigDecimal cos(BigDecimal x) {
		// Get x between 0 <= x < pi/2 so that the taylor series converges
		// quicker

		boolean negate = false;

		// Use the identity cos(-x) = cos(x)
		if (x.signum() == -1) {
			x = x.negate();
		}

		// Use periodicy
		BigDecimal[] qr = x.divideAndRemainder(HALF_PI);
		BigDecimal r = qr[1];
		// now 0 <= r < pi/2

		// Adjust r based on the quadrant we're in
		switch (qr[0].remainder(FOUR).intValue()) {
		case 0: // 0 <= x < pi/2 (mod 2pi)
			break;
		case 1: // pi/2 <= x < pi (mod 2pi)
			r = HALF_PI.subtract(r);
			negate = true;
			break;
		case 2: // pi <= x < 3pi/2 (mod 2pi)
			negate = true;
			break;
		case 3: // 3pi/2 <= x < 2pi (mod 2pi)
			r = HALF_PI.subtract(r);
			break;
		default:
			throw new AssertionError();
		}

		// cos(r) = 1 - (r^2)/(2!) + (r^4)/(4!) - (r^6)/(6!) + ...
		BigDecimal minusRSquared = r.multiply(r, MATH_CTX).negate();
		BigDecimal cosR = taylorSeries(ONE, ONE, (i, n) -> n.multiply(minusRSquared, MATH_CTX), (i, d) -> {
			i = i.add(i);
			return d.multiply(i, MATH_CTX).multiply(i.subtract(ONE), MATH_CTX);
		});

		// Do the negation
		BigDecimal cosX;
		if (negate) {
			cosX = cosR.negate();
		} else {
			cosX = cosR;
		}

		return cosX;
	}

	/**
	 * Computes the tangent of x in radians
	 * 
	 * Domain: x != pi/2 (mod pi)
	 */
	public static BigDecimal tan(BigDecimal x) {
		// Use the identity tan(x) = sin(x) / cos(x)
		// Check the domain by checking if cos(x) = 0, because we can't divide
		// by 0
		BigDecimal cosX = cos(x);
		if (convertToAnswer(cosX).signum() == 0) {
			throw new ArithmeticException("Cannot compute tan(x) for x = pi/2 + kpi, where k is an integer");
		}

		// Get sin(x) from cos(x) using the identity sin^2(x) + cos^2(x) = 1
		BigDecimal sinX = sqrt(ONE.subtract(cosX.multiply(cosX, MATH_CTX)));

		// Fix sign of sin(x), since sqrt can only give positive values
		if (x.signum() == 1) {
			if (x.remainder(TWO_PI).compareTo(PI) > 0) {
				sinX = sinX.negate();
			}
		} else {
			if (x.abs().remainder(TWO_PI).compareTo(PI) < 0) {
				sinX = sinX.negate();
			}
		}

		return sinX.divide(cosX, MATH_CTX);
	}

	/**
	 * Computes the inverse sine of x, returns the answer in radians
	 * 
	 * Domain: -1 <= x <= 1
	 * 
	 * Range: -pi/2 <= asin(x) <= pi/2
	 */
	public static BigDecimal asin(BigDecimal x) {
		if (x.abs().compareTo(ONE) > 0) {
			throw new ArithmeticException("Cannot compute asin(x) for |x| > 1");
		}

		// Special cases because the below identity doesn't work for these
		if (x.equals(ONE)) {
			return HALF_PI;
		}
		if (x.equals(MINUS_ONE)) {
			return HALF_PI.negate();
		}

		// Use the identity asin(x) = atan(x/(sqrt(1-x^2)))
		return atan(x.divide(sqrt(ONE.subtract(x.multiply(x, MATH_CTX))), MATH_CTX));
	}

	/**
	 * Computes the inverse cosine of x, returns the answer in radians
	 * 
	 * Domain: -1 <= x <= 1
	 * 
	 * Range: 0 <= acos(x) <= 2pi
	 */
	public static BigDecimal acos(BigDecimal x) {
		if (x.abs().compareTo(ONE) > 0) {
			throw new ArithmeticException("Cannot compute acos(x) for |x| > 1");
		}

		// Use the identity acos(x) = pi/2 - asin(x)
		return HALF_PI.subtract(asin(x));
	}

	private static final BigDecimal ATAN_DOUBLE_ANGLE_THRESHOLD = BigDecimal.valueOf(0.8);

	/**
	 * Computes the inverse tangent of x, returns the answer in radians
	 * 
	 * Domain: all values of x
	 * 
	 * Range: -pi/2 <= atan(x) <= pi/2
	 */
	public static BigDecimal atan(BigDecimal x) {
		// The taylor series only works for -1 <= x <= 1, so use the identity
		// atan(x) = pi/2 - atan(1/x)

		if (x.abs().compareTo(ONE) > 0) {
			if (x.signum() == 1) {
				return HALF_PI.subtract(atan(reciprocal(x)));
			} else {
				return atan(MINUS_ONE.divide(x, MATH_CTX)).subtract(HALF_PI);
			}
		}

		// The taylor series is not as convergent close to 1, use double-angle formula to reduce x
		if (x.abs().compareTo(ATAN_DOUBLE_ANGLE_THRESHOLD) > 0) {
			BigDecimal atanReduced = atan(x.divide(ONE.add(sqrt(ONE.add(x.multiply(x, MATH_CTX)))), MATH_CTX));
			return atanReduced.add(atanReduced);
		}

		// atan(x) = x - (x^3)/(3) + (x^5)/(5) - (x^7)/(7) + ...
		BigDecimal minusXSquared = x.multiply(x, MATH_CTX).negate();
		return taylorSeries(x, ONE, (i, n) -> n.multiply(minusXSquared, MATH_CTX), (i, d) -> d.add(TWO));
	}

	// Generic infinite series

	/**
	 * Finds a root of the input function f(x), that is, a value for x where
	 * f(x) = 0. fPrime(x) is the derivative function of f(x).
	 */
	private static BigDecimal newtonRaphson(BigDecimal initialGuess, UnaryOperator<BigDecimal> f,
			UnaryOperator<BigDecimal> fPrime) {
		// x_(i+1) = x_i - f(x_i)/f'(x_i)
		BigDecimal x = initialGuess;
		BigDecimal difference;
		do {
			BigDecimal gradient = fPrime.apply(x);
			if (gradient.signum() == 0) {
				throw new ArithmeticException("Newton-Raphson iteration hit gradient 0");
			}
			difference = f.apply(x).divide(gradient, MATH_CTX);
			x = x.subtract(difference);
		} while (difference.abs().compareTo(EPSILON) > 0);
		return x;
	}

	/**
	 * Computes an infinite sum of fractions to a certain precision. These
	 * fractions should get smaller and smaller so that they eventually fall
	 * below a threshold value.
	 * 
	 * The numerator and denominator generators exist in order to compute the
	 * terms more quickly. They take two inputs: the iteration number and the
	 * previous numerator/denominator, in that order.
	 */
	private static BigDecimal taylorSeries(BigDecimal firstNumerator, BigDecimal firstDenominator,
			BiFunction<BigDecimal, BigDecimal, BigDecimal> numeratorGenerator,
			BiFunction<BigDecimal, BigDecimal, BigDecimal> denominatorGenerator) {
		BigDecimal iteration = ZERO;
		BigDecimal numerator = firstNumerator;
		BigDecimal denominator = firstDenominator;
		BigDecimal term;
		BigDecimal result = firstNumerator.divide(firstDenominator, MATH_CTX);
		do {
			iteration = iteration.add(ONE);
			numerator = numeratorGenerator.apply(iteration, numerator);
			denominator = denominatorGenerator.apply(iteration, denominator);
			term = numerator.divide(denominator, MATH_CTX);
			result = result.add(term);
		} while (term.abs().compareTo(EPSILON) > 0);
		return result;
	}

	// Miscellaneous simple helper functions

	/**
	 * Computes the reciprocal, 1/n.
	 * 
	 * Domain: x != 0
	 */
	public static BigDecimal reciprocal(BigDecimal value) {
		return ONE.divide(value, MATH_CTX);
	}

	/**
	 * Rounds a number to remove the internal decimal places
	 */
	public static BigDecimal convertToAnswer(BigDecimal value) {
		return value.setScale(DISPLAYED_DECIMAL_PLACES, RoundingMode.HALF_UP);
	}

	/**
	 * Tests a value for whether it's an integer
	 */
	public static boolean isInteger(BigDecimal value) {
		value = convertToAnswer(value);
		return value.signum() == 0 || value.scale() <= 0 || value.stripTrailingZeros().scale() <= 0;
	}

	/**
	 * Rounds the input to the nearest integer
	 */
	public static BigDecimal round(BigDecimal val) {
		return val.setScale(0, RoundingMode.HALF_UP);
	}

	/**
	 * Returns the largest integer less than or equal to the input
	 */
	public static BigDecimal floor(BigDecimal val) {
		return val.setScale(0, RoundingMode.FLOOR);
	}

	/**
	 * Returns the smallest integer greater than or equal to the input
	 */
	public static BigDecimal ceil(BigDecimal val) {
		return val.setScale(0, RoundingMode.CEILING);
	}

	/**
	 * Converts a number to a displayable string
	 */
	public static String toString(BigDecimal value) {
		return convertToAnswer(value).stripTrailingZeros().toString();
	}

}
