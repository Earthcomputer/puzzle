package net.earthcomputer.puzzle.algebra;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;

public class BuiltInFunctions {

	private static final Map<String, UnaryOperator<BigDecimal>> FUNCTIONS = new HashMap<>();
	private static final Set<String> TRIG_FUNCTIONS;

	static {
		FUNCTIONS.put("sin", BigDecimals::sin);
		FUNCTIONS.put("cos", BigDecimals::cos);
		FUNCTIONS.put("tan", BigDecimals::tan);
		FUNCTIONS.put("arcsin", BigDecimals::asin);
		FUNCTIONS.put("arccos", BigDecimals::acos);
		FUNCTIONS.put("arctan", BigDecimals::atan);
		FUNCTIONS.put("cosec", x -> BigDecimals.reciprocal(BigDecimals.sin(x)));
		FUNCTIONS.put("sec", x -> BigDecimals.reciprocal(BigDecimals.cos(x)));
		FUNCTIONS.put("cot", x -> BigDecimals.reciprocal(BigDecimals.tan(x)));
		FUNCTIONS.put("arccosec", x -> BigDecimals.asin(BigDecimals.reciprocal(x)));
		FUNCTIONS.put("arcsec", x -> BigDecimals.acos(BigDecimals.reciprocal(x)));
		FUNCTIONS.put("arccot", x -> BigDecimals.atan(BigDecimals.reciprocal(x)));
		FUNCTIONS.put("sinh", x -> {
			BigDecimal expX = BigDecimals.exp(x);
			return expX.subtract(BigDecimals.reciprocal(expX)).divide(BigDecimals.TWO, BigDecimals.MATH_CTX);
		});
		FUNCTIONS.put("cosh", x -> {
			BigDecimal expX = BigDecimals.exp(x);
			return expX.add(BigDecimals.reciprocal(expX)).divide(BigDecimals.TWO, BigDecimals.MATH_CTX);
		});
		FUNCTIONS.put("tanh", x -> {
			BigDecimal expXSquared = BigDecimals.exp(x.add(x));
			return expXSquared.subtract(BigDecimals.ONE).divide(expXSquared.add(BigDecimals.ONE), BigDecimals.MATH_CTX);
		});
		FUNCTIONS.put("arsinh", x -> BigDecimals.ln(x.add(BigDecimals.sqrt(x.multiply(x).add(BigDecimals.ONE)))));
		FUNCTIONS.put("arcosh", x -> BigDecimals.ln(x.add(BigDecimals.sqrt(x.multiply(x).subtract(BigDecimals.ONE)))));
		FUNCTIONS.put("artanh",
				x -> BigDecimals.ln(BigDecimals.ONE.add(x).divide(BigDecimals.ONE.subtract(x), BigDecimals.MATH_CTX))
						.divide(BigDecimals.TWO, BigDecimals.MATH_CTX));
		FUNCTIONS.put("cosech", x -> {
			BigDecimal expX = BigDecimals.exp(x);
			return BigDecimals.TWO.divide(expX.subtract(BigDecimals.reciprocal(expX)), BigDecimals.MATH_CTX);
		});
		FUNCTIONS.put("sech", x -> {
			BigDecimal expX = BigDecimals.exp(x);
			return BigDecimals.TWO.divide(expX.add(BigDecimals.reciprocal(expX)), BigDecimals.MATH_CTX);
		});
		FUNCTIONS.put("coth", x -> {
			BigDecimal expXSquared = BigDecimals.exp(x.add(x));
			return expXSquared.add(BigDecimals.ONE).divide(expXSquared.subtract(BigDecimals.ONE), BigDecimals.MATH_CTX);
		});
		FUNCTIONS.put("arcosech", x -> {
			switch (x.signum()) {
			case 1:
				return BigDecimals.ln(
						BigDecimals.ONE.add(BigDecimals.sqrt(BigDecimals.ONE.add(x.multiply(x, BigDecimals.MATH_CTX))))
								.divide(x, BigDecimals.MATH_CTX));
			case -1:
				return BigDecimals.ln(BigDecimals.ONE
						.subtract(BigDecimals.sqrt(BigDecimals.ONE.add(x.multiply(x, BigDecimals.MATH_CTX))))
						.divide(x, BigDecimals.MATH_CTX));
			case 0:
				throw new ArithmeticException("arccosech(0) is undefined");
			default:
				throw new AssertionError();
			}
		});
		FUNCTIONS.put("arsech", x -> {
			if (x.compareTo(BigDecimals.ZERO) > 0) {
				return BigDecimals.ln(BigDecimals.ONE
						.add(BigDecimals.sqrt(BigDecimals.ONE.subtract(x.multiply(x, BigDecimals.MATH_CTX))))
						.divide(x, BigDecimals.MATH_CTX));
			}
			if (x.compareTo(BigDecimals.MINUS_ONE) < 0) {
				return BigDecimals.ln(BigDecimals.ONE
						.subtract(BigDecimals.sqrt(BigDecimals.ONE.subtract(x.multiply(x, BigDecimals.MATH_CTX))))
						.divide(x, BigDecimals.MATH_CTX));
			}
			throw new ArithmeticException("arcsech(x) for -1 <= x <= 0 is undefined");
		});
		FUNCTIONS.put("arcoth",
				x -> BigDecimals.ln(x.add(BigDecimals.ONE).divide(x.subtract(BigDecimals.ONE), BigDecimals.MATH_CTX))
						.divide(BigDecimals.TWO, BigDecimals.MATH_CTX));
		TRIG_FUNCTIONS = new HashSet<>(FUNCTIONS.keySet());

		FUNCTIONS.put("exp", BigDecimals::exp);
		FUNCTIONS.put("ln", BigDecimals::ln);
		FUNCTIONS.put("log", x -> BigDecimals.log(x, BigDecimals.TEN));
	}

	private BuiltInFunctions() {
	}

	public static Set<String> allBuiltInFunctions() {
		return FUNCTIONS.keySet();
	}

	public static boolean isTrigFunction(String function) {
		return TRIG_FUNCTIONS.contains(function);
	}

	public static BigDecimal evaluate(String functionName, BigDecimal input) {
		return FUNCTIONS.get(functionName.toLowerCase(Locale.ENGLISH)).apply(input);
	}

}
