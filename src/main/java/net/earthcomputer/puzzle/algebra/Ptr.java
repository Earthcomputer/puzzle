package net.earthcomputer.puzzle.algebra;

import java.util.AbstractList;
import java.util.List;
import java.util.Objects;

public final class Ptr<T> {

	private T value;

	public Ptr() {
	}

	public Ptr(T value) {
		this.value = value;
	}

	public T get() {
		return value;
	}

	public void set(T value) {
		this.value = value;
	}

	public <U> List<U> toSingletonList() {
		return new AbstractList<U>() {
			@SuppressWarnings("unchecked")
			@Override
			public U get(int index) {
				if (index != 0) {
					throw Util.indexOutOfBounds(index, 1);
				}
				return (U) value;
			}

			@SuppressWarnings("unchecked")
			@Override
			public U set(int index, U value) {
				if (index != 0) {
					throw Util.indexOutOfBounds(index, 1);
				}
				U old = value;
				Ptr.this.value = (T) value;
				return old;
			}

			@Override
			public int size() {
				return 1;
			}
		};
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Ptr && equals((Ptr<?>) other);
	}

	public boolean equals(Ptr<?> other) {
		return Objects.equals(value, other.value);
	}

	@Override
	public String toString() {
		return "Ptr{" + value + "}";
	}

}
