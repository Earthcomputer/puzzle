package net.earthcomputer.puzzle.ui;

import java.util.IdentityHashMap;
import java.util.Map;

import net.earthcomputer.puzzle.algebra.soltree.SolutionTree;
import net.earthcomputer.puzzle.algebra.soltree.SolutionTreeNode;

public class SolutionTreeUI {

	public static final int RENDERED_RADIUS = 16;
	public static final int RENDERED_ROW_HEIGHT = 48;

	private SolutionTreeUI() {
	}

	public static RenderingMetrics generateRenderingMetrics(SolutionTree tree) {
		return generateRenderingMetrics(tree.getRootNode());
	}

	private static RenderingMetrics generateRenderingMetrics(SolutionTreeNode thisNode) {
		int totalWidth = 0;
		int totalHeight = 0;
		Map<SolutionTreeNode, Point> nodePoints = new IdentityHashMap<>();
		for (SolutionTreeNode child : thisNode.getChildren()) {
			RenderingMetrics childMetrics = generateRenderingMetrics(child);
			final int totalWidth_f = totalWidth;
			childMetrics.getNodePoints().forEach((node, point) -> nodePoints.put(node,
					new Point(totalWidth_f + point.x, RENDERED_ROW_HEIGHT + point.y)));
			totalWidth += childMetrics.getWidth();
			totalHeight = Math.max(totalHeight, childMetrics.getHeight());
		}
		totalHeight += RENDERED_ROW_HEIGHT;
		if (totalWidth < RENDERED_RADIUS * 2) {
			totalWidth = RENDERED_RADIUS * 2;
		}
		nodePoints.put(thisNode, new Point(totalWidth / 2, RENDERED_ROW_HEIGHT / 2));
		return new RenderingMetrics(totalWidth, totalHeight, nodePoints);
	}

	public static void draw(SolutionTree tree, ICanvas canvas, RenderingMetrics metrics) {
		draw(tree, tree.getRootNode(), canvas, metrics);
	}

	private static void draw(SolutionTree tree, SolutionTreeNode thisNode, ICanvas canvas, RenderingMetrics metrics) {
		Point thisPoint = metrics.getNodePoints().get(thisNode);
		for (SolutionTreeNode child : thisNode.getChildren()) {
			Point childPoint = metrics.getNodePoints().get(child);
			canvas.setColor(ICanvas.BLACK);
			canvas.drawLine(thisPoint, childPoint);
			draw(tree, child, canvas, metrics);
		}

		canvas.setColor(thisNode == tree.getWorkingNode() ? ICanvas.MAGENTA : ICanvas.BLUE);
		canvas.fillCircle(thisPoint, RENDERED_RADIUS);
	}

	public static class RenderingMetrics {
		private int width;
		private int height;
		private Map<SolutionTreeNode, Point> nodePoints;

		public RenderingMetrics(int width, int height, Map<SolutionTreeNode, Point> nodePoints) {
			this.width = width;
			this.height = height;
			this.nodePoints = nodePoints;
		}

		public int getWidth() {
			return width;
		}

		public int getHeight() {
			return height;
		}

		public Map<SolutionTreeNode, Point> getNodePoints() {
			return nodePoints;
		}
	}

}
