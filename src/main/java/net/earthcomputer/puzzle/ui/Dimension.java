package net.earthcomputer.puzzle.ui;

/**
 * Contains a width and a height
 */
public class Dimension {

	public final int w;
	public final int h;

	public Dimension(int w, int h) {
		this.w = w;
		this.h = h;
	}

	public int getWidth() {
		return w;
	}

	public int getHeight() {
		return h;
	}

	public int getArea() {
		return w * h;
	}

	@Override
	public int hashCode() {
		return w + 31 * h;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Dimension && equals((Dimension) other);
	}

	public boolean equals(Dimension other) {
		return w == other.w && h == other.h;
	}

	@Override
	public String toString() {
		return "(" + w + "," + h + ")";
	}
}
