package net.earthcomputer.puzzle.ui;

/**
 * Represents a rectangle
 */
public class Box {

	public final int x;
	public final int y;
	public final int w;
	public final int h;

	public Box(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	public Box(Point location, Dimension size) {
		this(location.x, location.y, size.w, size.h);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return w;
	}

	public int getHeight() {
		return h;
	}

	public Box add(int x, int y) {
		return new Box(this.x + x, this.y + y, w, h);
	}

	public Box add(Point p) {
		return new Box(this.x + p.x, this.y + p.y, w, h);
	}

	public Box subtract(int x, int y) {
		return new Box(this.x - x, this.y - y, w, h);
	}

	public Box subtract(Point p) {
		return new Box(this.x - p.x, this.y - p.y, w, h);
	}

	public Box multiply(int scale) {
		return new Box(this.x * scale, this.y * scale, this.w * scale, this.h * scale);
	}

	public Box multiply(float scale) {
		return new Box((int) (this.x * scale), (int) (this.y * scale), (int) (this.w * scale), (int) (this.h * scale));
	}

	public Box multiply(double scale) {
		return new Box((int) (this.x * scale), (int) (this.y * scale), (int) (this.w * scale), (int) (this.h * scale));
	}

	public Box divide(int divisor) {
		return new Box(this.x / divisor, this.y / divisor, this.w / divisor, this.h / divisor);
	}

	public Box divide(float divisor) {
		return new Box((int) (this.x / divisor), (int) (this.y / divisor), (int) (this.w / divisor),
				(int) (this.h / divisor));
	}

	public Box divide(double divisor) {
		return new Box((int) (this.x / divisor), (int) (this.y / divisor), (int) (this.w / divisor),
				(int) (this.h / divisor));
	}

	public Point getLocation() {
		return new Point(x, y);
	}

	public Dimension getSize() {
		return new Dimension(w, h);
	}

	public int getArea() {
		return w * h;
	}

	public boolean contains(int x, int y) {
		return x >= this.x && y >= this.y && x < this.x + this.w && y < this.y + this.h;
	}

	public boolean contains(Point p) {
		return contains(p.x, p.y);
	}

	@Override
	public int hashCode() {
		int hash = x;
		hash = 31 * hash + y;
		hash = 31 * hash + w;
		hash = 31 * hash + h;
		return hash;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Box && equals((Box) other);
	}

	public boolean equals(Box other) {
		return x == other.x && y == other.y && w == other.w && h == other.h;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + "," + w + "," + h + ")";
	}

}
