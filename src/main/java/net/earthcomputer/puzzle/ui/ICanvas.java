package net.earthcomputer.puzzle.ui;

/**
 * An interface to the internal graphics engine, which may be different
 * depending on the platform
 */
public interface ICanvas {

	// Colors
	public static final int BLACK = 0xff000000;
	public static final int WHITE = 0xffffffff;
	public static final int RED = 0xffff0000;
	public static final int GREEN = 0xff00ff00;
	public static final int BLUE = 0xff0000ff;
	public static final int YELLOW = 0xffffff00;
	public static final int CYAN = 0xff00ffff;
	public static final int MAGENTA = 0xffff00ff;

	// Transformations

	/**
	 * Pushes a transformation matrix onto the matrix stack. All the
	 * transformations which happen after this will be reversed when the matrix
	 * is popped from the stack again. Note that transformations must be
	 * specified in reverse order.
	 */
	void pushMatrix();

	/**
	 * Pops the top transformation matrix from the matrix stack. All the
	 * transformations represented by that matrix will be reversed and hence not
	 * apply any more
	 */
	void popMatrix();

	/**
	 * Translates subsequent rendering by the vector (x, y)
	 */
	void translate(int x, int y);

	/**
	 * Scales subsequent rendering from the origin with scale factor factor
	 */
	default void scale(float factor) {
		scale(factor, factor);
	}

	/**
	 * Scales subsequent rendering from the origin with horizontal scale factor
	 * factorx and vertical scale factor factory
	 */
	void scale(float factorx, float factory);

	/**
	 * Rotates subsequent rendering counter-clockwise around the origin by theta
	 * radians
	 */
	void rotate(double angle);

	// Colors

	/**
	 * Sets the color of subsequent geometric shapes to the given packed hex
	 * color
	 */
	default void setColor(int argb) {
		setColor((argb & 0x00ff0000) >>> 16, (argb & 0x0000ff00) >>> 8, argb & 0x000000ff, (argb & 0xff000000) >>> 24);
	}

	/**
	 * Sets the color of subsequent geometric shapes to the given opaque color
	 */
	default void setColor(int red, int green, int blue) {
		setColor(red, green, blue, 255);
	}

	/**
	 * Sets the color of subsequent geometric shapes to the given color
	 */
	void setColor(int red, int green, int blue, int alpha);

	// Geometric functions

	/**
	 * Draws a line from (x1, y1) to (x2, y2)
	 */
	void drawLine(int x1, int y1, int x2, int y2);

	/**
	 * Draws a line from p1 to p2
	 */
	default void drawLine(Point p1, Point p2) {
		drawLine(p1.x, p1.y, p2.x, p2.y);
	}

	/**
	 * Fills the rectangle with top-left corner (x, y) and the given width and
	 * height
	 */
	void fillRect(int x, int y, int width, int height);

	/**
	 * Draws the given box
	 */
	default void fillRect(Box rect) {
		fillRect(rect.x, rect.y, rect.w, rect.h);
	}

	/**
	 * Draws the rectangle outline with top-left corner (x, y) and the given
	 * width and height
	 */
	void drawRect(int x, int y, int width, int height);

	/**
	 * Draws the given box
	 */
	default void drawRect(Box rect) {
		drawRect(rect.x, rect.y, rect.w, rect.h);
	}

	/**
	 * Fills a circle with center (x, y) and radius r
	 */
	void fillCircle(int x, int y, int r);

	/**
	 * Fills a circle with center c and radius r
	 */
	default void fillCircle(Point c, int r) {
		fillCircle(c.x, c.y, r);
	}

	/**
	 * Draws a circle with center (x, y) and radius r
	 */
	void drawCircle(int x, int y, int r);

	/**
	 * Draws a circle with center c and radius r
	 */
	default void drawCircle(Point c, int r) {
		drawCircle(c.x, c.y, r);
	}

	/**
	 * Draws the image at the specified resource location at the point (x, y)
	 */
	default void drawImage(String location, int x, int y) {
		drawImage(location, x, y, getImageWidth(location), getImageHeight(location));
	}

	/**
	 * Draws the image at the specified resource location at the point (x, y),
	 * scaled to the dimension (w, h)
	 */
	void drawImage(String location, int x, int y, int w, int h);

	/**
	 * Gets the width of the image at the specified resource location
	 */
	int getImageWidth(String location);

	/**
	 * Gets the height of the image at the specified resource location
	 */
	int getImageHeight(String location);

	/**
	 * Sets whether the CMU-Serif font should be bold and italic
	 */
	void setFont(boolean italic, boolean bold);

	/**
	 * Gets the width the given string would be rendered
	 */
	int getStringWidth(String text);

	/**
	 * Gets the height the given string would be rendered
	 */
	int getStringHeight(String text);

	/**
	 * Gets the baseline of the given string
	 */
	int getBaseline(String text);

	/**
	 * Draws the given string with the top-left corner at (x, y)
	 */
	void drawString(String text, int x, int y);

}
