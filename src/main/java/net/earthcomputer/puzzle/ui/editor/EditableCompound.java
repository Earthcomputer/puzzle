package net.earthcomputer.puzzle.ui.editor;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.Util;

public class EditableCompound implements IEditable {

	private List<IEditable> children;

	public EditableCompound() {
		this(new TextEditable());
	}

	public EditableCompound(TextEditable text) {
		children = Util.arrayListOf(text);
	}

	@Override
	public List<IEditable> getChildren() {
		return new ArrayList<>(children);
	}

	@Override
	public int getClickExtensionFlags(int childIndex) {
		if (children.size() == 1) {
			return EXTEND_ALL;
		} else if (childIndex == 0) {
			return EXTEND_VERTICAL | EXTEND_LEFT;
		} else if (childIndex == children.size() - 1) {
			return EXTEND_VERTICAL | EXTEND_RIGHT;
		} else {
			return EXTEND_VERTICAL;
		}
	}

	public TextEditable getChildText(int index) {
		return (TextEditable) children.get(index * 2);
	}

	public IEditable getChildEditable(int index) {
		return children.get(index * 2 + 1);
	}

	public int getChildEditableCount() {
		return children.size() / 2;
	}

	public void append(EditableCompound other) {
		((TextEditable) children.get(children.size() - 1)).append((TextEditable) other.children.get(0));
		children.addAll(other.children.subList(1, other.children.size()));
	}

	public void insertEditable(int textIndex, int indexInText, IEditable editable) {
		if (editable instanceof TextEditable) {
			throw new IllegalArgumentException("Can't insert a text editable");
		}
		if (editable instanceof EditableCompound) {
			throw new IllegalArgumentException("Can't insert a compound editable");
		}
		int indexInChildren = textIndex * 2;
		Pair<TextEditable, TextEditable> pair = getChildText(textIndex).split(indexInText);
		children.set(indexInChildren, pair.getLeft());
		children.add(indexInChildren + 1, editable);
		children.add(indexInChildren + 2, pair.getRight());
	}

	public void removeEditable(int editableIndex) {
		int indexInChildren = editableIndex * 2 + 1;
		getChildText(editableIndex).append(getChildText(editableIndex + 1));
		children.remove(indexInChildren);
		children.remove(indexInChildren);
	}

	public void removeTextsAndEditables(int editableStart, int editableEnd) {
		if (editableEnd > editableStart) {
			getChildText(editableStart).append(getChildText(editableEnd));
			Util.removeRange(children, editableStart * 2 + 1, editableEnd * 2 + 1);
		}
	}

	@Override
	public String toString() {
		return toString("");
	}

	@Override
	public String toString(String prefix) {
		StringBuilder str = new StringBuilder("{");
		int i = 0;
		while (true) {
			str.append("\n").append(prefix);
			str.append(i).append(": ");
			str.append(getChildText(i).toString(prefix + "   "));
			if (i == getChildEditableCount()) {
				break;
			}
			str.append("\n").append(prefix).append("      ");
			str.append(getChildEditable(i).toString(prefix + "      "));
			i++;
		}
		str.append("\n").append(prefix).append("}");
		return str.toString();
	}

}
