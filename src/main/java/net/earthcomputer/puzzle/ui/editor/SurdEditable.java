package net.earthcomputer.puzzle.ui.editor;

import java.util.List;

import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.algebra.ast.ISurd;

public class SurdEditable implements IEditable, ISurd<EditableCompound, EditableCompound> {

	private EditableCompound n;
	private EditableCompound x;

	public SurdEditable(EditableCompound n, EditableCompound x) {
		this.n = n;
		this.x = x;
	}

	@Override
	public EditableCompound getN() {
		return n;
	}

	@Override
	public EditableCompound getX() {
		return x;
	}

	@Override
	public List<IEditable> getChildren() {
		return Util.arrayListOf(n, x);
	}

	@Override
	public int getClickExtensionFlags(int childIndex) {
		if (childIndex == 0) {
			return EXTEND_VERTICAL | EXTEND_LEFT;
		} else {
			return EXTEND_VERTICAL | EXTEND_RIGHT;
		}
	}

	@Override
	public String toString() {
		return toString("");
	}

	@Override
	public String toString(String prefix) {
		return "surd[\n" + prefix + n.toString(prefix) + "\n" + prefix + "__________\n" + prefix + x.toString(prefix)
				+ "\n" + prefix + "]";
	}

}
