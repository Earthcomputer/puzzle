package net.earthcomputer.puzzle.ui.editor;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.ui.AstUI;
import net.earthcomputer.puzzle.ui.Box;
import net.earthcomputer.puzzle.ui.HoverInfo;
import net.earthcomputer.puzzle.ui.ICanvas;
import net.earthcomputer.puzzle.ui.Point;

/**
 * A text field specialized for inputting equations. Specifically, it is the
 * means by which the user edits editable equations.
 */
public class EquationTextField {

	private static final Pattern LETTER_PATTERN = Pattern.compile("\\p{L}+");

	/**
	 * The top-level equation being edited
	 */
	private EditableCompound equation;
	/**
	 * The deepest compound editable being edited
	 */
	private EditableCompound parent;
	/**
	 * A stack of the indexes of non-compound editables in compound editables
	 */
	private Deque<Integer> parentIndexStack = new ArrayDeque<>();
	/**
	 * A stack of the compound editables
	 */
	private Deque<EditableCompound> parentStack = new ArrayDeque<>();
	/**
	 * A stack of the indexes of compound editables in non-compound editables
	 */
	private Deque<Integer> editableIndexStack = new ArrayDeque<>();
	/**
	 * A stack of the non-compound editables
	 */
	private Deque<IEditable> editableStack = new ArrayDeque<>();
	/**
	 * The index of the text editable in the deepest compound editable in which
	 * the cursor is
	 */
	private int cursorIndexInParent;
	/**
	 * The index in the text editable where the cursor is
	 */
	private int cursorIndex;
	/**
	 * The index of the text editable in the deepest compound editable in which
	 * the other end of the selection to the cursor is
	 */
	private int selectionEndInParent;
	/**
	 * The index in the text editable where the other end of the selection is
	 */
	private int selectionEnd;
	/**
	 * Whether the mouse is down (being dragged)
	 */
	private boolean dragging = false;
	private EditableCompound parentBeforeDrag;
	private Deque<Integer> parentIndexStackBeforeDrag;
	private Deque<Integer> editableIndexStackBeforeDrag;
	private int selectionEndInParentBeforeDrag;
	private int selectionEndBeforeDrag;

	public EquationTextField() {
		this(new EditableCompound());
	}

	public EquationTextField(EditableCompound equation) {
		this.equation = equation;
		this.parent = equation;
	}

	// Deleting text

	/**
	 * Deletes the currently selected text
	 */
	public void deleteSelection() {
		if (cursorIndexInParent > selectionEndInParent
				|| (cursorIndexInParent == selectionEndInParent && cursorIndex > selectionEnd)) {
			int tmp = cursorIndexInParent;
			cursorIndexInParent = selectionEndInParent;
			selectionEndInParent = tmp;

			tmp = cursorIndex;
			cursorIndex = selectionEnd;
			selectionEnd = tmp;
		}

		int len = cursorIndexInParent == selectionEndInParent ? 0
				: parent.getChildText(cursorIndexInParent).getText().length();
		parent.removeTextsAndEditables(cursorIndexInParent, selectionEndInParent);
		parent.getChildText(cursorIndexInParent).getText().delete(cursorIndex, len + selectionEnd);

		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when backspace is pressed
	 */
	public void deleteBackwards() {
		if (hasSelection()) {
			deleteSelection();
		} else {
			if (cursorIndex != 0) {
				// Delete character to left
				cursorIndex--;
				parent.getChildText(cursorIndexInParent).getText().deleteCharAt(cursorIndex);
				selectionEndInParent = cursorIndexInParent;
				selectionEnd = cursorIndex;
			} else if (cursorIndexInParent != 0) {
				// Select the editable to the left
				cursorIndexInParent--;
				cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
			} else if (parentStack.isEmpty()) {
				// We're at the very left of the equation
			} else {
				// Select the containing editable
				editableStack.pop();
				editableIndexStack.pop();
				cursorIndexInParent = parentIndexStack.pop();
				parent = parentStack.pop();
				cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
				selectionEndInParent = cursorIndexInParent + 1;
				selectionEnd = 0;
			}
		}
	}

	/**
	 * Called when delete is pressed
	 */
	public void deleteForwards() {
		if (hasSelection()) {
			deleteSelection();
		} else {
			if (cursorIndex != parent.getChildText(cursorIndexInParent).getText().length()) {
				// Delete character to right
				parent.getChildText(cursorIndexInParent).getText().deleteCharAt(cursorIndex);
			} else if (cursorIndexInParent != parent.getChildEditableCount()) {
				// Select the editable to the right
				cursorIndexInParent++;
				cursorIndex = 0;
			} else if (parentStack.isEmpty()) {
				// We're at the very right of the equation
			} else {
				// Select the containing editable
				editableStack.pop();
				editableIndexStack.pop();
				cursorIndexInParent = parentIndexStack.pop() + 1;
				parent = parentStack.pop();
				cursorIndex = 0;
				selectionEndInParent = cursorIndexInParent - 1;
				selectionEnd = parent.getChildText(selectionEndInParent).getText().length();
			}
		}
	}

	// Inserting things

	/**
	 * Called when a character is typed
	 */
	public void insertCharacter(char character) {
		if (hasSelection()) {
			deleteSelection();
		}
		parent.getChildText(cursorIndexInParent).getText().insert(cursorIndex, character);
		cursorIndex++;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when a ^ is typed
	 */
	public void insertSuperscript() {
		EditableCompound superscript;
		if (hasSelection()) {
			superscript = getSelectionAsCompound();
			deleteSelection();
		} else {
			superscript = new EditableCompound();
		}
		SuperscriptEditable editable = new SuperscriptEditable(superscript);
		parent.insertEditable(cursorIndexInParent, cursorIndex, editable);
		parentStack.push(parent);
		parentIndexStack.push(cursorIndexInParent);
		editableStack.push(editable);
		editableIndexStack.push(0);
		parent = superscript;

		selectionEndInParent = 0;
		selectionEnd = 0;
		cursorIndexInParent = superscript.getChildEditableCount();
		cursorIndex = superscript.getChildText(cursorIndexInParent).getText().length();
	}

	/**
	 * Called when a surd is to be inserted
	 */
	public void insertSurd() {
		EditableCompound x;
		if (hasSelection()) {
			x = getSelectionAsCompound();
			deleteSelection();
		} else {
			x = new EditableCompound();
		}
		SurdEditable editable = new SurdEditable(new EditableCompound(), x);
		parent.insertEditable(cursorIndexInParent, cursorIndex, editable);
		parentStack.push(parent);
		parentIndexStack.push(cursorIndexInParent);
		editableStack.push(editable);
		editableIndexStack.push(1);
		parent = x;

		selectionEndInParent = 0;
		selectionEnd = 0;
		cursorIndexInParent = x.getChildEditableCount();
		cursorIndex = x.getChildText(cursorIndexInParent).getText().length();
	}

	/**
	 * Called when a fraction is to be inserted
	 */
	public void insertFraction() {
		EditableCompound ntor;
		boolean startOnDtor = false;
		if (hasSelection()) {
			ntor = getSelectionAsCompound();
			deleteSelection();
		} else if (selectLastTerm()) {
			ntor = getSelectionAsCompound();
			deleteSelection();
			startOnDtor = true;
		} else {
			ntor = new EditableCompound();
		}
		EditableCompound dtor = new EditableCompound();
		FractionEditable editable = new FractionEditable(ntor, dtor);
		parent.insertEditable(selectionEndInParent, cursorIndex, editable);
		parentStack.push(parent);
		parentIndexStack.push(cursorIndexInParent);
		editableStack.push(editable);
		editableIndexStack.push(startOnDtor ? 1 : 0);
		parent = startOnDtor ? dtor : ntor;

		selectionEndInParent = 0;
		selectionEnd = 0;
		cursorIndexInParent = parent.getChildEditableCount();
		cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
	}

	/**
	 * Auto-selects the "term" before the cursor, returns whether anything was
	 * selected
	 */
	private boolean selectLastTerm() {
		int bracketCount = 0;
		while (true) {
			if (selectionEnd == 0) {
				if (selectionEndInParent == 0) {
					break;
				} else {
					IEditable editable = parent.getChildEditable(selectionEndInParent - 1);
					if (!(editable instanceof SuperscriptEditable) && !(editable instanceof SurdEditable)) {
						break;
					}
					selectionEndInParent--;
					selectionEnd = parent.getChildText(selectionEndInParent).getText().length();
					continue;
				}
			} else {
				selectionEnd--;
			}
			char c = parent.getChildText(selectionEndInParent).getText().charAt(selectionEnd);
			if (c == '(') {
				if (bracketCount == 0) {
					selectionEnd++;
					break;
				}
				bracketCount--;
				continue;
			}
			if (c == ')') {
				bracketCount++;
				continue;
			}
			if (bracketCount != 0) {
				continue;
			}
			if (Character.isWhitespace(c) || Character.isLetter(c) || Character.isDigit(c)) {
				continue;
			}
			selectionEnd++;
			break;
		}

		while (hasSelection()) {
			if (selectionEnd != parent.getChildText(selectionEndInParent).getText().length()) {
				if (!Character.isWhitespace(parent.getChildText(selectionEndInParent).getText().charAt(selectionEnd))) {
					return true;
				}
				selectionEnd++;
			} else {
				IEditable editable = parent.getChildEditable(selectionEndInParent);
				if (!(editable instanceof SuperscriptEditable)) {
					return true;
				}
				selectionEndInParent++;
				selectionEnd = 0;
			}
		}
		return false;
	}

	// Navigating text

	/**
	 * Called when the left arrow key is pressed
	 */
	public void moveCursorLeft() {
		if (cursorIndex != 0) {
			// Move cursor left by one character
			cursorIndex--;
		} else if (cursorIndexInParent != 0) {
			// Move into an editable on the left
			parentIndexStack.push(cursorIndexInParent - 1);
			IEditable editable = parent.getChildEditable(cursorIndexInParent - 1);
			editableStack.push(editable);
			int index = editable.getChildren().size() - 1;
			editableIndexStack.push(index);
			parentStack.push(parent);
			parent = (EditableCompound) editable.getChildren().get(index);
			cursorIndexInParent = parent.getChildEditableCount();
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
		} else if (parentStack.isEmpty()) {
			// We're on the very left of the equation
		} else if (editableIndexStack.peek() != 0) {
			// Move to the previous parent in the editable
			int index = editableIndexStack.pop();
			index--;
			editableIndexStack.push(index);
			parent = (EditableCompound) editableStack.peek().getChildren().get(index);
			cursorIndexInParent = parent.getChildEditableCount();
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
		} else {
			// Go left out of the editable
			editableStack.pop();
			editableIndexStack.pop();
			cursorIndexInParent = parentIndexStack.pop();
			parent = parentStack.pop();
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
		}
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when the right arrow key is pressed
	 */
	public void moveCursorRight() {
		if (cursorIndex != parent.getChildText(cursorIndexInParent).getText().length()) {
			// Move cursor right by one character
			cursorIndex++;
		} else if (cursorIndexInParent != parent.getChildEditableCount()) {
			// Move cursor into an editable on the right
			parentIndexStack.push(cursorIndexInParent);
			IEditable editable = parent.getChildEditable(cursorIndexInParent);
			editableStack.push(editable);
			editableIndexStack.push(0);
			parentStack.push(parent);
			parent = (EditableCompound) editable.getChildren().get(0);
			cursorIndexInParent = 0;
			cursorIndex = 0;
		} else if (parentStack.isEmpty()) {
			// We're on the very right of the equation
		} else if (editableIndexStack.peek() != editableStack.peek().getChildren().size() - 1) {
			// Move to the next parent in the editable
			int index = editableIndexStack.pop();
			index++;
			editableIndexStack.push(index);
			parent = (EditableCompound) editableStack.peek().getChildren().get(index);
			cursorIndexInParent = 0;
			cursorIndex = 0;
		} else {
			// Go right out of the editable
			editableStack.pop();
			editableIndexStack.pop();
			cursorIndexInParent = parentIndexStack.pop() + 1;
			parent = parentStack.pop();
			cursorIndex = 0;
		}
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when the up arrow key is pressed
	 */
	public void moveCursorUp() {
		if (!editableIndexStack.isEmpty()) {
			int editableIndex = editableIndexStack.peek();
			if (editableIndex == 0) {
				cursorToStart();
			} else {
				editableIndex--;
				editableIndexStack.pop();
				editableIndexStack.push(editableIndex);
				parent = (EditableCompound) editableStack.peek().getChildren().get(editableIndex);
				cursorIndexInParent = parent.getChildEditableCount();
				cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
				selectionEndInParent = cursorIndexInParent;
				selectionEnd = cursorIndex;
			}
		}
	}

	/**
	 * Called when the down arrow key is pressed
	 */
	public void moveCursorDown() {
		if (!editableIndexStack.isEmpty()) {
			int editableIndex = editableIndexStack.peek();
			IEditable editable = editableStack.peek();
			if (editableIndex == editable.getChildren().size() - 1) {
				cursorToEnd();
			} else {
				editableIndex++;
				editableIndexStack.pop();
				editableIndexStack.push(editableIndex);
				parent = (EditableCompound) editableStack.peek().getChildren().get(editableIndex);
				cursorIndexInParent = 0;
				cursorIndex = 0;
				selectionEndInParent = 0;
				selectionEnd = 0;
			}
		}
	}

	/**
	 * Called when ctrl+left is pressed
	 */
	public void moveCursorWordLeft() {
		// Skip whitespace
		StringBuilder text = parent.getChildText(cursorIndexInParent).getText();
		while (cursorIndex != 0 && Character.isWhitespace(text.charAt(cursorIndex - 1))) {
			cursorIndex--;
		}

		if (cursorIndex != 0) {
			// Try to move left by an entire word
			if (Character.isDigit(text.charAt(cursorIndex - 1))) {
				do {
					cursorIndex--;
				} while (cursorIndex != 0 && Character.isDigit(text.charAt(cursorIndex - 1)));
			} else {
				cursorIndex--;
			}
		} else if (cursorIndexInParent != 0) {
			// Skip the editable
			cursorIndexInParent--;
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
		} else if (parentStack.isEmpty()) {
			// We're at the very left of the equation
		} else {
			// Exit this editable on the left
			editableStack.pop();
			editableIndexStack.pop();
			cursorIndexInParent = parentIndexStack.pop();
			parent = parentStack.pop();
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
		}
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when ctrl+right is pressed
	 */
	public void moveCursorWordRight() {
		// Skip whitespace
		StringBuilder text = parent.getChildText(cursorIndexInParent).getText();
		while (cursorIndex != text.length() && Character.isWhitespace(text.charAt(cursorIndex))) {
			cursorIndex++;
		}

		if (cursorIndex != text.length()) {
			// Try to move right by an entire word
			if (Character.isDigit(text.charAt(cursorIndex))) {
				do {
					cursorIndex++;
				} while (cursorIndex != text.length() && Character.isDigit(text.charAt(cursorIndex)));
			} else {
				cursorIndex++;
			}
		} else if (cursorIndexInParent != parent.getChildEditableCount()) {
			// Skip the editable
			cursorIndexInParent++;
			cursorIndex = 0;
		} else if (parentStack.isEmpty()) {
			// We're at the very right of the equation
		} else {
			// Exit this editable on the right
			editableStack.pop();
			editableIndexStack.pop();
			cursorIndexInParent = parentIndexStack.pop() + 1;
			parent = parentStack.pop();
			cursorIndex = 0;
		}
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when shift+left is pressed
	 */
	public void extendSelectionLeft() {
		if (cursorIndex != 0) {
			// Move cursor left by one character
			cursorIndex--;
		} else if (cursorIndexInParent != 0) {
			// Select the entire editable on the left
			cursorIndexInParent--;
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
		} else if (parentStack.isEmpty()) {
			// We're on the very left of the equation
		} else {
			// Select the entire parent in its parent
			editableStack.pop();
			editableIndexStack.pop();
			cursorIndexInParent = parentIndexStack.pop();
			parent = parentStack.pop();
			cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
			selectionEndInParent = cursorIndexInParent + 1;
			selectionEnd = 0;
		}
	}

	/**
	 * Called when shift+right is pressed
	 */
	public void extendSelectionRight() {
		if (cursorIndex != parent.getChildText(cursorIndexInParent).getText().length()) {
			// Move cursor right by one character
			cursorIndex++;
		} else if (cursorIndexInParent != parent.getChildEditableCount()) {
			// Select the entire editable on the right
			cursorIndexInParent++;
			cursorIndex = 0;
		} else if (parentStack.isEmpty()) {
			// We're on the very right of the equation
		} else {
			// Select the entire parent in its parent
			editableStack.pop();
			editableIndexStack.pop();
			cursorIndexInParent = parentIndexStack.pop() + 1;
			parent = parentStack.pop();
			cursorIndex = 0;
			selectionEndInParent = cursorIndexInParent - 1;
			selectionEnd = parent.getChildText(selectionEndInParent).getText().length();
		}
	}

	/**
	 * Called when shift+up is pressed
	 */
	public void extendSelectionUp() {
		if (!editableStack.isEmpty()) {
			cursorToStart();
			extendSelectionLeft();
		}
	}

	/**
	 * Called when shift+down is pressed
	 */
	public void extendSelectionDown() {
		if (!editableStack.isEmpty()) {
			cursorToEnd();
			extendSelectionRight();
		}
	}

	/**
	 * Called when ctrl+shift+left is pressed
	 */
	public void extendSelectionWordLeft() {
		int prevParentStackSize = parentStack.size();
		int prevSelectionEndInParent = selectionEndInParent;
		int prevSelectionEnd = selectionEnd;
		moveCursorWordLeft();
		if (parentStack.size() < prevParentStackSize) {
			selectionEndInParent = cursorIndexInParent + 1;
			selectionEnd = 0;
		} else {
			selectionEndInParent = prevSelectionEndInParent;
			selectionEnd = prevSelectionEnd;
		}
	}

	/**
	 * Called when ctrl+shift+right is pressed
	 */
	public void extendSelectionWordRight() {
		int prevParentStackSize = parentStack.size();
		int prevSelectionEndInParent = selectionEndInParent;
		int prevSelectionEnd = selectionEnd;
		moveCursorWordRight();
		if (parentStack.size() < prevParentStackSize) {
			selectionEndInParent = cursorIndexInParent - 1;
			selectionEnd = parent.getChildText(selectionEndInParent).getText().length();
		} else {
			selectionEndInParent = prevSelectionEndInParent;
			selectionEnd = prevSelectionEnd;
		}
	}

	/**
	 * Called when ctrl+a is pressed
	 */
	public void selectAll() {
		editableStack.clear();
		editableIndexStack.clear();
		parentStack.clear();
		parentIndexStack.clear();
		parent = equation;
		selectionEndInParent = 0;
		selectionEnd = 0;
		cursorIndexInParent = parent.getChildEditableCount();
		cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
	}

	/**
	 * Called when home is pressed
	 */
	public void cursorToStart() {
		extendSelectionToStart();
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when shift+home is pressed
	 */
	public void extendSelectionToStart() {
		cursorIndexInParent = 0;
		cursorIndex = 0;
	}

	/**
	 * Called when end is pressed
	 */
	public void cursorToEnd() {
		extendSelectionToEnd();
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when shift+end is pressed
	 */
	public void extendSelectionToEnd() {
		cursorIndexInParent = parent.getChildEditableCount();
		cursorIndex = parent.getChildText(cursorIndexInParent).getText().length();
	}

	/**
	 * Called when ctrl+home is pressed
	 */
	public void cursorToVeryStart() {
		selectAll();
		cursorIndexInParent = selectionEndInParent;
		cursorIndex = selectionEnd;
	}

	/**
	 * Called when ctrl+shift+home is pressed
	 */
	public void extendSelectionToVeryStart() {
		cursorIndexInParent = 0;
		cursorIndex = 0;
		if (!parentIndexStack.isEmpty()) {
			selectionEndInParent = parentIndexStack.peekLast() + 1;
			selectionEnd = 0;
			parent = equation;
			editableStack.clear();
			editableIndexStack.clear();
			parentStack.clear();
			parentIndexStack.clear();
		}
	}

	/**
	 * Called when ctrl+end is pressed
	 */
	public void cursorToVeryEnd() {
		selectAll();
		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	/**
	 * Called when ctrl+shift+end is pressed
	 */
	public void extendSelectionToVeryEnd() {
		cursorIndexInParent = equation.getChildEditableCount();
		cursorIndex = equation.getChildText(cursorIndexInParent).getText().length();
		if (!parentIndexStack.isEmpty()) {
			selectionEndInParent = parentIndexStack.peekLast();
			selectionEnd = equation.getChildText(selectionEndInParent).getText().length();
			parent = equation;
			editableStack.clear();
			editableIndexStack.clear();
			parentStack.clear();
			parentIndexStack.clear();
		}
	}

	// Mouse/touch controls

	private HoverInfo getHoverInfo(ICanvas canvas, Point location) {
		return getHoverInfo(canvas, 0, equation, location, IEditable.EXTEND_ALL);
	}

	private static HoverInfo getHoverInfo(ICanvas canvas, int index, Object node, Point relativeLocation,
			int extensionFlags) {
		AstUI<Object> ui = AstUI.getUI(node);
		int width = ui.getWidth(canvas, node);
		int height = ui.getHeight(canvas, node);

		if (((extensionFlags & IEditable.EXTEND_LEFT) == 0 && relativeLocation.x < 0)
				|| ((extensionFlags & IEditable.EXTEND_RIGHT) == 0 && relativeLocation.x >= width)
				|| ((extensionFlags & IEditable.EXTEND_UP) == 0 && relativeLocation.y < 0)
				|| ((extensionFlags & IEditable.EXTEND_DOWN) == 0 && relativeLocation.y >= height)) {
			return HoverInfo.EMPTY;
		}

		List<Object> children = ui.getChildren(canvas, node);
		List<Point> childPositions = ui.getChildPositions(canvas, node);
		List<Float> childScales = ui.getChildScales(canvas, node);
		HoverInfo.Builder builder = new HoverInfo.Builder();

		for (int i = 0; i < children.size(); i++) {
			Object child = children.get(i);
			Point position = childPositions.get(i);
			float scale = childScales.get(i);

			Point newRelLoc = relativeLocation.subtract(position).divide(scale);
			int newExtensionFlags = node instanceof IEditable ? ((IEditable) node).getClickExtensionFlags(i)
					: extensionFlags;
			builder.add(getHoverInfo(canvas, i, child, newRelLoc, newExtensionFlags), position, scale);
		}

		builder.addHovered(index, node, relativeLocation, new Box(0, 0, width, height));

		return builder.build();
	}

	/**
	 * Called when the mouse is clicked on the text field, or the text field is
	 * tapped on a touchscreen.
	 */
	public void equationClicked(ICanvas canvas, Point location) {
		HoverInfo hoverInfo = getHoverInfo(canvas, location);
		boolean hasText = false;
		for (int i = 0; i < hoverInfo.getDepth(); i++) {
			if (hoverInfo.getNode(i) instanceof TextEditable) {
				hasText = true;
				break;
			}
		}
		if (!hasText) {
			return;
		}

		editableStack.clear();
		editableIndexStack.clear();
		parentStack.clear();
		parentIndexStack.clear();

		int i = hoverInfo.getDepth();
		int parentIndexInStack = -1;
		int textIndexInStack = -1;

		// Warning to reader: spaghetti code!!! sorry :(
		outside: while (true) {
			do {
				if (--i < 0) {
					break outside;
				}
			} while (!(hoverInfo.getNode(i) instanceof EditableCompound));
			parentIndexInStack = i;
			EditableCompound parent = (EditableCompound) hoverInfo.getNode(i);
			int parentIndex = hoverInfo.getIndex(i - 1) / 2;
			do {
				if (--i < 0) {
					break outside;
				}
			} while (!(hoverInfo.getNode(i) instanceof IEditable));
			textIndexInStack = i;
			IEditable editable = (IEditable) hoverInfo.getNode(i);
			if (editable instanceof TextEditable) {
				break;
			}
			int editableIndex = hoverInfo.getIndex(i - 1);

			editableStack.push(editable);
			editableIndexStack.push(editableIndex);
			parentStack.push(parent);
			parentIndexStack.push(parentIndex);
		}

		parent = (EditableCompound) hoverInfo.getNode(parentIndexInStack);
		cursorIndexInParent = hoverInfo.getIndex(textIndexInStack) / 2;
		StringBuilder text = parent.getChildText(cursorIndexInParent).getText();
		Point pointInText = hoverInfo.getRelativeLocation(textIndexInStack);
		int prevWidth = 0, width;
		for (cursorIndex = 0; cursorIndex <= text.length(); cursorIndex++) {
			width = getWidthWithStyle(text.substring(0, cursorIndex), canvas);
			if (width > pointInText.x) {
				int avgWidth = (width + prevWidth) / 2;
				if (pointInText.x < avgWidth && cursorIndex > 0) {
					cursorIndex--;
				}
				break;
			}
			prevWidth = width;
		}
		if (cursorIndex > text.length()) {
			cursorIndex = text.length();
		}

		selectionEndInParent = cursorIndexInParent;
		selectionEnd = cursorIndex;
	}

	public void equationDragged(ICanvas canvas, Point location) {
		if (!dragging) {
			dragging = true;
			parentBeforeDrag = parent;
			parentIndexStackBeforeDrag = new ArrayDeque<>(parentIndexStack);
			editableIndexStackBeforeDrag = new ArrayDeque<>(editableIndexStack);
			selectionEndInParentBeforeDrag = selectionEndInParent;
			selectionEndBeforeDrag = selectionEnd;
		}

		equationClicked(canvas, location);

		Iterator<Integer> parentIndexItr = parentIndexStack.descendingIterator();
		Iterator<Integer> parentIndexBeforeDragItr = parentIndexStackBeforeDrag.descendingIterator();
		Iterator<Integer> editableIndexItr = editableIndexStack.descendingIterator();
		Iterator<Integer> editableIndexBeforeDragItr = editableIndexStackBeforeDrag.descendingIterator();
		int reverseIndexDifferent = 0;
		boolean allSame = true;
		while (parentIndexItr.hasNext() && parentIndexBeforeDragItr.hasNext()) {
			int cursor = parentIndexItr.next();
			int selection = parentIndexBeforeDragItr.next();
			if (cursor != selection) {
				parent = Util.linearGet(parentStack, parentStack.size() - 1 - reverseIndexDifferent);
				if (cursor < selection) {
					cursorIndexInParent = cursor;
					cursorIndex = parent.getChildText(cursor).getText().length();
					selectionEndInParent = selection + 1;
					selectionEnd = 0;
				} else {
					cursorIndexInParent = cursor + 1;
					cursorIndex = 0;
					selectionEndInParent = selection;
					selectionEnd = parent.getChildText(selection).getText().length();
				}
				allSame = false;
				break;
			}
			int indexInParent = cursor;
			cursor = editableIndexItr.next();
			selection = editableIndexBeforeDragItr.next();
			if (cursor != selection) {
				parent = Util.linearGet(parentStack, parentStack.size() - 1 - reverseIndexDifferent);
				if (cursor < selection) {
					cursorIndexInParent = indexInParent;
					cursorIndex = parent.getChildText(indexInParent).getText().length();
					selectionEndInParent = indexInParent + 1;
					selectionEnd = 0;
				} else {
					cursorIndexInParent = indexInParent + 1;
					cursorIndex = 0;
					selectionEndInParent = indexInParent;
					selectionEnd = parent.getChildText(indexInParent).getText().length();
				}
				allSame = false;
				break;
			}
			reverseIndexDifferent++;
		}
		if (allSame) {
			if (parentIndexItr.hasNext()) {
				parent = parentBeforeDrag;
				int cursor = parentIndexItr.next();
				if (cursor >= selectionEndInParentBeforeDrag) {
					cursorIndexInParent = cursor + 1;
					cursorIndex = 0;
				} else {
					cursorIndexInParent = cursor;
					cursorIndex = parent.getChildText(cursor).getText().length();
				}
				selectionEndInParent = selectionEndInParentBeforeDrag;
				selectionEnd = selectionEndBeforeDrag;
			} else if (parentIndexBeforeDragItr.hasNext()) {
				// Parent doesn't need to change
				int selection = parentIndexBeforeDragItr.next();
				if (selection >= cursorIndexInParent) {
					selectionEndInParent = selection + 1;
					selectionEnd = 0;
				} else {
					selectionEndInParent = selection;
					selectionEnd = parent.getChildText(selection).getText().length();
				}
			} else {
				// Parent doesn't need to change
				selectionEndInParent = selectionEndInParentBeforeDrag;
				selectionEnd = selectionEndBeforeDrag;
			}
		}
		Util.removeRange(parentIndexStack, 0, parentIndexStack.size() - reverseIndexDifferent);
		Util.removeRange(parentStack, 0, parentStack.size() - reverseIndexDifferent);
		Util.removeRange(editableIndexStack, 0, editableIndexStack.size() - reverseIndexDifferent);
		Util.removeRange(editableStack, 0, editableStack.size() - reverseIndexDifferent);
	}

	public void equationReleaseDragged() {
		dragging = false;
		parentBeforeDrag = null;
		parentIndexStackBeforeDrag = null;
		editableIndexStackBeforeDrag = null;
	}

	// Rendering

	/**
	 * Draws the text field at (0, 0) on the canvas
	 */
	public void draw(ICanvas canvas, boolean showCursor) {
		// Draw the equation
		AstUI<EditableCompound> equationUI = AstUI.getUI(equation);
		equationUI.draw(canvas, equation);

		// Cursor/selection
		if (showCursor || cursorIndexInParent != selectionEndInParent || cursorIndex != selectionEnd) {
			// Work out positions and scales
			int x = 0, y = 0;
			float scale = 1;
			Iterator<EditableCompound> parentItr = parentStack.descendingIterator();
			Iterator<Integer> parentIndexItr = parentIndexStack.descendingIterator();
			Iterator<IEditable> editableItr = editableStack.descendingIterator();
			Iterator<Integer> editableIndexItr = editableIndexStack.descendingIterator();
			while (parentItr.hasNext()) {
				EditableCompound parent = parentItr.next();
				AstUI<EditableCompound> parentUI = AstUI.getUI(parent);
				int childIndex = parentIndexItr.next() * 2 + 1;
				Point childPos = parentUI.getChildPositions(canvas, parent).get(childIndex);
				float childScale = parentUI.getChildScales(canvas, parent).get(childIndex);
				x += childPos.x * scale;
				y += childPos.y * scale;
				scale *= childScale;

				IEditable editable = editableItr.next();
				AstUI<IEditable> editableUI = AstUI.getUI(editable);
				childIndex = editableIndexItr.next();
				childPos = editableUI.getChildPositions(canvas, editable).get(childIndex);
				childScale = editableUI.getChildScales(canvas, editable).get(childIndex);
				x += childPos.x * scale;
				y += childPos.y * scale;
				scale *= childScale;
			}

			// Show selection
			if (hasSelection()) {
				int startIndexInParent, startIndex;
				int endIndexInParent, endIndex;
				if (cursorIndexInParent < selectionEndInParent
						|| (cursorIndexInParent == selectionEndInParent && cursorIndex < selectionEnd)) {
					startIndexInParent = cursorIndexInParent;
					startIndex = cursorIndex;
					endIndexInParent = selectionEndInParent;
					endIndex = selectionEnd;
				} else {
					startIndexInParent = selectionEndInParent;
					startIndex = selectionEnd;
					endIndexInParent = cursorIndexInParent;
					endIndex = cursorIndex;
				}
				AstUI<EditableCompound> parentUI = AstUI.getUI(parent);
				int selectionStartX = x
						+ (int) (parentUI.getChildPositions(canvas, parent).get(startIndexInParent * 2).x * scale);
				selectionStartX += getWidthWithStyle(
						parent.getChildText(startIndexInParent).getText().substring(0, startIndex), canvas) * scale;
				int selectionEndX = x
						+ (int) (parentUI.getChildPositions(canvas, parent).get(endIndexInParent * 2).x * scale);
				selectionEndX += getWidthWithStyle(
						parent.getChildText(endIndexInParent).getText().substring(0, endIndex), canvas) * scale;

				canvas.setColor(0x80, 0x80, 0xff, 0x40);
				canvas.fillRect(selectionStartX, y, selectionEndX - selectionStartX,
						(int) (parentUI.getHeight(canvas, parent) * scale));
			}
			// Show cursor
			if (showCursor) {
				AstUI<EditableCompound> parentUI = AstUI.getUI(parent);
				int cursorX = x
						+ (int) (parentUI.getChildPositions(canvas, parent).get(cursorIndexInParent * 2).x * scale);
				cursorX += getWidthWithStyle(
						parent.getChildText(cursorIndexInParent).getText().substring(0, cursorIndex), canvas) * scale;

				canvas.setColor(ICanvas.BLACK);
				canvas.drawLine(cursorX, y, cursorX, y + (int) (parentUI.getHeight(canvas, parent) * scale));
			}
		}
	}

	/**
	 * Gets the width of text which will be styled
	 */
	private static int getWidthWithStyle(String text, ICanvas canvas) {
		int width = 0;
		Matcher matcher = LETTER_PATTERN.matcher(text);
		int start = 0;
		int end = 0;
		while (matcher.find()) {
			start = matcher.start();
			if (start != 0) {
				canvas.setFont(false, false);
				width += canvas.getStringWidth(text.substring(end, start));
			}
			end = matcher.end();
			canvas.setFont(true, false);
			width += canvas.getStringWidth(text.substring(start, end));
		}
		if (end != text.length()) {
			canvas.setFont(false, false);
			width += canvas.getStringWidth(text.substring(end));
		}
		return width;
	}

	// Other utilities

	/**
	 * Gets the currently selected stuff as an editable compound
	 */
	private EditableCompound getSelectionAsCompound() {
		int startInParent, start;
		int endInParent, end;
		if (cursorIndexInParent < selectionEndInParent
				|| (cursorIndexInParent == selectionEndInParent && cursorIndex < selectionEnd)) {
			startInParent = cursorIndexInParent;
			start = cursorIndex;
			endInParent = selectionEndInParent;
			end = selectionEnd;
		} else {
			startInParent = selectionEndInParent;
			start = selectionEnd;
			endInParent = cursorIndexInParent;
			end = cursorIndex;
		}

		EditableCompound compound = new EditableCompound();
		if (startInParent == endInParent) {
			compound.getChildText(0).getText()
					.append(parent.getChildText(startInParent).getText().substring(start, end));
		} else {
			compound.getChildText(0).getText().append(parent.getChildText(startInParent).getText().substring(start));
			compound.insertEditable(0, compound.getChildText(0).getText().length(),
					parent.getChildEditable(startInParent));
			for (int i = startInParent + 1; i < endInParent; i++) {
				StringBuilder text = parent.getChildText(i).getText();
				compound.getChildText(i - startInParent).getText().append(text);
				compound.insertEditable(i - startInParent, text.length(), parent.getChildEditable(i));
			}
			compound.getChildText(compound.getChildEditableCount()).getText()
					.append(parent.getChildText(endInParent).getText().substring(0, end));
		}

		return compound;
	}

	/**
	 * Returns whether any part of the equation is selected
	 */
	private boolean hasSelection() {
		return selectionEnd != cursorIndex || selectionEndInParent != cursorIndexInParent;
	}

	@Override
	public String toString() {
		// @formatter:off
		return "EquationTextField{\n"
				+ "equation: " + equation.toString("          ") + "\n"
				+ "parent: " + parent.toString("        ") + "\n"
				+ "parentIndexStack: " + parentIndexStack + "\n"
				+ "editableIndexStack: " + editableIndexStack + "\n"
				+ "cursorIndexInParent: " + cursorIndexInParent + "\n"
				+ "cursorIndex: " + cursorIndex + "\n"
				+ "selectionEndInParent: " + selectionEndInParent + "\n"
				+ "selectionEnd: " + selectionEnd + "\n"
				+ "dragging: " + dragging + "\n"
				+ "parentIndexStackBeforeDrag: " + parentIndexStackBeforeDrag + "\n"
				+ "editableIndexStackBeforeDrag: " + editableIndexStackBeforeDrag + "\n"
				+ "parentBeforeDrag: " + parentBeforeDrag.toString("                  ") + "\n"
				+ "}";
		// @formatter:on
	}

}
