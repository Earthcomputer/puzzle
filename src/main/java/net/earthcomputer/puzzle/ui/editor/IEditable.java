package net.earthcomputer.puzzle.ui.editor;

import java.util.List;

/**
 * A part of the equation in the equation editor
 */
public interface IEditable {

	public static final int EXTEND_NONE = 0x0;
	public static final int EXTEND_LEFT = 0x1;
	public static final int EXTEND_RIGHT = 0x2;
	public static final int EXTEND_UP = 0x4;
	public static final int EXTEND_DOWN = 0x8;
	public static final int EXTEND_HORIZONTAL = EXTEND_LEFT | EXTEND_RIGHT;
	public static final int EXTEND_VERTICAL = EXTEND_UP | EXTEND_DOWN;
	public static final int EXTEND_ALL = EXTEND_HORIZONTAL | EXTEND_VERTICAL;

	List<IEditable> getChildren();

	int getClickExtensionFlags(int childIndex);

	String toString(String prefix);

}
