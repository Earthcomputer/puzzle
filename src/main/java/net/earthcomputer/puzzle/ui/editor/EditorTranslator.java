package net.earthcomputer.puzzle.ui.editor;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.earthcomputer.puzzle.algebra.SpecialChars;
import net.earthcomputer.puzzle.algebra.ast.ComparisonOperator;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.ast.ExponentialTerm;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FractionTerm;
import net.earthcomputer.puzzle.algebra.ast.FunctionTerm;
import net.earthcomputer.puzzle.algebra.ast.IAstNode;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;
import net.earthcomputer.puzzle.algebra.ast.SurdTerm;
import net.earthcomputer.puzzle.algebra.ast.VariableTerm;

public class EditorTranslator {

	private static final Pattern ARGUMENT_PATTERN = Pattern.compile("\\$(\\d+)");

	// AST TO EDITABLE

	public static EditableCompound toEditable(IAstNode node) {
		IEditable editable = toEditableNode(node);
		if (editable instanceof EditableCompound) {
			return (EditableCompound) editable;
		}

		EditableCompound compound = new EditableCompound();
		compound.insertEditable(0, 0, editable);
		return compound;
	}

	private static IEditable toEditableNode(IAstNode node) {
		String format = node.getFormat();
		if (format == null) {
			if (node instanceof FractionTerm) {
				FractionTerm fraction = (FractionTerm) node;
				return new FractionEditable(toEditable(fraction.getNumerator()), toEditable(fraction.getDenominator()));
			}
			if (node instanceof ExponentialTerm) {
				ExponentialTerm exponent = (ExponentialTerm) node;
				EditableCompound compound = toEditable(exponent.getBase());
				compound.insertEditable(compound.getChildEditableCount(),
						compound.getChildText(compound.getChildEditableCount()).getText().length(),
						new SuperscriptEditable(toEditable(exponent.getExponent())));
				return compound;
			}
			if (node instanceof SurdTerm) {
				SurdTerm surd = (SurdTerm) node;
				return new SurdEditable(surd.getN() == null ? new EditableCompound() : toEditable(surd.getN()),
						toEditable(surd.getX()));
			}
			if (node instanceof FunctionTerm) {
				FunctionTerm function = (FunctionTerm) node;
				String superscriptText;
				if (function.isInverse()) {
					superscriptText = "-1";
				} else if (function.getPowers() != 1) {
					superscriptText = String.valueOf(function.getPowers());
				} else {
					superscriptText = "";
				}
				EditableCompound superscriptCompound = new EditableCompound();
				superscriptCompound.getChildText(0).getText().append(superscriptText);
				EditableCompound compound = new EditableCompound();
				compound.insertEditable(0, 0, new SuperscriptEditable(superscriptCompound));
				compound.getChildText(0).getText().append(function.getName());
				compound.getChildText(1).getText().append("(");
				boolean first = true;
				for (int i = 0; i < function.getArgs().size(); i++) {
					if (!first) {
						compound.getChildText(compound.getChildEditableCount()).getText().append(", ");
					}
					first = false;
					compound.append(toEditable(function.getArgs().get(i)));
				}
				compound.getChildText(compound.getChildEditableCount()).getText().append(")");
				return compound;
			}
			throw new IllegalStateException("format returned null, but unsupported built-in");
		}

		EditableCompound compound = new EditableCompound();
		Matcher matcher = ARGUMENT_PATTERN.matcher(format);
		int start = 0, end = 0;
		while (matcher.find()) {
			start = matcher.start();
			compound.getChildText(compound.getChildEditableCount()).getText().append(format.substring(end, start));
			end = matcher.end();
			IAstNode child = node.getChild(Integer.parseInt(matcher.group(1)) - 1);
			compound.append(toEditable(child));
		}
		compound.getChildText(compound.getChildEditableCount()).getText().append(format.substring(end));

		return compound;
	}

	// EDITABLE TO AST

	public static Equation toEquation(EditableCompound editable, Collection<String> functionDictionary)
			throws SyntaxErrorException {
		EditableParser parser = new EditableParser(editable, functionDictionary);
		Equation equation = parser.parseEquation();
		parser.consumeEnd();
		return equation;
	}

	public static Expression toExpression(EditableCompound editable, Collection<String> functionDictionary)
			throws SyntaxErrorException {
		EditableParser parser = new EditableParser(editable, functionDictionary);
		Expression expression = parser.parseExpression();
		parser.consumeEnd();
		return expression;
	}

	private static class EditableParser {
		private EditableCompound compound;
		private Collection<String> functionDictionary;
		private List<TokenType> nextTokenTypes = new ArrayList<>();
		private List<Object> nextTokens = new ArrayList<>();
		private int nextCompoundIndex = 0;
		private int nextTextIndex = 0;
		private int tailCompoundIndex = 0;
		private int tailTextIndex = 0;
		private Deque<Integer> savedTailCompoundIndexes = new ArrayDeque<>();
		private Deque<Integer> savedTailTextIndexes = new ArrayDeque<>();

		public EditableParser(EditableCompound compound, Collection<String> functionDictionary) {
			this.compound = compound;
			this.functionDictionary = functionDictionary;
		}

		public Equation parseEquation() throws SyntaxErrorException {
			Expression left = parseExpression();

			ComparisonOperator comparisonOp;
			switch (consumeChar()) {
			case '=':
				comparisonOp = ComparisonOperator.EQUALS;
				break;
			case '<':
				comparisonOp = ComparisonOperator.LESS_THAN;
				break;
			case '>':
				comparisonOp = ComparisonOperator.GREATER_THAN;
				break;
			case SpecialChars.LESS_THAN_EQUAL:
				comparisonOp = ComparisonOperator.LESS_THAN_EQUAL;
				break;
			case SpecialChars.GREATER_THAN_EQUAL:
				comparisonOp = ComparisonOperator.GREATER_THAN_EQUAL;
				break;
			default:
				throw new SyntaxErrorException();
			}

			Expression right = parseExpression();

			return new Equation(left, right, comparisonOp);
		}

		public Expression parseExpression() throws SyntaxErrorException {
			List<ProductTerm> terms = new ArrayList<>();
			List<Sign> signs = new ArrayList<>();

			Sign sign = Sign.PLUS;
			if (nextTokenType() == TokenType.CHARACTER) {
				switch (nextChar()) {
				case '+':
				case '-':
				case SpecialChars.PLUS_MINUS:
				case SpecialChars.MINUS_PLUS:
					sign = parseSign();
					break;
				}
			}
			signs.add(sign);
			terms.add(parseProductTerm());

			loop: while (nextTokenType() == TokenType.CHARACTER) {
				switch (nextChar()) {
				case '+':
				case '-':
				case SpecialChars.PLUS_MINUS:
				case SpecialChars.MINUS_PLUS:
					sign = parseSign();
					break;
				default:
					break loop;
				}
				signs.add(sign);
				terms.add(parseProductTerm());
			}

			return new Expression(terms, signs);
		}

		private Sign parseSign() throws SyntaxErrorException {
			boolean hadSign = false;
			Sign sign = Sign.PLUS;
			loop: while (nextTokenType() == TokenType.CHARACTER) {
				switch (nextChar()) {
				case '+':
					consume();
					break;
				case '-':
					switch (sign) {
					case PLUS:
						sign = Sign.MINUS;
						break;
					case MINUS:
						sign = Sign.PLUS;
						break;
					case PLUS_MINUS:
						sign = Sign.MINUS_PLUS;
						break;
					case MINUS_PLUS:
						sign = Sign.PLUS_MINUS;
						break;
					}
					consume();
					break;
				case SpecialChars.PLUS_MINUS:
					switch (sign) {
					case PLUS:
						sign = Sign.PLUS_MINUS;
						break;
					case MINUS:
						sign = Sign.MINUS_PLUS;
						break;
					case PLUS_MINUS:
						sign = Sign.PLUS;
						break;
					case MINUS_PLUS:
						sign = Sign.MINUS;
						break;
					}
					consume();
					break;
				case SpecialChars.MINUS_PLUS:
					switch (sign) {
					case PLUS:
						sign = Sign.MINUS_PLUS;
						break;
					case MINUS:
						sign = Sign.PLUS_MINUS;
						break;
					case PLUS_MINUS:
						sign = Sign.MINUS;
						break;
					case MINUS_PLUS:
						sign = Sign.PLUS;
						break;
					}
					consume();
					break;
				default:
					break loop;
				}
				hadSign = true;
			}

			if (!hadSign) {
				throw new SyntaxErrorException();
			}
			return sign;
		}

		private ProductTerm parseProductTerm() throws SyntaxErrorException {
			List<SimpleTerm> terms = new ArrayList<>();
			terms.add(parseSimpleTerm());

			while (true) {
				boolean multSign = nextTokenType() == TokenType.CHARACTER && nextChar() == '*';
				Sign sign = Sign.PLUS;
				if (multSign) {
					consume();

					if (nextTokenType() == TokenType.CHARACTER) {
						switch (nextChar()) {
						case '+':
						case '-':
						case SpecialChars.PLUS_MINUS:
						case SpecialChars.MINUS_PLUS:
							sign = parseSign();
							break;
						}
					}
				}

				SimpleTerm simpleTerm;
				saveState();
				try {
					simpleTerm = parseSimpleTerm();
				} catch (SyntaxErrorException e) {
					if (multSign) {
						discardSavedState();
						throw e;
					} else {
						restoreSavedState();
						return new ProductTerm(terms);
					}
				}
				discardSavedState();

				if (sign != Sign.PLUS) {
					simpleTerm = new ExpressionTerm(new Expression(sign, simpleTerm));
				}

				terms.add(simpleTerm);
			}
		}

		private SimpleTerm parseSimpleTerm() throws SyntaxErrorException {
			SimpleTerm term = parseSimpleTermNoExponent();
			if (nextTokenType() == TokenType.SUPERSCRIPT) {
				term = new ExponentialTerm(term,
						toExpression(((SuperscriptEditable) consume()).getChild(), functionDictionary));
			}
			return term;
		}

		private SimpleTerm parseSimpleTermNoExponent() throws SyntaxErrorException {
			if (nextTokenType() == TokenType.FRACTION) {
				FractionEditable fraction = (FractionEditable) consume();
				return new FractionTerm(toExpression(fraction.getNumerator(), functionDictionary),
						toExpression(fraction.getDenominator(), functionDictionary));
			}
			if (nextTokenType() == TokenType.SURD) {
				SurdEditable surd = (SurdEditable) consume();
				EditableParser parser = new EditableParser(surd.getN(), functionDictionary);
				Expression n = parser.nextTokenType() == TokenType.END ? null : parser.parseExpression();
				parser.consumeEnd();
				Expression x = toExpression(surd.getX(), functionDictionary);
				return new SurdTerm(n, x);
			}
			char c = nextChar();
			if (c == '(') {
				return parseExpressionTerm();
			}
			if (Character.isDigit(c)) {
				return parseNumberTerm();
			}
			if (Character.isLetter(c)) {
				String functionName = nextFunctionName();
				if (functionName.isEmpty()) {
					return parseVariableTerm();
				} else {
					return parseFunctionTerm(functionName);
				}
			}
			throw new SyntaxErrorException();
		}

		private ExpressionTerm parseExpressionTerm() throws SyntaxErrorException {
			consumeChar('(');
			Expression expr = parseExpression();
			consumeChar(')');
			return new ExpressionTerm(expr);
		}

		private NumberTerm parseNumberTerm() throws SyntaxErrorException {
			StringBuilder text = new StringBuilder();

			text.append(parseInteger());

			if (nextTokenType() == TokenType.CHARACTER && nextChar() == '.') {
				consume();
				text.append('.');
				text.append(parseInteger());
			}

			return new NumberTerm(new BigDecimal(text.toString()));
		}

		private String parseInteger() throws SyntaxErrorException {
			StringBuilder text = new StringBuilder();
			if (!Character.isDigit(nextChar())) {
				throw new SyntaxErrorException();
			}
			while (true) {
				if (nextTokenType() != TokenType.CHARACTER) {
					break;
				}
				char c = nextChar();
				if (!Character.isDigit(c)) {
					break;
				}
				consume();
				text.append(c);
			}
			return text.toString();
		}

		private VariableTerm parseVariableTerm() throws SyntaxErrorException {
			return new VariableTerm(String.valueOf(consumeChar()));
		}

		private FunctionTerm parseFunctionTerm(String functionName) throws SyntaxErrorException {
			for (int i = 0; i < functionName.length(); i++) {
				consume();
			}
			int nderivatives = 0;
			while (nextTokenType() == TokenType.CHARACTER && nextChar() == '\'') {
				consume();
				nderivatives++;
			}

			boolean inverse = false;
			int powers = 1;
			if (nderivatives == 0 && nextTokenType() == TokenType.SUPERSCRIPT) {
				EditableParser parser = new EditableParser(((SuperscriptEditable) consume()).getChild(),
						Collections.emptySet());
				if (parser.nextTokenType() == TokenType.CHARACTER && parser.nextChar() == '-') {
					inverse = true;
					parser.consume();
				}
				String number = parser.parseInteger();
				int integer;
				try {
					integer = Integer.parseInt(number);
				} catch (NumberFormatException e) {
					throw new SyntaxErrorException();
				}
				if (inverse && integer != 1) {
					throw new SyntaxErrorException();
				}
				if (!inverse) {
					powers = integer;
				}

				parser.consumeEnd();
			}

			List<Expression> functionArgs = new ArrayList<>();
			if (nextTokenType() == TokenType.CHARACTER && nextChar() == '(') {
				consume();
				functionArgs.add(parseExpression());
				while (nextChar() != ')') {
					consumeChar(',');
					functionArgs.add(parseExpression());
				}
				consume();
			} else {
				functionArgs.add(new Expression(Sign.PLUS, new ProductTerm(parseSimpleTerm())));
			}

			return new FunctionTerm(functionName, inverse, nderivatives, powers, new ArrayList<>(), functionArgs);
		}

		private String nextFunctionName() {
			String longestFunctionName = "";
			String text = "";
			int count = 1;
			while (true) {
				if (lookaheadTokenType(count) != TokenType.CHARACTER) {
					break;
				}
				char c = (Character) lookaheadToken(count);
				if (!Character.isLetter(c) && !Character.isDigit(c)) {
					break;
				}
				text += c;
				count++;
				if (functionDictionary.contains(text)) {
					longestFunctionName = text;
				}
			}
			return longestFunctionName;
		}

		private char consumeChar() throws SyntaxErrorException {
			return (Character) consume(TokenType.CHARACTER);
		}

		private void consumeChar(char expected) throws SyntaxErrorException {
			if (consumeChar() != expected) {
				throw new SyntaxErrorException();
			}
		}

		public void consumeEnd() throws SyntaxErrorException {
			consume(TokenType.END);
		}

		private Object consume(TokenType tokenType) throws SyntaxErrorException {
			if (nextTokenType() != tokenType) {
				throw new SyntaxErrorException();
			}
			return consume();
		}

		private TokenType nextTokenType() {
			shiftIfEmpty();
			return nextTokenTypes.get(0);
		}

		private char nextChar() throws SyntaxErrorException {
			return (Character) nextToken(TokenType.CHARACTER);
		}

		private Object nextToken(TokenType tokenType) throws SyntaxErrorException {
			if (nextTokenType() != tokenType) {
				throw new SyntaxErrorException();
			}
			return nextToken();
		}

		private Object nextToken() {
			shiftIfEmpty();
			return nextTokens.get(0);
		}

		private TokenType lookaheadTokenType(int count) {
			while (nextTokenTypes.size() < count) {
				shift();
			}
			return nextTokenTypes.get(count - 1);
		}

		private Object lookaheadToken(int count) {
			while (nextTokens.size() < count) {
				shift();
			}
			return nextTokens.get(count - 1);
		}

		private Object consume() {
			shiftIfEmpty();

			if (tailCompoundIndex != compound.getChildren().size()) {
				if (tailCompoundIndex % 2 == 0) {
					StringBuilder text = compound.getChildText(tailCompoundIndex / 2).getText();
					while (tailTextIndex < text.length() && Character.isWhitespace(text.charAt(tailTextIndex))) {
						tailTextIndex++;
					}
					if (tailTextIndex >= text.length()) {
						tailCompoundIndex++;
						tailTextIndex = 0;
					}
					if (tailCompoundIndex == compound.getChildren().size()) {
						nextTokenTypes.remove(0);
						return nextTokens.remove(0);
					}
				}

				if (tailCompoundIndex % 2 == 1) {
					tailCompoundIndex++;
					tailTextIndex = 0;
				} else {
					tailTextIndex++;
				}
				StringBuilder text = compound.getChildText(tailCompoundIndex / 2).getText();
				while (tailTextIndex < text.length() && Character.isWhitespace(text.charAt(tailTextIndex))) {
					tailTextIndex++;
				}
				if (tailTextIndex >= text.length()) {
					tailCompoundIndex++;
					tailTextIndex = 0;
				}
			}

			nextTokenTypes.remove(0);
			return nextTokens.remove(0);
		}

		private void shiftIfEmpty() {
			if (nextTokenTypes.isEmpty()) {
				shift();
			}
		}

		private void shift() {
			// Check for end
			if (nextCompoundIndex == compound.getChildren().size()) {
				nextTokenTypes.add(TokenType.END);
				nextTokens.add(null);
				return;
			}

			// Skip whitespace
			if (nextCompoundIndex % 2 == 0) {
				StringBuilder text = compound.getChildText(nextCompoundIndex / 2).getText();
				while (nextTextIndex < text.length() && Character.isWhitespace(text.charAt(nextTextIndex))) {
					nextTextIndex++;
				}
				if (nextTextIndex >= text.length()) {
					nextCompoundIndex++;
					nextTextIndex = 0;
				}
				if (nextCompoundIndex == compound.getChildren().size()) {
					nextTokenTypes.add(TokenType.END);
					nextTokens.add(null);
					return;
				}
			}

			if (nextCompoundIndex % 2 == 0) {
				StringBuilder text = compound.getChildText(nextCompoundIndex / 2).getText();
				char c = text.charAt(nextTextIndex);
				nextTextIndex++;
				if (nextTextIndex >= text.length()) {
					nextCompoundIndex++;
					nextTextIndex = 0;
				}
				nextTokenTypes.add(TokenType.CHARACTER);
				nextTokens.add(c);
			} else {
				IEditable editable = compound.getChildren().get(nextCompoundIndex);
				nextCompoundIndex++;
				TokenType tokenType;
				if (editable instanceof SuperscriptEditable) {
					tokenType = TokenType.SUPERSCRIPT;
				} else if (editable instanceof FractionEditable) {
					tokenType = TokenType.FRACTION;
				} else if (editable instanceof SurdEditable) {
					tokenType = TokenType.SURD;
				} else {
					throw new AssertionError("Invalid editable class " + editable.getClass().getSimpleName());
				}
				nextTokenTypes.add(tokenType);
				nextTokens.add(editable);
			}
		}

		private void saveState() {
			savedTailCompoundIndexes.push(tailCompoundIndex);
			savedTailTextIndexes.push(tailTextIndex);
		}

		private void restoreSavedState() {
			nextCompoundIndex = tailCompoundIndex = savedTailCompoundIndexes.pop();
			nextTextIndex = tailTextIndex = savedTailTextIndexes.pop();
			nextTokenTypes.clear();
			nextTokens.clear();
		}

		private void discardSavedState() {
			savedTailCompoundIndexes.pop();
			savedTailTextIndexes.pop();
		}

		private static enum TokenType {
			CHARACTER, SUPERSCRIPT, FRACTION, SURD, END
		}
	}

}
