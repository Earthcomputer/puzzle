package net.earthcomputer.puzzle.ui.editor;

import java.util.ArrayList;
import java.util.List;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.ui.AstUI;

/**
 * A plain text part of the equation
 */
public class TextEditable implements IEditable {

	private StringBuilder text;

	public TextEditable() {
		this.text = new StringBuilder();
	}

	public TextEditable(String text) {
		this.text = new StringBuilder(text);
	}

	public StringBuilder getText() {
		return text;
	}

	@Override
	public List<IEditable> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public int getClickExtensionFlags(int childIndex) {
		int numChildren = AstUI.getUI(this).getChildren(null, this).size();
		if (numChildren == 1) {
			return EXTEND_ALL;
		} else if (childIndex == 0) {
			return EXTEND_VERTICAL | EXTEND_LEFT;
		} else if (childIndex == numChildren - 1) {
			return EXTEND_VERTICAL | EXTEND_RIGHT;
		} else {
			return EXTEND_VERTICAL;
		}
	}

	public Pair<TextEditable, TextEditable> split(int index) {
		return Pair.of(new TextEditable(text.substring(0, index)), new TextEditable(text.substring(index)));
	}

	public void append(TextEditable other) {
		text.append(other.text);
	}

	@Override
	public String toString() {
		return toString("");
	}

	@Override
	public String toString(String prefix) {
		return "\"" + text + "\"";
	}

}
