package net.earthcomputer.puzzle.ui.editor;

import java.util.List;

import net.earthcomputer.puzzle.algebra.Util;

public class SuperscriptEditable implements IEditable {

	private EditableCompound child;

	public SuperscriptEditable(EditableCompound child) {
		this.child = child;
	}

	public EditableCompound getChild() {
		return child;
	}

	@Override
	public List<IEditable> getChildren() {
		return Util.arrayListOf(child);
	}

	@Override
	public int getClickExtensionFlags(int childIndex) {
		return EXTEND_ALL;
	}

	@Override
	public String toString() {
		return toString("");
	}

	@Override
	public String toString(String prefix) {
		return "sup[" + child.toString(prefix) + "]";
	}

}
