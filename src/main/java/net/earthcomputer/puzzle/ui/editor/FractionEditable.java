package net.earthcomputer.puzzle.ui.editor;

import java.util.List;

import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.algebra.ast.IFraction;

public class FractionEditable implements IEditable, IFraction<EditableCompound> {

	private EditableCompound ntor;
	private EditableCompound dtor;

	public FractionEditable(EditableCompound ntor, EditableCompound dtor) {
		this.ntor = ntor;
		this.dtor = dtor;
	}

	@Override
	public EditableCompound getNumerator() {
		return ntor;
	}

	@Override
	public EditableCompound getDenominator() {
		return dtor;
	}

	@Override
	public List<IEditable> getChildren() {
		return Util.arrayListOf(ntor, dtor);
	}

	@Override
	public int getClickExtensionFlags(int childIndex) {
		if (childIndex == 0) {
			return EXTEND_HORIZONTAL | EXTEND_UP;
		} else {
			return EXTEND_HORIZONTAL | EXTEND_DOWN;
		}
	}

	@Override
	public String toString() {
		return toString("");
	}

	@Override
	public String toString(String prefix) {
		return "frac[\n" + prefix + ntor.toString(prefix) + "\n" + prefix + "__________\n" + prefix
				+ dtor.toString(prefix) + "\n" + prefix + "]";
	}

}
