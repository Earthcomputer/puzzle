package net.earthcomputer.puzzle.ui;

/**
 * Describes a point, with an x and y coordinate
 */
public class Point {

	public final int x;
	public final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Point add(int x, int y) {
		return new Point(this.x + x, this.y + y);
	}

	public Point add(Point other) {
		return new Point(this.x + other.x, this.y + other.y);
	}

	public Point subtract(int x, int y) {
		return new Point(this.x - x, this.y - y);
	}

	public Point subtract(Point other) {
		return new Point(this.x - other.x, this.y - other.y);
	}

	public Point multiply(int scale) {
		return new Point(x * scale, y * scale);
	}

	public Point multiply(float scale) {
		return new Point((int) (x * scale), (int) (y * scale));
	}

	public Point multiply(double scale) {
		return new Point((int) (x * scale), (int) (y * scale));
	}

	public Point divide(int divisor) {
		return new Point(x / divisor, y / divisor);
	}

	public Point divide(float divisor) {
		return new Point((int) (x / divisor), (int) (y / divisor));
	}

	public double distance(int x, int y) {
		return Math.sqrt(distanceSq(x, y));
	}

	public double distance(Point other) {
		return distance(other.x, other.y);
	}

	public int distanceSq(int x, int y) {
		int dx = x - this.x;
		int dy = y - this.y;
		return dx * dx + dy * dy;
	}

	public int distanceSq(Point other) {
		return distanceSq(other.x, other.y);
	}

	@Override
	public int hashCode() {
		return x + 31 * y;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Point && equals((Point) other);
	}

	public boolean equals(Point other) {
		return x == other.x && y == other.y;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

}
