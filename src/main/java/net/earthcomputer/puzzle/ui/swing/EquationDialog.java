package net.earthcomputer.puzzle.ui.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import net.earthcomputer.puzzle.algebra.BuiltInFunctions;
import net.earthcomputer.puzzle.algebra.Ptr;
import net.earthcomputer.puzzle.algebra.SpecialChars;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.ui.ICanvas;
import net.earthcomputer.puzzle.ui.editor.EditableCompound;
import net.earthcomputer.puzzle.ui.editor.EditorTranslator;
import net.earthcomputer.puzzle.ui.editor.EquationTextField;
import net.earthcomputer.puzzle.ui.editor.SyntaxErrorException;

public class EquationDialog {

	private EquationDialog() {
	}

	public static Equation showEquationDialog() {
		return showDialog(new EditableCompound());
	}

	public static Equation showEquationDialog(Equation equation) {
		return showDialog(EditorTranslator.toEditable(equation));
	}

	private static Equation showDialog(EditableCompound editable) {
		Ptr<Equation> outValue = new Ptr<>();
		EquationTextField textField = new EquationTextField(editable);
		Ptr<Boolean> cursorVisible = new Ptr<Boolean>(Boolean.TRUE);
		Timer flasher = new Timer(500, e -> cursorVisible.set(!cursorVisible.get()));
		flasher.setInitialDelay(500);
		flasher.start();
		JComponent textFieldComponent = new JComponent() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				ICanvas canvas = new SwingCanvas(g);
				canvas.setColor(ICanvas.WHITE);
				canvas.fillRect(0, 0, getWidth(), getHeight());
				textField.draw(canvas, hasFocus() && cursorVisible.get());
			}
		};
		textFieldComponent.setPreferredSize(new Dimension(640, 240));
		textFieldComponent.setMinimumSize(textFieldComponent.getPreferredSize());
		textFieldComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				SwingEquationTextFieldInput.mousePressed(new SwingCanvas(textFieldComponent.getGraphics()), textField,
						e);
				flasher.restart();
				cursorVisible.set(Boolean.TRUE);
				textFieldComponent.repaint();
				textFieldComponent.requestFocusInWindow();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				SwingEquationTextFieldInput.mouseReleased(textField, e);
				flasher.restart();
				cursorVisible.set(Boolean.TRUE);
				textFieldComponent.repaint();
			}
		});
		textFieldComponent.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				SwingEquationTextFieldInput.mouseDragged(new SwingCanvas(textFieldComponent.getGraphics()), textField,
						e);
				flasher.restart();
				cursorVisible.set(Boolean.TRUE);
				textFieldComponent.repaint();
			}
		});
		textFieldComponent.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				SwingEquationTextFieldInput.keyPressed(textField, e);
				flasher.restart();
				cursorVisible.set(Boolean.TRUE);
				textFieldComponent.repaint();
			}

			@Override
			public void keyTyped(KeyEvent e) {
				SwingEquationTextFieldInput.keyTyped(textField, e);
				flasher.restart();
				cursorVisible.set(Boolean.TRUE);
				textFieldComponent.repaint();
			}
		});
		flasher.addActionListener(e -> textFieldComponent.repaint());

		JPanel buttonPanel = new JPanel();
		{
			JButton button = new JButton("^");
			button.addActionListener(e -> {
				textField.equationReleaseDragged();
				textField.insertSuperscript();
				textFieldComponent.requestFocusInWindow();
			});
			button.setToolTipText("Supercript");
			buttonPanel.add(button);
		}
		{
			JButton button = new JButton("" + SpecialChars.DIVISION_SIGN);
			button.addActionListener(e -> {
				textField.equationReleaseDragged();
				textField.insertFraction();
				textFieldComponent.requestFocusInWindow();
			});
			button.setToolTipText("Fraction (/)");
			buttonPanel.add(button);
		}
		{
			JButton button = new JButton("" + SpecialChars.SQUARE_ROOT);
			button.addActionListener(e -> {
				textField.equationReleaseDragged();
				textField.insertSurd();
				textFieldComponent.requestFocusInWindow();
			});
			button.setToolTipText("Surd (Ctrl+2)");
			buttonPanel.add(button);
		}
		{
			JButton button = new JButton("" + SpecialChars.PLUS_MINUS);
			button.addActionListener(e -> {
				textField.equationReleaseDragged();
				textField.insertCharacter(SpecialChars.PLUS_MINUS);
				textFieldComponent.requestFocusInWindow();
			});
			button.setToolTipText("Plus-Minus Sign");
			buttonPanel.add(button);
		}
		{
			JButton button = new JButton("" + SpecialChars.ALPHA + SpecialChars.BETA + SpecialChars.GAMMA);
			button.addActionListener(e -> {
				char greekLetter = showGreekLetterDialog();
				if (greekLetter != 0) {
					textField.equationReleaseDragged();
					textField.insertCharacter(greekLetter);
					textFieldComponent.requestFocusInWindow();
				}
			});
			button.setToolTipText("Greek Letter");
			buttonPanel.add(button);
		}

		JLabel errorMessage = new JLabel();
		errorMessage.setForeground(Color.RED);

		Object[] message = { "Enter an equation:", textFieldComponent, buttonPanel, errorMessage };

		JOptionPane optionPane = new JOptionPane(message, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);

		JDialog dialog = new JDialog((Frame) null, "Enter Equation", true);
		dialog.setContentPane(optionPane);
		dialog.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				textFieldComponent.requestFocusInWindow();
			}
		});

		optionPane.addPropertyChangeListener(e -> {
			String prop = e.getPropertyName();

			if (dialog.isVisible() && e.getSource() == optionPane && prop.equals(JOptionPane.VALUE_PROPERTY)
					&& optionPane.getValue() != null) {
				if (optionPane.getValue() instanceof Integer
						&& (Integer) optionPane.getValue() == JOptionPane.OK_OPTION) {
					Equation equation;
					try {
						equation = EditorTranslator.toEquation(editable, BuiltInFunctions.allBuiltInFunctions());
					} catch (SyntaxErrorException e1) {
						errorMessage.setText("Syntax error");
						optionPane.setValue(null);
						return;
					}
					outValue.set(equation);
				}
				dialog.dispose();
			}
		});

		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);

		flasher.stop();

		if (!(optionPane.getValue() instanceof Integer) || (Integer) optionPane.getValue() != JOptionPane.OK_OPTION) {
			return null;
		} else {
			return outValue.get();
		}
	}

	public static char showGreekLetterDialog() {
		TableModel dataModel = new AbstractTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public int getColumnCount() {
				return 6;
			}

			@Override
			public int getRowCount() {
				return 8;
			}

			@Override
			public Object getValueAt(int row, int col) {
				if (row < 4) {
					char c = (char) (SpecialChars.ALPHA + col + row * getColumnCount());
					if (c > SpecialChars.RHO) {
						c++;
					}
					return c;
				} else {
					char c = (char) (SpecialChars.CAPITAL_ALPHA + col + (row - 4) * getColumnCount());
					if (c > SpecialChars.CAPITAL_RHO) {
						c++;
					}
					return c;
				}
			}
		};
		JTable table = new JTable(dataModel) {
			private static final long serialVersionUID = 1L;

			@Override
			public String getToolTipText(MouseEvent e) {
				Point p = e.getPoint();
				int row = rowAtPoint(p);
				int col = columnAtPoint(p);

				Character c = (Character) getValueAt(row, col);
				if (c == null) {
					return null;
				} else {
					String tooltip = SwingEquationTextFieldInput.GREEK_LETTER_NAMES.get(c);
					tooltip += " (Alt+";
					if (row >= 4) {
						tooltip += "Shift+";
					}
					tooltip += Character.toUpperCase(SwingEquationTextFieldInput.GREEK_TO_LATIN.get(c));
					tooltip += ")";
					return tooltip;
				}
			}
		};
		table.setSelectionModel(new DefaultListSelectionModel() {
			private static final long serialVersionUID = 1L;

			{
				setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}

			@Override
			public void clearSelection() {
				// nop
			}

			@Override
			public void removeSelectionInterval(int from, int to) {
				// nop
			}
		});
		table.setCellSelectionEnabled(true);
		table.setRowSelectionInterval(0, 0);
		table.setColumnSelectionInterval(0, 0);

		JOptionPane optionPane = new JOptionPane(table, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
		JDialog dialog = new JDialog((Frame) null, "Select Greek Letter", true);
		dialog.setContentPane(optionPane);
		dialog.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				table.requestFocusInWindow();
			}
		});

		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					optionPane.setValue(JOptionPane.OK_OPTION);
					dialog.dispose();
				}
			}
		});

		optionPane.addPropertyChangeListener(e -> {
			String prop = e.getPropertyName();

			if (dialog.isVisible() && e.getSource() == optionPane && prop.equals(JOptionPane.VALUE_PROPERTY)
					&& optionPane.getValue() != null) {
				dialog.dispose();
			}
		});

		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);

		int result = optionPane.getValue() instanceof Integer ? (Integer) optionPane.getValue()
				: JOptionPane.CLOSED_OPTION;
		if (result != JOptionPane.OK_OPTION) {
			return '\0';
		}

		return (Character) dataModel.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
	}

}
