package net.earthcomputer.puzzle.ui.swing;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;

import net.earthcomputer.puzzle.algebra.FormattedText;
import net.earthcomputer.puzzle.algebra.ast.IAstNode;
import net.earthcomputer.puzzle.ui.AstUI;
import net.earthcomputer.puzzle.ui.ICanvas;

public class MathProtocol {

	private static AtomicInteger nextAstId = new AtomicInteger(0);

	private MathProtocol() {
	}

	public static String formattedTextToHTML(FormattedText text) {
		StringBuilder html = new StringBuilder("<html><body style=\"font-family:CMU Serif;font-size:20pt;\">");
		for (int i = 0; i < text.getAstNodes().length; i++) {
			html.append(text.getStrings()[i].replace("\n", "<br/>"));
			html.append("<img src=\"").append(generateImageURL(text.getAstNodes()[i])).append("\"/>");
		}
		html.append(text.getStrings()[text.getStrings().length - 1].replace("\n", "<br/>"));
		return html.append("</body></html>").toString();
	}

	public static String generateImageURL(IAstNode astNode) {
		return "math:" + Integer.toHexString(getId(astNode));
	}

	private static final ReferenceQueue<IAstNode> REF_QUEUE = new ReferenceQueue<>();
	private static final Map<WeakReference<IAstNode>, Integer> AST_IDS = Collections.synchronizedMap(new HashMap<>());
	private static final Map<Integer, byte[]> IMAGE_DATAS = Collections.synchronizedMap(new HashMap<>());

	private static int getId(IAstNode astNode) {
		for (Map.Entry<WeakReference<IAstNode>, Integer> entry : AST_IDS.entrySet()) {
			if (entry.getKey().get() == astNode) {
				return entry.getValue();
			}
		}

		int id = nextAstId.getAndIncrement();
		WeakReference<IAstNode> ref = new WeakReference<IAstNode>(astNode, REF_QUEUE);
		AST_IDS.put(ref, id);

		BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		ICanvas canvas = new SwingCanvas(image.getGraphics());
		AstUI<IAstNode> ui = AstUI.getUI(astNode);
		int width = ui.getWidth(canvas, astNode);
		int height = ui.getHeight(canvas, astNode);

		final float scaleFactor = 0.7f;

		image = new BufferedImage((int) Math.ceil(width * scaleFactor), (int) Math.ceil(height * scaleFactor),
				BufferedImage.TYPE_INT_ARGB);
		canvas = new SwingCanvas(image.getGraphics());
		canvas.pushMatrix();
		canvas.scale(scaleFactor);
		ui.draw(canvas, astNode);
		canvas.popMatrix();

		image = cropImage(image);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, "PNG", baos);
		} catch (IOException e) {
			throw new AssertionError(e);
		}
		IMAGE_DATAS.put(id, baos.toByteArray());

		return id;
	}

	private static int[] pixels = null;

	private static BufferedImage cropImage(BufferedImage image) {
		int x = -1, y = -1, maxX = -1, maxY = -1;
		int oldWidth = image.getWidth();
		int oldHeight = image.getHeight();
		if (pixels != null && pixels.length < oldWidth * oldHeight) {
			pixels = null;
		}
		pixels = image.getAlphaRaster().getPixels(0, 0, oldWidth, oldHeight, pixels);

		// left
		outerLoop: for (int dx = 0; dx < oldWidth; dx++) {
			for (int dy = 0; dy < oldHeight; dy++) {
				if (pixels[dy * oldWidth + dx] != 0) {
					x = dx;
					break outerLoop;
				}
			}
		}
		if (x == -1) { // if the image is empty
			return new BufferedImage(0, 0, BufferedImage.TYPE_INT_ARGB);
		}
		// top
		outerLoop: for (int dy = 0; dy < oldHeight; dy++) {
			for (int dx = 0; dx < oldWidth; dx++) {
				if (pixels[dy * oldWidth + dx] != 0) {
					y = dy;
					break outerLoop;
				}
			}
		}

		// right
		outerLoop: for (int dx = oldWidth - 1; dx >= 0; dx--) {
			for (int dy = 0; dy < oldHeight; dy++) {
				if (pixels[dy * oldWidth + dx] != 0) {
					maxX = dx;
					break outerLoop;
				}
			}
		}
		// bottom
		outerLoop: for (int dy = oldHeight - 1; dy >= 0; dy--) {
			for (int dx = 0; dx < oldWidth; dx++) {
				if (pixels[dy * oldWidth + dx] != 0) {
					maxY = dy;
					break outerLoop;
				}
			}
		}

		int width = maxX - x + 1;
		int height = maxY - y + 1;
		if (width == oldWidth && height == oldHeight) {
			return image;
		}
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		newImage.getGraphics().drawImage(image, -x, -y, null);

		return newImage;
	}

	public static void register() {
		Thread cleanupThread = new Thread(() -> {
			while (true) {
				try {
					@SuppressWarnings("unchecked")
					WeakReference<IAstNode> astNode = (WeakReference<IAstNode>) REF_QUEUE.remove();
					Integer id = AST_IDS.remove(astNode);
					IMAGE_DATAS.remove(id);
				} catch (InterruptedException e) {
				}
			}
		}, "Math Images Cleanup Thread");
		cleanupThread.setDaemon(true);
		cleanupThread.start();
		URL.setURLStreamHandlerFactory(protocol -> !"math".equals(protocol) ? null : new URLStreamHandler() {
			@Override
			protected URLConnection openConnection(URL url) throws IOException {
				return new URLConnection(url) {
					private byte[] data;

					{
						String path = url.getPath();
						int id;
						try {
							id = Integer.parseInt(path, 16);
							data = IMAGE_DATAS.get(id);
							if (data == null) {
								id = -1;
							}
						} catch (NumberFormatException e) {
							id = -1;
						}
						if (id == -1) {
							throw new UnknownHostException(path);
						}
					}

					@Override
					public void connect() throws IOException {
					}

					@Override
					public InputStream getInputStream() throws IOException {
						return new ByteArrayInputStream(data);
					}
				};
			}
		});
	}

}
