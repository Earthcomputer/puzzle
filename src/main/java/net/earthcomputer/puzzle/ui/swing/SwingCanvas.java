package net.earthcomputer.puzzle.ui.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import net.earthcomputer.puzzle.ui.ICanvas;

/**
 * The swing implementation of ICanvas
 */
public class SwingCanvas implements ICanvas {

	private static Map<String, BufferedImage> imageCache = new HashMap<>();
	private static Set<String> erroredImages = new HashSet<>();

	private Graphics2D g;
	private Deque<AffineTransform> matrices = new ArrayDeque<>();

	public SwingCanvas(Graphics g) {
		this.g = (Graphics2D) g;
	}

	@Override
	public void pushMatrix() {
		matrices.push(new AffineTransform());
	}

	@Override
	public void translate(int x, int y) {
		g.translate(x, y);
		matrices.peek().translate(x, y);
	}

	@Override
	public void scale(float factorx, float factory) {
		g.scale(factorx, factory);
		matrices.peek().scale(factorx, factory);
	}

	@Override
	public void rotate(double angle) {
		g.rotate(angle);
		matrices.peek().rotate(angle);
	}

	@Override
	public void popMatrix() {
		AffineTransform inverse = matrices.pop();
		try {
			inverse.invert();
		} catch (NoninvertibleTransformException e) {
			throw new Error("We should never get a singular matrix", e);
		}
		g.transform(inverse);
	}

	@Override
	public void setColor(int red, int green, int blue, int alpha) {
		g.setColor(new Color(red, green, blue, alpha));
	}

	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {
		g.drawLine(x1, y1, x2, y2);
	}

	@Override
	public void fillRect(int x, int y, int width, int height) {
		g.fillRect(x, y, width, height);
	}

	@Override
	public void drawRect(int x, int y, int width, int height) {
		g.drawRect(x, y, width, height);
	}

	@Override
	public void fillCircle(int x, int y, int r) {
		g.fillOval(x - r, y - r, r + r, r + r);
	}

	@Override
	public void drawCircle(int x, int y, int r) {
		g.drawOval(x - r, y - r, r + r, r + r);
	}

	@Override
	public void drawImage(String location, int x, int y, int w, int h) {
		BufferedImage image = getOrLoadImage(location);
		if (image == null) {
			return;
		}

		g.drawImage(image, x, y, w, h, null);
	}

	@Override
	public int getImageWidth(String location) {
		BufferedImage image = getOrLoadImage(location);
		if (image == null) {
			return 0;
		}

		return image.getWidth();
	}

	@Override
	public int getImageHeight(String location) {
		BufferedImage image = getOrLoadImage(location);
		if (image == null) {
			return 0;
		}

		return image.getHeight();
	}

	public static BufferedImage getOrLoadImage(String location) {
		BufferedImage image = imageCache.get(location);
		if (image == null) {
			if (erroredImages.contains(location)) {
				return null;
			}
			InputStream is = SwingBackend.class.getResourceAsStream("/images/" + location + ".png");
			if (is == null) {
				System.err.println("Image not found: " + location);
				erroredImages.add(location);
				return null;
			}
			try {
				image = ImageIO.read(is);
			} catch (IOException e) {
				System.err.println("Image format error in: " + location);
				erroredImages.add(location);
				return null;
			}
			imageCache.put(location, image);
		}
		return image;
	}

	@Override
	public void setFont(boolean italic, boolean bold) {
		int style = Font.PLAIN;
		if (italic) {
			style |= Font.ITALIC;
		}
		if (bold) {
			style |= Font.BOLD;
		}
		g.setFont(SwingBackend.CM_FONT.deriveFont(style));
	}

	@Override
	public int getStringWidth(String text) {
		return g.getFontMetrics().stringWidth(text);
	}

	@Override
	public int getStringHeight(String text) {
		return (int) createGlyphVector(text).getVisualBounds().getHeight();
	}

	@Override
	public int getBaseline(String text) {
		return (int) -createGlyphVector(text).getVisualBounds().getY();
	}

	@Override
	public void drawString(String text, int x, int y) {
		g.drawString(text, x, y + getBaseline(text));
	}

	private GlyphVector createGlyphVector(String text) {
		return g.getFont().createGlyphVector(g.getFontMetrics().getFontRenderContext(), text);
	}

}
