package net.earthcomputer.puzzle.ui.swing;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.Map;

import javax.swing.JPanel;

import net.earthcomputer.puzzle.algebra.soltree.SolutionTree;
import net.earthcomputer.puzzle.algebra.soltree.SolutionTreeNode;
import net.earthcomputer.puzzle.ui.Point;
import net.earthcomputer.puzzle.ui.SolutionTreeUI;

public class SolutionTreeComponent extends JPanel {

	private static final long serialVersionUID = 1L;

	private SolutionTree solutionTree;

	public void setSolutionTree(SolutionTree tree) {
		this.solutionTree = tree;
		// Short story courtesy of Richard
		setToolTipText("Billy went to the shops to buy a bottle of Vodka. "
				+ "He then went to a cashier and Deidre the assistant said: "
				+ "\"you look about 2 fam, what's going on blad?\"");
	}

	public SolutionTreeNode getHoveredNode(int mouseX, int mouseY) {
		SolutionTreeUI.RenderingMetrics metrics = SolutionTreeUI.generateRenderingMetrics(solutionTree);
		for (Map.Entry<SolutionTreeNode, Point> nodePoint : metrics.getNodePoints().entrySet()) {
			if (nodePoint.getValue().distanceSq(mouseX, mouseY) < SolutionTreeUI.RENDERED_RADIUS
					* SolutionTreeUI.RENDERED_RADIUS) {
				return nodePoint.getKey();
			}
		}
		return null;
	}

	@Override
	public String getToolTipText(MouseEvent e) {
		SolutionTreeNode hoveredNode = getHoveredNode(e.getX(), e.getY());
		if (hoveredNode == null) {
			return null;
		} else {
			return MathProtocol.formattedTextToHTML(hoveredNode.getTooltip());
		}
	}

	public void recalcSize() {
		if (solutionTree == null) {
			setPreferredSize(new Dimension(1, 1));
		} else {
			SolutionTreeUI.RenderingMetrics metrics = SolutionTreeUI.generateRenderingMetrics(solutionTree);
			setPreferredSize(new Dimension(metrics.getWidth(), metrics.getHeight()));
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (solutionTree != null) {
			SolutionTreeUI.RenderingMetrics metrics = SolutionTreeUI.generateRenderingMetrics(solutionTree);
			SolutionTreeUI.draw(solutionTree, new SwingCanvas(g), metrics);
		}
	}

}
