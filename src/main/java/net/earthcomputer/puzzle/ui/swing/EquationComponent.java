package net.earthcomputer.puzzle.ui.swing;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.ui.AstUI;
import net.earthcomputer.puzzle.ui.Box;
import net.earthcomputer.puzzle.ui.HoverInfo;
import net.earthcomputer.puzzle.ui.ICanvas;

public class EquationComponent extends JComponent {

	private static final long serialVersionUID = 1L;

	private Equation equation;
	private List<HoverInfoClickListener> hoverInfoClickListeners = new ArrayList<>();
	private Box highlightBox;
	private Box clickBox;

	public EquationComponent() {
	}

	public EquationComponent(Equation equation) {
		this.equation = equation;
	}

	public void registerMouseListener(Component other) {
		other.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (!isShowing()) {
					return;
				}
				ICanvas canvas = new SwingCanvas(getGraphics());
				Point offset = getOffset(canvas);
				Point mouseLocationOnScreen = e.getLocationOnScreen();
				Point thisLocationOnScreen = getLocationOnScreen();
				Point mouseLocation = new Point(mouseLocationOnScreen.x - thisLocationOnScreen.x,
						mouseLocationOnScreen.y - thisLocationOnScreen.y);
				mouseLocation = new Point(mouseLocation.x - offset.x, mouseLocation.y - offset.y);
				HoverInfo hoverInfo = AstUI.getHoverInfo(canvas, 0, EquationComponent.this.equation,
						SwingUtil.AWTPointToPoint(mouseLocation));
				for (HoverInfoClickListener listener : hoverInfoClickListeners) {
					listener.onClick(hoverInfo);
				}
			}
		});
	}

	public Equation getEquation() {
		return equation;
	}

	public void setEquation(Equation equation) {
		this.equation = equation;
	}

	public void addHoverInfoClickListener(HoverInfoClickListener listener) {
		hoverInfoClickListeners.add(listener);
	}

	public Box getHighlightBox() {
		return highlightBox;
	}

	public void setHighlightBox(Box highlightBox) {
		this.highlightBox = highlightBox;
	}

	public void setClickBox(Box clickBox) {
		this.clickBox = clickBox;
	}

	@Override
	public void paintComponent(Graphics g) {
		if (equation != null) {
			ICanvas canvas = new SwingCanvas(g);
			canvas.pushMatrix();
			Point offset = getOffset(canvas);
			canvas.translate(offset.x, offset.y);
			AstUI.getUI(equation).draw(new SwingCanvas(g), equation);
			if (clickBox != null) {
				canvas.setColor(0xff808080);
				canvas.drawRect(clickBox);
			}
			if (highlightBox != null) {
				canvas.setColor(0x40ffff00);
				canvas.fillRect(highlightBox);
				canvas.setColor(ICanvas.BLACK);
				canvas.drawRect(highlightBox);
			}
			canvas.popMatrix();
		}
	}

	private Point getOffset(ICanvas canvas) {
		AstUI<Equation> eqUI = AstUI.getUI(equation);
		return new Point(getWidth() / 2 - eqUI.getWidth(canvas, equation) / 2,
				getHeight() / 2 - eqUI.getHeight(canvas, equation));
	}

}
