package net.earthcomputer.puzzle.ui.swing;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.io.IOException;

import javax.swing.ToolTipManager;

public class SwingBackend {

	public static final Font CM_FONT;
	private static Thread awtHackThread;

	static {
		try {
			GraphicsEnvironment.getLocalGraphicsEnvironment()
					.registerFont(Font
							.createFont(Font.TRUETYPE_FONT, SwingBackend.class.getResourceAsStream("/fonts/cmunbi.ttf"))
							.deriveFont(Font.BOLD | Font.ITALIC));
			GraphicsEnvironment.getLocalGraphicsEnvironment()
					.registerFont(Font
							.createFont(Font.TRUETYPE_FONT, SwingBackend.class.getResourceAsStream("/fonts/cmunbx.ttf"))
							.deriveFont(Font.BOLD));
			GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(
					Font.createFont(Font.TRUETYPE_FONT, SwingBackend.class.getResourceAsStream("/fonts/cmunrm.ttf")));
			GraphicsEnvironment.getLocalGraphicsEnvironment()
					.registerFont(Font
							.createFont(Font.TRUETYPE_FONT, SwingBackend.class.getResourceAsStream("/fonts/cmunti.ttf"))
							.deriveFont(Font.ITALIC));
		} catch (FontFormatException | IOException e) {
			throw new RuntimeException(e);
		}
		CM_FONT = new Font("CMU Serif", Font.PLAIN, 40);
	}

	public static void main(String[] args) {
		awtHackThread = new Thread(() -> {
			Object o = new Object();
			try {
				synchronized (o) {
					o.wait();
				}
			} catch (InterruptedException e) {
				// ignore
			}
		});
		awtHackThread.start();

		MathProtocol.register();

		ToolTipManager.sharedInstance().setInitialDelay(0);
		ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

		MainWindow window = new MainWindow();
		window.setPreferredSize(new Dimension(1280, 720));
		window.setVisible(true);
	}

	public static void exit() {
		for (Window window : Window.getWindows()) {
			if (window.isDisplayable()) {
				window.dispose();
			}
		}
		awtHackThread.interrupt();
	}

}
