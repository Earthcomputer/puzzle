package net.earthcomputer.puzzle.ui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import com.sun.glass.events.KeyEvent;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.rule.AlgebraRules;
import net.earthcomputer.puzzle.algebra.rule.IAlgebraRule;
import net.earthcomputer.puzzle.algebra.rule.RuleContext;
import net.earthcomputer.puzzle.algebra.soltree.SolutionTree;
import net.earthcomputer.puzzle.algebra.soltree.SolutionTreeNode;
import net.earthcomputer.puzzle.ui.Box;
import net.earthcomputer.puzzle.ui.HoverInfo;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private Equation activeEquation;
	private EquationComponent equationComponent;
	private JLabel noEquationMessage;
	private JMenuBar menuBar;
	private JMenuItem editEquationMenuItem;
	private JPanel algebraActionPanel;
	private SolutionTree solutionTree;
	private SolutionTreeComponent solTreeComponent;
	private Deque<SolutionTreeNode> undoStack = new ArrayDeque<>();

	public MainWindow() {
		setTitle("Puzzle");
		getContentPane().setBackground(Color.WHITE);
		setMinimumSize(new Dimension(640, 480));

		setLayout(new BorderLayout());
		setExtendedState(getExtendedState() | MAXIMIZED_BOTH);

		equationComponent = new EquationComponent();
		equationComponent.registerMouseListener(this);
		equationComponent.setPreferredSize(new Dimension(640, 480));
		equationComponent.addHoverInfoClickListener(this::setAlgebraActions);

		noEquationMessage = new JLabel("Press File -> New to enter an equation");
		noEquationMessage.setHorizontalAlignment(JLabel.CENTER);
		noEquationMessage.setVerticalAlignment(JLabel.CENTER);
		noEquationMessage.setFont(SwingBackend.CM_FONT);
		noEquationMessage.setForeground(Color.GRAY);
		add(noEquationMessage, BorderLayout.CENTER);

		algebraActionPanel = new JPanel();
		algebraActionPanel.setBackground(Color.WHITE);
		algebraActionPanel.setPreferredSize(new Dimension(64, 100));
		add(algebraActionPanel, BorderLayout.SOUTH);

		solTreeComponent = new SolutionTreeComponent();
		solTreeComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				SolutionTreeNode node = solTreeComponent.getHoveredNode(e.getX(), e.getY());
				if (node != null) {
					undoStack.clear();
					solutionTree.setWorkingNode(node);
					setActiveEquation(node.getEquation().copy(), false);
					repaint();
				}
			}
		});
		JScrollPane scrollPane = new JScrollPane(solTreeComponent);
		scrollPane.setPreferredSize(new Dimension(100, 64));
		add(scrollPane, BorderLayout.EAST);

		menuBar = new JMenuBar();
		{
			JMenu menu = createMenu("File", "Relates to the program and file", KeyEvent.VK_F);
			{
				JMenuItem item = createMenuItem("New", "Create a new equation", KeyEvent.VK_N,
						KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK), e -> {
							Equation eq = EquationDialog.showEquationDialog();
							if (eq != null) {
								setActiveEquation(eq, true);
							}
						});
				menu.add(item);
			}
			{
				JMenuItem item = createMenuItem("Edit Equation", "Edit the existing equation", KeyEvent.VK_E,
						KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK), e -> {
							Equation eq = EquationDialog.showEquationDialog(activeEquation);
							if (eq != null) {
								setActiveEquation(eq, true);
							}
						});
				editEquationMenuItem = item;
				item.setEnabled(false);
				menu.add(item);
			}
			menu.addSeparator();
			{
				JMenuItem item = createMenuItem("Exit", "Exits the program", KeyEvent.VK_X, null,
						e -> SwingBackend.exit());
				menu.add(item);
			}
			menuBar.add(menu);
		}
		{
			JMenu menu = createMenu("Edit", "Editing the equation", KeyEvent.VK_E);
			{
				JMenuItem item = createMenuItem("Undo", "Undoes the last action", KeyEvent.VK_U,
						KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK), e -> undo());
				menu.add(item);
			}
			{
				JMenuItem item = createMenuItem("Redo", "Redoes the last undone action", KeyEvent.VK_R,
						KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK), e -> redo());
				menu.add(item);
			}
			menuBar.add(menu);
		}
		{
			JMenu menu = createMenu("Help", "You want help? You click this!", KeyEvent.VK_H);
			{
				JMenuItem item = createMenuItem("A Pink Pig", ":)", KeyEvent.VK_P, null, e -> {
					JOptionPane.showMessageDialog(null, new ImageIcon(SwingCanvas.getOrLoadImage("a_pink_pig")),
							"Puzzle", JOptionPane.PLAIN_MESSAGE);
				});
				menu.add(item);
			}
			menuBar.add(menu);
		}
		setJMenuBar(menuBar);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				SwingBackend.exit();
			}
		});
	}

	private JMenu createMenu(String name, String desc, int mnemonic) {
		JMenu menu = new JMenu(name);
		menu.setMnemonic(mnemonic);
		menu.getAccessibleContext().setAccessibleDescription(desc);
		return menu;
	}

	private JMenuItem createMenuItem(String name, String desc, int mnemonic, KeyStroke accelerator,
			ActionListener action) {
		JMenuItem item = new JMenuItem(name, mnemonic);
		item.getAccessibleContext().setAccessibleDescription(desc);
		if (accelerator != null) {
			item.setAccelerator(accelerator);
		}
		item.addActionListener(action);
		return item;
	}

	public void setActiveEquation(Equation equation, boolean newTree) {
		if (equation == activeEquation) {
			return;
		}
		setAlgebraActions(null);
		if (equation == null && activeEquation != null) {
			remove(equationComponent);
			add(noEquationMessage, BorderLayout.CENTER);
			editEquationMenuItem.setEnabled(false);
			if (newTree) {
				solutionTree = null;
			}
			revalidate();
		} else if (equation != null && activeEquation == null) {
			remove(noEquationMessage);
			add(equationComponent, BorderLayout.CENTER);
			editEquationMenuItem.setEnabled(true);
			if (newTree) {
				solutionTree = new SolutionTree(equation.copy());
			}
			revalidate();
		}
		this.activeEquation = equation;
		equationComponent.setEquation(equation);
		if (newTree) {
			if (equation == null) {
				solutionTree = null;
			} else {
				solutionTree = new SolutionTree(equation.copy());
			}
			solTreeComponent.setSolutionTree(solutionTree);
			solTreeComponent.recalcSize();
			solTreeComponent.revalidate();
		}
		repaint();
	}

	public void setAlgebraActions(HoverInfo hoverInfo) {
		algebraActionPanel.removeAll();
		equationComponent.setClickBox(null);
		if (hoverInfo != null) {
			List<Pair<RuleContext, Box>> bounds = hoverInfo.createRuleContexts();
			if (!bounds.isEmpty()) {
				equationComponent.setClickBox(bounds.get(0).getRight());
			}
			Map<RuleContext, Box> map = new IdentityHashMap<>();
			for (Pair<RuleContext, Box> pair : bounds) {
				map.put(pair.getLeft(), pair.getRight());
			}
			List<RuleContext> contexts = bounds.stream().map(Pair::getLeft).collect(Collectors.toList());
			for (Pair<IAlgebraRule, RuleContext> pair : AlgebraRules.getApplicableRules(contexts)) {
				Box box = map.get(pair.getRight());
				JButton button = new JButton(new ImageIcon(SwingCanvas.getOrLoadImage(pair.getLeft().getIcon())));
				button.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent e) {
						equationComponent.setHighlightBox(box);
						equationComponent.repaint();
					}

					@Override
					public void mouseExited(MouseEvent e) {
						if (box == equationComponent.getHighlightBox()) {
							equationComponent.setHighlightBox(null);
							equationComponent.repaint();
						}
					}
				});
				button.addActionListener(e -> {
					Equation prevEquation = activeEquation.copy();
					pair.getLeft().apply(pair.getRight());
					addSolTreeNode(prevEquation, pair.getLeft(), pair.getRight());
					algebraActionPanel.removeAll();
					equationComponent.setHighlightBox(null);
					equationComponent.setClickBox(null);
					repaint();
					revalidate();
				});
				button.setToolTipText(MathProtocol.formattedTextToHTML(pair.getLeft().getDescription(pair.getRight())));
				algebraActionPanel.add(button);
			}
		}
		equationComponent.repaint();
		algebraActionPanel.repaint();
		revalidate();
	}

	private void undo() {
		SolutionTreeNode workingNode = solutionTree.getWorkingNode().getParent();
		if (workingNode == null) {
			return;
		}
		undoStack.push(solutionTree.getWorkingNode());
		solutionTree.setWorkingNode(workingNode);
		setActiveEquation(workingNode.getEquation().copy(), false);
		repaint();
	}

	private void redo() {
		if (undoStack.isEmpty()) {
			return;
		}
		SolutionTreeNode workingNode = undoStack.pop();
		solutionTree.setWorkingNode(workingNode);
		setActiveEquation(workingNode.getEquation().copy(), false);
		repaint();
	}

	private void addSolTreeNode(Equation prevEquation, IAlgebraRule rule, RuleContext context) {
		context = context.withDifferentRoot(prevEquation);
		SolutionTreeNode newNode = new SolutionTreeNode(activeEquation.copy(), rule, context);
		solutionTree.getWorkingNode().addChild(newNode);
		solutionTree.setWorkingNode(newNode);
		solTreeComponent.recalcSize();
		solTreeComponent.revalidate();
		solTreeComponent.repaint();
		undoStack.clear();
	}

}
