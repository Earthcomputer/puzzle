package net.earthcomputer.puzzle.ui.swing;

import net.earthcomputer.puzzle.ui.HoverInfo;

@FunctionalInterface
public interface HoverInfoClickListener {

	void onClick(HoverInfo info);

}
