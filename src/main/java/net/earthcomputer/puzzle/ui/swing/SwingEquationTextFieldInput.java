package net.earthcomputer.puzzle.ui.swing;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import net.earthcomputer.puzzle.algebra.SpecialChars;
import net.earthcomputer.puzzle.ui.ICanvas;
import net.earthcomputer.puzzle.ui.editor.EquationTextField;

public class SwingEquationTextFieldInput {

	static final Map<Character, Character> LATIN_TO_GREEK = new HashMap<>();
	static final Map<Character, Character> GREEK_TO_LATIN = new HashMap<>();
	static final Map<Character, String> GREEK_LETTER_NAMES = new HashMap<>();
	static {
		registerLatinGreekMapping(SpecialChars.CAPITAL_ALPHA, 'A', "Capital Alpha");
		registerLatinGreekMapping(SpecialChars.CAPITAL_BETA, 'B', "Capital Beta");
		registerLatinGreekMapping(SpecialChars.CAPITAL_GAMMA, 'G', "Capital Gamma");
		registerLatinGreekMapping(SpecialChars.CAPITAL_DELTA, 'D', "Capital Delta");
		registerLatinGreekMapping(SpecialChars.CAPITAL_EPSILON, 'E', "Capital Epsilon");
		registerLatinGreekMapping(SpecialChars.CAPITAL_ZETA, 'Z', "Capital Zeta");
		registerLatinGreekMapping(SpecialChars.CAPITAL_ETA, 'H', "Capital Eta");
		registerLatinGreekMapping(SpecialChars.CAPITAL_THETA, 'U', "Capital Theta");
		registerLatinGreekMapping(SpecialChars.CAPITAL_IOTA, 'I', "Capital Iota");
		registerLatinGreekMapping(SpecialChars.CAPITAL_KAPPA, 'K', "Capital Kappa");
		registerLatinGreekMapping(SpecialChars.CAPITAL_LAMBDA, 'L', "Capital Lambda");
		registerLatinGreekMapping(SpecialChars.CAPITAL_MU, 'M', "Capital Mu");
		registerLatinGreekMapping(SpecialChars.CAPITAL_NU, 'N', "Capital Nu");
		registerLatinGreekMapping(SpecialChars.CAPITAL_XI, 'X', "Capital Xi");
		registerLatinGreekMapping(SpecialChars.CAPITAL_OMICRON, 'O', "Capital Omicron");
		registerLatinGreekMapping(SpecialChars.CAPITAL_PI, 'P', "Capital Pi");
		registerLatinGreekMapping(SpecialChars.CAPITAL_RHO, 'R', "Capital Rho");
		registerLatinGreekMapping(SpecialChars.CAPITAL_SIGMA, 'S', "Capital Sigma");
		registerLatinGreekMapping(SpecialChars.CAPITAL_TAU, 'T', "Capital Tau");
		registerLatinGreekMapping(SpecialChars.CAPITAL_UPSILON, 'Y', "Capital Upsilon");
		registerLatinGreekMapping(SpecialChars.CAPITAL_PHI, 'F', "Capital Phi");
		registerLatinGreekMapping(SpecialChars.CAPITAL_CHI, 'C', "Capital Chi");
		registerLatinGreekMapping(SpecialChars.CAPITAL_PSI, 'Q', "Capital Psi");
		registerLatinGreekMapping(SpecialChars.CAPITAL_OMEGA, 'W', "Capital Omega");
		registerLatinGreekMapping(SpecialChars.ALPHA, 'a', "Alpha");
		registerLatinGreekMapping(SpecialChars.BETA, 'b', "Beta");
		registerLatinGreekMapping(SpecialChars.GAMMA, 'g', "Gamma");
		registerLatinGreekMapping(SpecialChars.DELTA, 'd', "Delta");
		registerLatinGreekMapping(SpecialChars.EPSILON, 'e', "Epsilon");
		registerLatinGreekMapping(SpecialChars.ZETA, 'z', "Zeta");
		registerLatinGreekMapping(SpecialChars.ETA, 'h', "Eta");
		registerLatinGreekMapping(SpecialChars.THETA, 'u', "Theta");
		registerLatinGreekMapping(SpecialChars.IOTA, 'i', "Iota");
		registerLatinGreekMapping(SpecialChars.KAPPA, 'k', "Kappa");
		registerLatinGreekMapping(SpecialChars.LAMBDA, 'l', "Lambda");
		registerLatinGreekMapping(SpecialChars.MU, 'm', "Mu");
		registerLatinGreekMapping(SpecialChars.NU, 'n', "Nu");
		registerLatinGreekMapping(SpecialChars.XI, 'x', "Xi");
		registerLatinGreekMapping(SpecialChars.OMICRON, 'o', "Omicron");
		registerLatinGreekMapping(SpecialChars.PI, 'p', "Pi");
		registerLatinGreekMapping(SpecialChars.RHO, 'r', "Rho");
		registerLatinGreekMapping(SpecialChars.SIGMA, 's', "Sigma");
		registerLatinGreekMapping(SpecialChars.TAU, 't', "Tau");
		registerLatinGreekMapping(SpecialChars.UPSILON, 'y', "Upsilon");
		registerLatinGreekMapping(SpecialChars.PHI, 'f', "Phi");
		registerLatinGreekMapping(SpecialChars.CHI, 'c', "Chi");
		registerLatinGreekMapping(SpecialChars.PSI, 'q', "Psi");
		registerLatinGreekMapping(SpecialChars.OMEGA, 'w', "Omega");
	}

	private static void registerLatinGreekMapping(char greek, char latin, String greekLetterName) {
		GREEK_TO_LATIN.put(greek, latin);
		LATIN_TO_GREEK.put(latin, greek);
		GREEK_LETTER_NAMES.put(greek, greekLetterName);
	}

	public static void keyPressed(EquationTextField textField, KeyEvent e) {
		int keyCode = e.getKeyCode();
		boolean shiftDown = (e.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) != 0;
		boolean ctrlDown = (e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0;

		switch (keyCode) {
		case KeyEvent.VK_LEFT:
			textField.equationReleaseDragged();
			if (shiftDown) {
				if (ctrlDown) {
					textField.extendSelectionWordLeft();
				} else {
					textField.extendSelectionLeft();
				}
			} else {
				if (ctrlDown) {
					textField.moveCursorWordLeft();
				} else {
					textField.moveCursorLeft();
				}
			}
			break;
		case KeyEvent.VK_RIGHT:
			textField.equationReleaseDragged();
			if (shiftDown) {
				if (ctrlDown) {
					textField.extendSelectionWordRight();
				} else {
					textField.extendSelectionRight();
				}
			} else {
				if (ctrlDown) {
					textField.moveCursorWordRight();
				} else {
					textField.moveCursorRight();
				}
			}
			break;
		case KeyEvent.VK_UP:
			textField.equationReleaseDragged();
			if (shiftDown) {
				textField.extendSelectionUp();
			} else {
				textField.moveCursorUp();
			}
			break;
		case KeyEvent.VK_DOWN:
			textField.equationReleaseDragged();
			if (shiftDown) {
				textField.extendSelectionDown();
			} else {
				textField.moveCursorDown();
			}
			break;
		case KeyEvent.VK_HOME:
			textField.equationReleaseDragged();
			if (shiftDown) {
				if (ctrlDown) {
					textField.extendSelectionToVeryStart();
				} else {
					textField.extendSelectionToStart();
				}
			} else {
				if (ctrlDown) {
					textField.cursorToVeryStart();
				} else {
					textField.cursorToStart();
				}
			}
			break;
		case KeyEvent.VK_END:
			textField.equationReleaseDragged();
			if (shiftDown) {
				if (ctrlDown) {
					textField.extendSelectionToVeryEnd();
				} else {
					textField.extendSelectionToEnd();
				}
			} else {
				if (ctrlDown) {
					textField.cursorToVeryEnd();
				} else {
					textField.cursorToEnd();
				}
			}
			break;
		case KeyEvent.VK_BACK_SPACE:
			textField.equationReleaseDragged();
			textField.deleteBackwards();
			break;
		case KeyEvent.VK_DELETE:
			textField.equationReleaseDragged();
			textField.deleteForwards();
			break;
		case KeyEvent.VK_A:
			if (ctrlDown) {
				textField.equationReleaseDragged();
				textField.selectAll();
			}
			break;
		case KeyEvent.VK_2:
			if (ctrlDown) {
				textField.equationReleaseDragged();
				textField.insertSurd();
			}
		}
	}

	public static void keyTyped(EquationTextField textField, KeyEvent e) {
		char typedChar = e.getKeyChar();
		boolean altDown = (e.getModifiersEx() & KeyEvent.ALT_DOWN_MASK) != 0;
		if (typedChar != KeyEvent.CHAR_UNDEFINED && !Character.isISOControl(typedChar)) {
			textField.equationReleaseDragged();
			if (typedChar == '^') {
				textField.insertSuperscript();
			} else if (typedChar == '/' || typedChar == '\\' || typedChar == SpecialChars.DIVISION_SIGN) {
				textField.insertFraction();
			} else if (typedChar == SpecialChars.SQUARE_ROOT) {
				textField.insertSurd();
			} else if (altDown && LATIN_TO_GREEK.containsKey(typedChar)) {
				textField.insertCharacter(LATIN_TO_GREEK.get(typedChar));
			} else {
				textField.insertCharacter(typedChar);
			}
		}
	}

	public static void mousePressed(ICanvas canvas, EquationTextField textField, MouseEvent e) {
		if (!e.isShiftDown()) {
			textField.equationClicked(canvas, SwingUtil.AWTPointToPoint(e.getPoint()));
		} else {
			textField.equationDragged(canvas, SwingUtil.AWTPointToPoint(e.getPoint()));
		}
	}

	public static void mouseReleased(EquationTextField textField, MouseEvent e) {
		textField.equationReleaseDragged();
	}

	public static void mouseDragged(ICanvas canvas, EquationTextField textField, MouseEvent e) {
		textField.equationDragged(canvas, SwingUtil.AWTPointToPoint(e.getPoint()));
	}

}
