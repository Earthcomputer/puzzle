package net.earthcomputer.puzzle.ui.swing;

/**
 * Utilities specific to swing
 */
public class SwingUtil {

	private SwingUtil() {
	}

	public static net.earthcomputer.puzzle.ui.Point AWTPointToPoint(java.awt.Point point) {
		return new net.earthcomputer.puzzle.ui.Point(point.x, point.y);
	}

	public static java.awt.Point pointToAWTPoint(net.earthcomputer.puzzle.ui.Point point) {
		return new java.awt.Point(point.x, point.y);
	}

	public static net.earthcomputer.puzzle.ui.Dimension AWTDimensionToDimension(java.awt.Dimension dim) {
		return new net.earthcomputer.puzzle.ui.Dimension(dim.width, dim.height);
	}

	public static java.awt.Dimension dimensionToAWTDimension(net.earthcomputer.puzzle.ui.Dimension dim) {
		return new java.awt.Dimension(dim.w, dim.h);
	}

	public static net.earthcomputer.puzzle.ui.Box rectangleToBox(java.awt.geom.Rectangle2D rect) {
		return new net.earthcomputer.puzzle.ui.Box((int) rect.getX(), (int) rect.getY(), (int) rect.getWidth(),
				(int) rect.getHeight());
	}

	public static java.awt.Rectangle boxToRectangle(net.earthcomputer.puzzle.ui.Box box) {
		return new java.awt.Rectangle(box.x, box.y, box.w, box.h);
	}

}
