package net.earthcomputer.puzzle.ui;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import net.earthcomputer.puzzle.algebra.Util;
import net.earthcomputer.puzzle.algebra.ast.ComparisonOperator;
import net.earthcomputer.puzzle.algebra.ast.CompoundTerm;
import net.earthcomputer.puzzle.algebra.ast.ConstantTerm;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.ast.Expression;
import net.earthcomputer.puzzle.algebra.ast.ExpressionTerm;
import net.earthcomputer.puzzle.algebra.ast.FunctionTerm;
import net.earthcomputer.puzzle.algebra.ast.IAstNode;
import net.earthcomputer.puzzle.algebra.ast.IExponent;
import net.earthcomputer.puzzle.algebra.ast.IFraction;
import net.earthcomputer.puzzle.algebra.ast.ISurd;
import net.earthcomputer.puzzle.algebra.ast.NumberTerm;
import net.earthcomputer.puzzle.algebra.ast.ProductTerm;
import net.earthcomputer.puzzle.algebra.ast.Sign;
import net.earthcomputer.puzzle.algebra.ast.SimpleTerm;
import net.earthcomputer.puzzle.algebra.ast.VariableTerm;
import net.earthcomputer.puzzle.ui.editor.EditableCompound;
import net.earthcomputer.puzzle.ui.editor.SuperscriptEditable;
import net.earthcomputer.puzzle.ui.editor.TextEditable;

/**
 * Controls the rendering and interface of an AST node (or actually any Object)
 */
public abstract class AstUI<T> {

	private static final DecimalFormat EXP_FORMAT = new DecimalFormat("0.####E0");
	static {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setExponentSeparator("*10^");
		EXP_FORMAT.setDecimalFormatSymbols(symbols);
	}
	private static final DecimalFormat NORMAL_FORMAT = new DecimalFormat("0.####");
	private static final BigDecimal MIN_NORMAL = BigDecimal.valueOf(1, 4);
	private static final BigDecimal MAX_NORMAL = BigDecimal.valueOf(1, -7);

	private static String formatNumber(BigDecimal number) {
		if (number.signum() == 0) {
			return "0";
		}
		BigDecimal abs = number.abs();
		if (abs.compareTo(MIN_NORMAL) < 0 || abs.compareTo(MAX_NORMAL) >= 0) {
			return EXP_FORMAT.format(number);
		} else {
			return NORMAL_FORMAT.format(number);
		}
	}

	private static final Map<Class<?>, AstUI<?>> uiRegistry = new HashMap<>();

	static {
		registerUI(String.class, new TextUI<>(Function.identity(), false, false));

		registerUI(TextEditable.class, TextEditableUI.INSTANCE);
		registerUI(EditableCompound.class, EditableCompoundUI.INSTANCE);
		registerUI(SuperscriptEditable.class, SuperscriptEditableUI.INSTANCE);

		registerUI(NumberTerm.class, new TextUI<NumberTerm>(t -> formatNumber(t.getValue()), false, false));
		registerUI(ConstantTerm.class, new TextUI<>(ConstantTerm::getName, true, false));
		registerUI(VariableTerm.class, new TextUI<>(VariableTerm::getName, true, false));
		registerUI(ComparisonOperator.class, new TextUI<>(ComparisonOperator::getText, false, false));
		registerUI(Equation.class, new BasicHorizontalUI<>());
		registerUI(Sign.class, SignUI.INSTANCE);
		registerUI(Expression.class, ExpressionUI.INSTANCE);
		registerUI(CompoundTerm.class, new BasicHorizontalUI<>());
		registerUI(ProductTerm.class, ProductUI.INSTANCE);
		registerUI(IFraction.class, FractionUI.INSTANCE);
		registerUI(ISurd.class, SurdUI.INSTANCE);
		registerUI(IExponent.class, ExponentialUI.INSTANCE);
		registerUI(ExpressionTerm.class, ParenthesesUI.INSTANCE);
		registerUI(FunctionTerm.class, FunctionUI.INSTANCE);
	}

	/**
	 * Registers a default UI handler for the given type. Use a ForceUI to
	 * override this.
	 */
	private static void registerUI(Class<?> clazz, AstUI<?> renderer) {
		uiRegistry.put(clazz, renderer);
	}

	/**
	 * Gets the UI handler for the given object.
	 */
	@SuppressWarnings("unchecked")
	public static <T> AstUI<T> getUI(T node) {
		// Null safety
		if (node == null) {
			return (AstUI<T>) NullUI.INSTANCE;
		}

		// Force UIs are special cases
		if (node instanceof ForceUI) {
			return (AstUI<T>) createForceUIUI((ForceUI<?>) node);
		}

		// Find a matching class in the registry
		AstUI<T> ui = (AstUI<T>) getUI(node.getClass());
		if (ui != null) {
			return ui;
		}

		// No registered UI -> null UI
		return (AstUI<T>) NullUI.INSTANCE;
	}

	private static AstUI<?> getUI(Class<?> clazz) {
		// Direct match
		AstUI<?> ui = uiRegistry.get(clazz);
		if (ui != null) {
			return ui;
		}

		// Check interfaces
		for (Class<?> itf : clazz.getInterfaces()) {
			ui = getUI(itf);
			if (ui != null) {
				return ui;
			}
		}

		// Check superclass
		if (clazz == Object.class || clazz.isInterface()) {
			return null;
		} else {
			return getUI(clazz.getSuperclass());
		}
	}

	/**
	 * This method exists because of the limitations of Java generics
	 */
	@SuppressWarnings("unchecked")
	private static <T> ForceUIUI<T> createForceUIUI(ForceUI<?> forceUI) {
		return new ForceUIUI<>(((ForceUI<T>) forceUI).ui);
	}

	/**
	 * Gets the rendered width of this node
	 */
	public abstract int getWidth(ICanvas canvas, T node);

	/**
	 * Gets the rendered height of this node
	 */
	public abstract int getHeight(ICanvas canvas, T node);

	/**
	 * The baseline must be at the same y-position as the baselines of other
	 * nodes in the same HorizontalUI
	 */
	public abstract int getBaseline(ICanvas canvas, T node);

	/**
	 * Draws the node at 0, 0
	 */
	public void draw(ICanvas canvas, T node) {
		List<Object> children = getChildren(canvas, node);
		List<Point> childPositions = getChildPositions(canvas, node);
		List<Float> childScales = getChildScales(canvas, node);

		for (int i = 0; i < children.size(); i++) {
			AstUI<Object> childUI = getUI(children.get(i));
			Point position = childPositions.get(i);
			float scale = childScales.get(i);

			canvas.pushMatrix();
			canvas.translate(position.x, position.y);
			canvas.scale(scale);
			childUI.draw(canvas, children.get(i));
			canvas.popMatrix();
		}
	}

	/**
	 * Gets the rendered child nodes of this UI
	 */
	public abstract List<Object> getChildren(ICanvas canvas, T node);

	/**
	 * Gets the relative positions of the rendered child nodes of this UI
	 */
	public abstract List<Point> getChildPositions(ICanvas canvas, T node);

	/**
	 * Gets the relative scales of the rendered child nodes of this UI
	 */
	public abstract List<Float> getChildScales(ICanvas canvas, T node);

	// Interface

	/**
	 * Gets the hover info for a node
	 */
	public static HoverInfo getHoverInfo(ICanvas canvas, int index, Object node, Point relativeLocation) {
		AstUI<Object> ui = getUI(node);
		int width = ui.getWidth(canvas, node);
		int height = ui.getHeight(canvas, node);

		if (relativeLocation.x < 0 || relativeLocation.y < 0 || relativeLocation.x >= width
				|| relativeLocation.y >= height) {
			return HoverInfo.EMPTY;
		}

		List<Object> children = ui.getChildren(canvas, node);
		List<Point> childPositions = ui.getChildPositions(canvas, node);
		List<Float> childScales = ui.getChildScales(canvas, node);
		HoverInfo.Builder builder = new HoverInfo.Builder();

		for (int i = 0; i < children.size(); i++) {
			Object child = children.get(i);
			Point position = childPositions.get(i);
			float scale = childScales.get(i);

			Point newRelLoc = relativeLocation.subtract(position).divide(scale);
			builder.add(getHoverInfo(canvas, i, child, newRelLoc), position, scale);
		}

		builder.addHovered(index, node, relativeLocation, new Box(0, 0, width, height));

		return builder.build();
	}

	// Force UI

	/**
	 * Use this class as a wrapper object to replace the default registered UI
	 * for an object
	 */
	public static class ForceUI<T> {
		private T node;
		private AstUI<T> ui;

		public ForceUI(T node, AstUI<T> ui) {
			this.node = node;
			this.ui = ui;
		}
	}

	/**
	 * Part of the implementation of ForceUIs
	 */
	private static class ForceUIUI<T> extends AstUI<ForceUI<T>> {
		private AstUI<T> ui;

		public ForceUIUI(AstUI<T> ui) {
			this.ui = ui;
		}

		@Override
		public int getWidth(ICanvas canvas, ForceUI<T> node) {
			return ui.getWidth(canvas, node.node);
		}

		@Override
		public int getHeight(ICanvas canvas, ForceUI<T> node) {
			return ui.getHeight(canvas, node.node);
		}

		@Override
		public int getBaseline(ICanvas canvas, ForceUI<T> node) {
			return ui.getBaseline(canvas, node.node);
		}

		@Override
		public void draw(ICanvas canvas, ForceUI<T> node) {
			ui.draw(canvas, node.node);
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, ForceUI<T> node) {
			return ui.getChildren(canvas, node.node);
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, ForceUI<T> node) {
			return ui.getChildPositions(canvas, node.node);
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, ForceUI<T> node) {
			return ui.getChildScales(canvas, node.node);
		}
	}

	// Null UI

	/**
	 * The UI rendered when no registered UI could be found
	 */
	public static class NullUI extends LeafUI<IAstNode> {
		public static final NullUI INSTANCE = new NullUI();

		private NullUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, IAstNode node) {
			return 32;
		}

		@Override
		public int getHeight(ICanvas canvas, IAstNode node) {
			return 32;
		}

		@Override
		public int getBaseline(ICanvas canvas, IAstNode node) {
			return 32;
		}

		@Override
		public void draw(ICanvas canvas, IAstNode node) {
			canvas.setColor(ICanvas.MAGENTA);
			canvas.fillRect(0, 0, 16, 16);
			canvas.fillRect(16, 16, 16, 16);
			canvas.setColor(ICanvas.BLACK);
			canvas.fillRect(0, 16, 16, 16);
			canvas.fillRect(16, 0, 16, 16);
		}
	}

	// Utility UIs

	/**
	 * A UI which has no children
	 */
	public static abstract class LeafUI<T> extends AstUI<T> {
		@Override
		public List<Object> getChildren(ICanvas canvas, T node) {
			return new ArrayList<>();
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, T node) {
			return new ArrayList<>();
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, T node) {
			return new ArrayList<>();
		}
	}

	/**
	 * A leaf UI which renders text using a text extractor, which converts the
	 * node to the rendered text.
	 */
	public static class TextUI<T> extends LeafUI<T> {
		private Function<T, String> textExtractor;
		private boolean italic;
		private boolean bold;
		private static int HEIGHT = -1;
		private static int BASELINE = -1;

		public static int getHeight(ICanvas canvas) {
			if (HEIGHT != -1) {
				return HEIGHT;
			}
			canvas.setFont(false, false);
			return HEIGHT = canvas.getStringHeight("|");
		}

		public static int getBaseline(ICanvas canvas) {
			if (BASELINE != -1) {
				return BASELINE;
			}
			canvas.setFont(false, false);
			return BASELINE = canvas.getBaseline("|");
		}

		public TextUI(Function<T, String> textExtractor, boolean italic, boolean bold) {
			this.textExtractor = textExtractor;
			this.italic = italic;
			this.bold = bold;
		}

		@Override
		public int getWidth(ICanvas canvas, T node) {
			canvas.setFont(italic, bold);
			return canvas.getStringWidth(textExtractor.apply(node));
		}

		@Override
		public int getHeight(ICanvas canvas, T node) {
			return getHeight(canvas);
		}

		@Override
		public int getBaseline(ICanvas canvas, T node) {
			return getBaseline(canvas);
		}

		@Override
		public void draw(ICanvas canvas, T node) {
			canvas.setFont(italic, bold);
			canvas.setColor(ICanvas.BLACK);
			String text = textExtractor.apply(node);
			int y = getBaseline(canvas) - canvas.getBaseline(text);
			canvas.drawString(textExtractor.apply(node), 0, y);
		}
	}

	/**
	 * Renders its children horizontally, aligning them with the baseline
	 */
	public abstract static class HorizontalUI<T> extends AstUI<T> {
		@Override
		public int getWidth(ICanvas canvas, T node) {
			return getChildren(canvas, node).stream().mapToInt(n -> getUI(n).getWidth(canvas, n)).sum();
		}

		@Override
		public int getHeight(ICanvas canvas, T node) {
			return getBaseline(canvas, node) + getChildren(canvas, node).stream().mapToInt(n -> {
				AstUI<Object> ui = getUI(n);
				return ui.getHeight(canvas, n) - ui.getBaseline(canvas, n);
			}).max().orElse(0);
		}

		@Override
		public int getBaseline(ICanvas canvas, T node) {
			return getChildren(canvas, node).stream().mapToInt(n -> getUI(n).getBaseline(canvas, n)).max().orElse(0);
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, T node) {
			List<Object> children = getChildren(canvas, node);
			int baseline = getBaseline(canvas, node);
			int x = 0;

			List<Point> childPositions = new ArrayList<>(children.size());
			for (Object child : children) {
				AstUI<Object> ui = getUI(child);
				childPositions.add(new Point(x, baseline - ui.getBaseline(canvas, child)));
				x += ui.getWidth(canvas, child);
			}

			return childPositions;
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, T node) {
			return Util.arrayListWithNCopies(getChildren(canvas, node).size(), 1f);
		}
	}

	/**
	 * A horizontal UI whose children are the children of the AST node
	 */
	public static class BasicHorizontalUI<T extends IAstNode> extends HorizontalUI<T> {

		@Override
		public List<Object> getChildren(ICanvas canvas, T node) {
			return new ArrayList<>(node.getChildren());
		}

	}

	// Specialized Editor UIs

	/**
	 * Draws editable text
	 */
	private static class TextEditableUI extends HorizontalUI<TextEditable> {

		public static final TextEditableUI INSTANCE = new TextEditableUI();
		private static final Pattern LETTER_PATTERN = Pattern.compile("\\p{L}+");

		private TextEditableUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, TextEditable node) {
			List<Object> children = new ArrayList<>();

			Matcher matcher = LETTER_PATTERN.matcher(node.getText());
			int start = 0;
			int end = 0;
			while (matcher.find()) {
				start = matcher.start();
				if (start != 0) {
					children.add(new ForceUI<>(node.getText().substring(end, start),
							new TextUI<>(Function.identity(), false, false)));
				}
				end = matcher.end();
				children.add(new ForceUI<>(node.getText().substring(start, end),
						new TextUI<>(Function.identity(), true, false)));
			}
			if (end != node.getText().length()) {
				children.add(
						new ForceUI<>(node.getText().substring(end), new TextUI<>(Function.identity(), false, false)));
			}

			return children;
		}

	}

	/**
	 * Draws an editable compound
	 */
	private static class EditableCompoundUI extends HorizontalUI<EditableCompound> {

		public static final EditableCompoundUI INSTANCE = new EditableCompoundUI();

		private EditableCompoundUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, EditableCompound node) {
			if (node.getChildEditableCount() == 0 && node.getChildText(0).getText().length() == 0) {
				return Util.arrayListOf(new ForceUI<>(null, EmptyEditableUI.INSTANCE));
			}
			return new ArrayList<>(node.getChildren());
		}

	}

	private static class EmptyEditableUI extends LeafUI<Void> {
		public static final EmptyEditableUI INSTANCE = new EmptyEditableUI();

		private EmptyEditableUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, Void node) {
			return TextUI.getHeight(canvas) / 2;
		}

		@Override
		public int getHeight(ICanvas canvas, Void node) {
			return TextUI.getHeight(canvas);
		}

		@Override
		public int getBaseline(ICanvas canvas, Void node) {
			return TextUI.getBaseline(canvas);
		}

		@Override
		public void draw(ICanvas canvas, Void node) {
			canvas.setColor(ICanvas.BLACK);
			canvas.drawRect(2, 2, TextUI.getHeight(canvas) / 2 - 2, TextUI.getHeight(canvas) - 2);
		}
	}

	private static class SuperscriptEditableUI extends AstUI<SuperscriptEditable> {

		public static final SuperscriptEditableUI INSTANCE = new SuperscriptEditableUI();

		private SuperscriptEditableUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, SuperscriptEditable node) {
			return getUI(node.getChild()).getWidth(canvas, node.getChild()) / 2;
		}

		@Override
		public int getHeight(ICanvas canvas, SuperscriptEditable node) {
			return getUI(node.getChild()).getHeight(canvas, node.getChild()) / 2;
		}

		@Override
		public int getBaseline(ICanvas canvas, SuperscriptEditable node) {
			return getUI(node.getChild()).getHeight(canvas, node.getChild());
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, SuperscriptEditable node) {
			return Util.arrayListOf(node.getChild());
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, SuperscriptEditable node) {
			return Util.arrayListOf(new Point(0, 0));
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, SuperscriptEditable node) {
			return Util.arrayListOf(0.5f);
		}
	}

	// Specialized AST UIs

	/**
	 * Draws an additive sign without any extra padding, used for the first term
	 * in an expression
	 */
	private static class ThinSignUI extends TextUI<Sign> {

		public static final ThinSignUI INSTANCE = new ThinSignUI();

		private ThinSignUI() {
			super(sign -> {
				String text = sign == Sign.MINUS_PLUS ? Sign.PLUS_MINUS.getText() : sign.getText();
				return text.substring(1, 2);
			}, false, false);
		}

		@Override
		public void draw(ICanvas canvas, Sign node) {
			if (node == Sign.MINUS_PLUS) {
				canvas.pushMatrix();
				int height = getHeight(canvas, node);
				canvas.translate(0, height / 2);
				canvas.scale(1, -1);
				canvas.translate(0, -height / 2);
			}
			super.draw(canvas, node);
			if (node == Sign.MINUS_PLUS) {
				canvas.popMatrix();
			}
		}

	}

	/**
	 * Draws an additive sign
	 */
	private static class SignUI extends TextUI<Sign> {

		public static final SignUI INSTANCE = new SignUI();

		private SignUI() {
			super(sign -> sign == Sign.MINUS_PLUS ? Sign.PLUS_MINUS.getText() : sign.getText(), false, false);
		}

		@Override
		public void draw(ICanvas canvas, Sign node) {
			if (node == Sign.MINUS_PLUS) {
				canvas.pushMatrix();
				int height = getHeight(canvas, node);
				canvas.translate(0, height / 2);
				canvas.scale(1, -1);
				canvas.translate(0, -height / 2);
			}
			super.draw(canvas, node);
			if (node == Sign.MINUS_PLUS) {
				canvas.popMatrix();
			}
		}

	}

	/**
	 * Draws an expression
	 */
	private static class ExpressionUI extends HorizontalUI<Expression> {

		public static final ExpressionUI INSTANCE = new ExpressionUI();

		private ExpressionUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, Expression node) {
			// Default children is each term preceded by signs
			List<Object> children = new ArrayList<>(node.getChildren());

			if (children.isEmpty()) {
				return children;
			}

			Object firstChild = children.get(0);
			if (firstChild == Sign.PLUS) {
				// + at start of expression is redundant
				children.remove(0);
			} else {
				// The first sign in the expression should have no padding
				children.set(0, new ForceUI<Sign>((Sign) firstChild, ThinSignUI.INSTANCE));
			}

			return children;
		}

	}

	/**
	 * Draws a multiplication sign
	 */
	private static class MultiplicationSignUI extends LeafUI<Object> {

		public static final MultiplicationSignUI INSTANCE = new MultiplicationSignUI();

		private MultiplicationSignUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, Object node) {
			return 32;
		}

		@Override
		public int getHeight(ICanvas canvas, Object node) {
			return 16;
		}

		@Override
		public int getBaseline(ICanvas canvas, Object node) {
			return 16;
		}

		@Override
		public void draw(ICanvas canvas, Object node) {
			canvas.setColor(ICanvas.BLACK);
			canvas.drawLine(8, 0, 23, 15);
			canvas.drawLine(8, 15, 23, 0);
		}

	}

	/**
	 * Draws a product of terms
	 */
	private static class ProductUI extends HorizontalUI<ProductTerm> {

		public static final ProductUI INSTANCE = new ProductUI();

		private ProductUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, ProductTerm node) {
			List<SimpleTerm> terms = node.getTerms();
			List<Object> list = new ArrayList<>();

			if (terms.isEmpty()) {
				return list;
			}

			// Add the terms with multiplication signs in between only if the
			// two terms shouldn't be juxtaposed
			list.add(terms.get(0));
			for (int i = 1; i < terms.size(); i++) {
				if (!Util.shouldJuxtapose(terms.get(i - 1), terms.get(i))) {
					list.add(new ForceUI<>(null, MultiplicationSignUI.INSTANCE));
				}
				list.add(terms.get(i));
			}

			return list;
		}

	}

	/**
	 * Draws a fraction
	 */
	private static class FractionUI extends AstUI<IFraction<?>> {

		public static final FractionUI INSTANCE = new FractionUI();

		private FractionUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, IFraction<?> node) {
			Object ntor = node.getNumerator();
			Object dtor = node.getDenominator();

			int ntorWidth = getUI(ntor).getWidth(canvas, ntor);
			int dtorWidth = getUI(dtor).getWidth(canvas, dtor);

			return Math.max(ntorWidth, dtorWidth) + 4;
		}

		@Override
		public int getHeight(ICanvas canvas, IFraction<?> node) {
			Object ntor = node.getNumerator();
			Object dtor = node.getDenominator();

			int ntorHeight = getUI(ntor).getHeight(canvas, ntor);
			int dtorHeight = getUI(dtor).getHeight(canvas, dtor);

			return ntorHeight + dtorHeight + 7;
		}

		@Override
		public int getBaseline(ICanvas canvas, IFraction<?> node) {
			Object ntor = node.getNumerator();
			return getUI(ntor).getBaseline(canvas, ntor);
		}

		@Override
		public void draw(ICanvas canvas, IFraction<?> node) {
			super.draw(canvas, node);

			Object ntor = node.getNumerator();
			AstUI<Object> ntorUI = getUI(ntor);
			int ntorHeight = ntorUI.getHeight(canvas, ntor);
			int totalWidth = getWidth(canvas, node);

			canvas.setColor(ICanvas.BLACK);
			canvas.drawLine(0, ntorHeight + 4, totalWidth, ntorHeight + 4);
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, IFraction<?> node) {
			return Util.arrayListOf(node.getNumerator(), node.getDenominator());
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, IFraction<?> node) {
			Object ntor = node.getNumerator();
			Object dtor = node.getDenominator();

			AstUI<Object> ntorUI = getUI(ntor);
			AstUI<Object> dtorUI = getUI(dtor);
			int ntorWidth = ntorUI.getWidth(canvas, ntor);
			int dtorWidth = dtorUI.getWidth(canvas, dtor);
			int ntorHeight = ntorUI.getHeight(canvas, ntor);
			int totalWidth = Math.max(ntorWidth, dtorWidth) + 4;

			return Util.arrayListOf(new Point(totalWidth / 2 - ntorWidth / 2, 0),
					new Point(totalWidth / 2 - dtorWidth / 2, ntorHeight + 7));
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, IFraction<?> node) {
			return Util.arrayListOf(1f, 1f);
		}

	}

	/**
	 * Draws a surd
	 */
	private static class SurdUI extends AstUI<ISurd<?, ?>> {

		public static final SurdUI INSTANCE = new SurdUI();
		private static final int ROOT_SIGN_WIDTH = 16;

		private SurdUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, ISurd<?, ?> node) {
			Object n = node.getN();
			Object x = node.getX();

			int width;
			if (n == null) {
				width = 0;
			} else {
				width = getUI(n).getWidth(canvas, n) / 2 - 5;
			}

			width += ROOT_SIGN_WIDTH;

			width += getUI(x).getWidth(canvas, x);

			return width + 2;
		}

		@Override
		public int getHeight(ICanvas canvas, ISurd<?, ?> node) {
			Object n = node.getN();
			Object x = node.getX();

			int height = getUI(x).getHeight(canvas, x) + 4;
			int nHeight = n == null ? 0 : getUI(n).getHeight(canvas, n);
			if (nHeight > height) {
				height += (nHeight - height) / 2;
			}

			return height;
		}

		@Override
		public int getBaseline(ICanvas canvas, ISurd<?, ?> node) {
			Object n = node.getN();
			Object x = node.getX();
			AstUI<Object> nUI = getUI(n);
			AstUI<Object> xUI = getUI(x);
			int nHeight = nUI.getHeight(canvas, n);
			int rootHeight = xUI.getHeight(canvas, x) + 4;

			int baseline;
			if (nHeight > rootHeight) {
				baseline = (nHeight - rootHeight) / 2;
			} else {
				baseline = 0;
			}

			return baseline + 3 + xUI.getBaseline(canvas, x);
		}

		@Override
		public void draw(ICanvas canvas, ISurd<?, ?> node) {
			super.draw(canvas, node);

			Object n = node.getN();
			Object x = node.getX();
			AstUI<Object> xUI = getUI(x);
			int rootHeight = xUI.getHeight(canvas, x) + 4;

			if (n != null) {
				AstUI<Object> nUI = getUI(n);
				int nHeight = nUI.getHeight(canvas, n);

				canvas.pushMatrix();
				canvas.translate((int) (nUI.getWidth(canvas, n) * 0.5 - 5), 0);
				if (nHeight > rootHeight) {
					canvas.translate(0, (int) ((nHeight - rootHeight) * 0.5f));
				}
			}

			canvas.setColor(ICanvas.BLACK);
			canvas.drawLine(0, rootHeight * 12 / 16, ROOT_SIGN_WIDTH / 4, rootHeight * 10 / 16);
			canvas.drawLine(ROOT_SIGN_WIDTH / 4, rootHeight * 10 / 16, ROOT_SIGN_WIDTH / 2, rootHeight);
			canvas.drawLine(ROOT_SIGN_WIDTH / 2, rootHeight, ROOT_SIGN_WIDTH, 0);
			canvas.drawLine(ROOT_SIGN_WIDTH, 0, ROOT_SIGN_WIDTH + xUI.getWidth(canvas, x) + 2, 0);

			if (n != null) {
				canvas.popMatrix();
			}
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, ISurd<?, ?> node) {
			if (node.getN() == null) {
				return Util.arrayListOf(node.getX());
			} else {
				return Util.arrayListOf(node.getN(), node.getX());
			}
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, ISurd<?, ?> node) {
			if (node.getN() == null) {
				return Util.arrayListOf(new Point(ROOT_SIGN_WIDTH, 3));
			} else {
				Object x = node.getX();
				Object n = node.getN();
				AstUI<Object> xUI = getUI(x);
				AstUI<Object> nUI = getUI(n);
				int rootHeight = xUI.getHeight(canvas, x) + 4;
				int nHeight = nUI.getHeight(canvas, n);

				Point nPoint = new Point(0, rootHeight / 2 - nHeight / 2);

				int xx = nUI.getWidth(canvas, n) / 2 - 5;
				int xy;
				if (nHeight > rootHeight) {
					xy = (nHeight - rootHeight) / 2;
				} else {
					xy = 0;
				}

				xx += ROOT_SIGN_WIDTH;
				xy += 3;
				Point xPoint = new Point(xx, xy);

				return Util.arrayListOf(nPoint, xPoint);
			}
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, ISurd<?, ?> node) {
			if (node.getN() == null) {
				return Util.arrayListOf(1f);
			} else {
				return Util.arrayListOf(0.5f, 1f);
			}
		}

	}

	/**
	 * Draws an exponential term
	 */
	private static class ExponentialUI extends AstUI<IExponent<?, ?>> {

		public static final ExponentialUI INSTANCE = new ExponentialUI();

		private ExponentialUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, IExponent<?, ?> node) {
			Object base = node.getBase();
			Object exponent = node.getExponent();
			return getUI(base).getWidth(canvas, base) + getUI(exponent).getWidth(canvas, exponent) / 2;
		}

		@Override
		public int getHeight(ICanvas canvas, IExponent<?, ?> node) {
			Object base = node.getBase();
			Object exponent = node.getExponent();
			return getUI(base).getHeight(canvas, base) + getUI(exponent).getHeight(canvas, exponent) / 4;
		}

		@Override
		public int getBaseline(ICanvas canvas, IExponent<?, ?> node) {
			Object base = node.getBase();
			Object exponent = node.getExponent();
			return getUI(base).getBaseline(canvas, base) + getUI(exponent).getHeight(canvas, exponent) / 4;
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, IExponent<?, ?> node) {
			return Util.arrayListOf(node.getBase(), node.getExponent());
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, IExponent<?, ?> node) {
			Object base = node.getBase();
			Object exponent = node.getExponent();
			AstUI<Object> baseUI = getUI(base);
			AstUI<Object> exponentUI = getUI(exponent);

			return Util.arrayListOf(new Point(0, exponentUI.getHeight(canvas, exponent) / 4),
					new Point(baseUI.getWidth(canvas, base), 0));
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, IExponent<?, ?> node) {
			return Util.arrayListOf(1f, 0.5f);
		}

	}

	/**
	 * Draws a parenthesized term
	 */
	private static class ParenthesesUI extends HorizontalUI<ExpressionTerm> {

		public static final ParenthesesUI INSTANCE = new ParenthesesUI();

		private ParenthesesUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, ExpressionTerm node) {
			return Util.arrayListOf("(", node.getExpression(), ")");
		}

	}

	/**
	 * Draws a function reference. This is split into 3 parts: the function
	 * name, the "function extra" and the function arguments. The function extra
	 * can display the inverse, derivatives and powers notations, along with the
	 * function parameters
	 */
	private static class FunctionUI extends HorizontalUI<FunctionTerm> {

		public static final FunctionUI INSTANCE = new FunctionUI();

		private FunctionUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, FunctionTerm node) {
			List<Object> list = new ArrayList<>();
			list.add(node.getName());
			list.add(new ForceUI<>(node, FunctionExtraUI.INSTANCE));

			list.add("(");
			List<Expression> args = node.getArgs();
			if (!args.isEmpty()) {
				list.add(args.get(0));
				for (int i = 1; i < args.size(); i++) {
					list.add(",");
					list.add(args.get(i));
				}
			}
			list.add(")");

			return list;
		}

	}

	/**
	 * Draws the inverse, derivatives or power notation (only one at a time,
	 * collectively known as a "top UI"), along with the function parameters
	 */
	private static class FunctionExtraUI extends AstUI<FunctionTerm> {

		public static final FunctionExtraUI INSTANCE = new FunctionExtraUI();

		private FunctionExtraUI() {
		}

		@Override
		public int getWidth(ICanvas canvas, FunctionTerm node) {
			int paramWidth;
			if (node.getParams().isEmpty()) {
				paramWidth = 0;
			} else {
				paramWidth = FunctionParametersUI.INSTANCE.getWidth(canvas, node) / 2;
			}

			AstUI<FunctionTerm> topUI = getTopUI(node);
			int topWidth = topUI == null ? 0 : topUI.getWidth(canvas, node);
			topWidth *= getTopUIType(node).getScale();

			return Math.max(topWidth, paramWidth);
		}

		@Override
		public int getHeight(ICanvas canvas, FunctionTerm node) {
			if (node.getParams().isEmpty()) {
				AstUI<FunctionTerm> topUI = getTopUI(node);
				if (topUI == null) {
					return 0;
				}
				return topUI.getHeight(canvas, node);
			}
			if (getTopUIType(node) == TopUIType.NONE) {
				return FunctionParametersUI.INSTANCE.getHeight(canvas, node) / 2;
			}
			return getTopUI(node).getBaseline(canvas, node) + FunctionParametersUI.INSTANCE.getHeight(canvas, node) / 2;
		}

		@Override
		public int getBaseline(ICanvas canvas, FunctionTerm node) {
			AstUI<FunctionTerm> ui = getTopUI(node);
			if (ui != null) {
				return ui.getBaseline(canvas, node);
			}
			return 0;
		}

		private static AstUI<FunctionTerm> getTopUI(FunctionTerm node) {
			switch (getTopUIType(node)) {
			case DERIVATIVES:
				String primes = Stream.generate(() -> "'").limit(node.getnDerivatives())
						.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
				return new TextUI<>(f -> primes, false, false);
			case INVERSE:
				return new TextUI<>(f -> "-1", false, false);
			case NONE:
				return null;
			case POWERS:
				return new TextUI<>(f -> String.valueOf(node.getPowers()), false, false);
			default:
				throw new AssertionError();
			}
		}

		private static TopUIType getTopUIType(FunctionTerm node) {
			if (node.isInverse()) {
				return TopUIType.INVERSE;
			}
			if (node.getnDerivatives() != 0) {
				return TopUIType.DERIVATIVES;
			}
			if (node.getPowers() != 1) {
				return TopUIType.POWERS;
			}
			return TopUIType.NONE;
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, FunctionTerm node) {
			List<Object> list = new ArrayList<>();

			if (getTopUIType(node) != TopUIType.NONE) {
				list.add(new ForceUI<>(node, getTopUI(node)));
			}

			List<Expression> params = node.getParams();
			if (!params.isEmpty()) {
				list.add(new ForceUI<>(node, FunctionParametersUI.INSTANCE));
			}

			return list;
		}

		@Override
		public List<Point> getChildPositions(ICanvas canvas, FunctionTerm node) {
			if (node.getParams().isEmpty()) {
				AstUI<FunctionTerm> topUI = getTopUI(node);
				if (topUI == null) {
					return new ArrayList<>();
				} else {
					return Util.arrayListOf(new Point(0, 0));
				}
			}
			if (getTopUIType(node) == TopUIType.NONE) {
				return Util.arrayListOf(new Point(0, 0));
			}
			return Util.arrayListOf(new Point(0, 0), new Point(0, getBaseline(canvas, node)));
		}

		@Override
		public List<Float> getChildScales(ICanvas canvas, FunctionTerm node) {
			TopUIType topUIType = getTopUIType(node);
			if (node.getParams().isEmpty()) {
				if (topUIType == TopUIType.NONE) {
					return new ArrayList<>();
				} else {
					return Util.arrayListOf(topUIType.getScale());
				}
			}
			if (topUIType == TopUIType.NONE) {
				return Util.arrayListOf(0.5f);
			}
			return Util.arrayListOf(topUIType.getScale(), 0.5f);
		}

		private static enum TopUIType {
			NONE(0), INVERSE(0.5f), DERIVATIVES(1), POWERS(0.5f);

			private final float scale;

			private TopUIType(float scale) {
				this.scale = scale;
			}

			public float getScale() {
				return scale;
			}
		}

	}

	/**
	 * Draws a list of parameters
	 */
	private static class FunctionParametersUI extends HorizontalUI<FunctionTerm> {

		public static final FunctionParametersUI INSTANCE = new FunctionParametersUI();

		private FunctionParametersUI() {
		}

		@Override
		public List<Object> getChildren(ICanvas canvas, FunctionTerm node) {
			List<Expression> params = node.getParams();
			List<Object> list = new ArrayList<>();
			if (params.isEmpty()) {
				return list;
			}

			list.add(params.get(0));
			for (int i = 1; i < params.size(); i++) {
				list.add(",");
				list.add(params.get(i));
			}

			return list;
		}

	}

}
