package net.earthcomputer.puzzle.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.earthcomputer.puzzle.algebra.Pair;
import net.earthcomputer.puzzle.algebra.ast.Equation;
import net.earthcomputer.puzzle.algebra.ast.IAstNode;
import net.earthcomputer.puzzle.algebra.rule.RuleContext;

/**
 * Information about which AST nodes the user is hovering over with the cursor,
 * or has tapped on the screen. The smallest and most specific node should be
 * first in the list
 */
public class HoverInfo {

	public static final HoverInfo EMPTY = new HoverInfo(Collections.emptyList(), Collections.emptyList(),
			Collections.emptyList(), Collections.emptyList());

	private List<Integer> indexes;
	private List<Object> nodes;
	private List<Point> relativeLocations;
	private List<Box> bounds;

	private HoverInfo(List<Integer> indexes, List<Object> nodes, List<Point> relativeLocations, List<Box> bounds) {
		this.indexes = indexes;
		this.nodes = nodes;
		this.relativeLocations = relativeLocations;
		this.bounds = bounds;
	}

	public int getIndex(int index) {
		return indexes.get(index);
	}

	public Object getNode(int index) {
		return nodes.get(index);
	}

	public Point getRelativeLocation(int index) {
		return relativeLocations.get(index);
	}

	public Box getBounds(int index) {
		return bounds.get(index);
	}

	public int getDepth() {
		return nodes.size();
	}

	public List<Pair<RuleContext, Box>> createRuleContexts() {
		List<Integer> indexes = new ArrayList<>();
		List<Box> bounds = new ArrayList<>();
		IAstNode lastAstNode = null;
		Equation equation = null;
		for (int i = 0; i < nodes.size(); i++) {
			Object node = nodes.get(i);
			if (!(node instanceof IAstNode)) {
				continue;
			}
			IAstNode astNode = (IAstNode) node;
			if (lastAstNode != null) {
				boolean found = false;
				for (int j = 0; j < astNode.getChildCount(); j++) {
					if (astNode.getChild(j) == lastAstNode) {
						indexes.add(j);
						bounds.add(this.bounds.get(i - 1));
						found = true;
						break;
					}
				}
				if (!found) {
					throw new AssertionError();
				}
			}
			lastAstNode = astNode;
			if (astNode instanceof Equation) {
				equation = (Equation) astNode;
				break;
			}
		}

		if (equation == null) {
			return Collections.emptyList();
		}
		Collections.reverse(indexes);

		List<Pair<RuleContext, Box>> contexts = new ArrayList<>(indexes.size() + 1);
		while (!indexes.isEmpty()) {
			Box box = bounds.remove(0);
			RuleContext context = RuleContext.create(equation, new ArrayList<>(indexes));
			contexts.add(Pair.of(context, box));
			indexes.remove(indexes.size() - 1);
		}
		contexts.add(
				Pair.of(RuleContext.create(equation, new ArrayList<>(0)), this.bounds.get(this.bounds.size() - 1)));
		return contexts;
	}

	public static class Builder {
		private List<Integer> indexes = new ArrayList<>();
		private List<Object> nodes = new ArrayList<>();
		private List<Point> relativeLocations = new ArrayList<>();
		private List<Box> bounds = new ArrayList<>();

		public Builder addHovered(int index, Object node, Point relativeLocation, Box bounds) {
			this.indexes.add(index);
			this.nodes.add(node);
			this.relativeLocations.add(relativeLocation);
			this.bounds.add(bounds);
			return this;
		}

		public Builder add(HoverInfo info) {
			indexes.addAll(info.indexes);
			nodes.addAll(info.nodes);
			relativeLocations.addAll(info.relativeLocations);
			bounds.addAll(info.bounds);
			return this;
		}

		public Builder add(HoverInfo info, Point location, float scale) {
			indexes.addAll(info.indexes);
			nodes.addAll(info.nodes);
			relativeLocations.addAll(info.relativeLocations);
			info.bounds.stream().map(b -> b.multiply(scale).add(location)).forEach(bounds::add);
			return this;
		}

		public HoverInfo build() {
			return new HoverInfo(indexes, nodes, relativeLocations, bounds);
		}
	}

}
