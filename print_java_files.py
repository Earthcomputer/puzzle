import os

def main():
    rootDir = "."
    outfile = open(os.path.join(rootDir, "output.txt"), "w")
    for dirName, subdirList, fileList in os.walk(rootDir):
        for fileName in fileList:
            if fileName.endswith(".java"):
                processJavaFile(os.path.join(dirName, fileName), outfile)
    outfile.close()

def processJavaFile(fileName, outfile):
    print("Processing " + fileName)
    outfile.write("===== File: " + fileName + " =====\n")
    with open(fileName, "r") as infile:
        for line in infile:
            outfile.write(line)
    outfile.write("\n")

main()
